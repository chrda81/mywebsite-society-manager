/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

// Dependencies
const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const marked = require('marked');
const renderer = new marked.Renderer();

module.exports = env => {
    const DEBUG = process.env.NODE_ENV !== 'production';
    const $dist = path.resolve(__dirname, 'dist');

    const config = {
        // The base directory (absolute path) for resolving the entry option.
        context: __dirname,
        mode: DEBUG ? 'development' : 'production',
        entry: {
            appSocietyManager: 'society_manager_assets/js/app.js',
            appSocietyManagerStyle: 'society_manager_assets/js/appStyle.js',
            app: 'core_assets/js/app.js',
            appStyle: 'core_assets/js/appStyle.js',
            appAdmin: 'society_manager_assets/js/appAdmin.js',
            appAdminStyle: 'society_manager_assets/js/appAdminStyle.js',
        },
        cache: DEBUG,
        output: {
            path: path.join($dist, 'society_manager/assets_bundles'),
            filename: '[name].js',
            publicPath: DEBUG
                ? 'http://127.0.0.1:8080/dist/society_manager/assets_bundles/'
                : '/static/society_manager/assets_bundles/',
        },
        node: {
            fs: 'empty',
        },
        // optimization: {
        //     splitChunks: {
        //         chunks: 'all',
        //         name: DEBUG ? true : false,
        //     },
        // },
        plugins: [
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: '[name].css',
                chunkFilename: '[name].[chunkhash].css',
            }),
            // Where webpack stores data about bundles.
            new BundleTracker({
                path: __dirname,
                filename:
                    './mywebsite_society_manager/static/society_manager/webpack-stats.json',
            }),
            new webpack.HashedModuleIdsPlugin(),
            // Makes jQuery available in every module.
            new webpack.ProvidePlugin({
                $: 'node_modules/jquery',
                jQuery: 'node_modules/jquery',
                'window.jQuery': 'node_modules/jquery',
                'window.$': 'node_modules/jquery',
                Popper: 'node_modules/popper.js/dist/umd/popper.min.js',
                JSONEditor: 'node_modules/jsoneditor/dist/jsoneditor.js',
                ScrollReveal: 'core_assets/js/default/scrollReveal.js',
            }),

            // Remove all files from the out dir, before re-creating.
            new CleanWebpackPlugin([`${$dist}/**/*`]),
            // Copy all image files from assets to webpack
            // new CopyWebpackPlugin([{
            //     from: './custom_website/assets/img',
            //     to: path.join($dist, 'assets_bundles/custom_website/assets/img')
            // }, ]),

            // To strip all locales except “en”
            new MomentLocalesPlugin(),
            // Or: To strip all locales except “en” and de
            // (“en” is built into Moment and can’t be removed)
            new MomentLocalesPlugin({
                localesToKeep: ['de'],
            }),
        ],
        module: {
            rules: [
                {
                    // Exposes jQuery for use outside Webpack build
                    test: require.resolve('jquery'),
                    use: [
                        {
                            loader: 'expose-loader',
                            options: 'jQuery',
                        },
                        {
                            loader: 'expose-loader',
                            options: '$',
                        },
                    ],
                },
                {
                    test: /\.(sa|sc)ss$/,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                        'resolve-url-loader',
                        'sass-loader',
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                    ],
                },
                {
                    test: /\.less$/,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'resolve-url-loader',
                        'less-loader',
                    ],
                },
                {
                    // Use loaders on all .js and .jsx files.
                    test: /\.jsx?$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                },
                {
                    test: /\.json$/,
                    exclude: /(node_modules)/,
                    loader: 'json-loader', //JSON loader
                },
                {
                    test: /\.txt$/,
                    loader: 'raw-loader',
                },
                {
                    test: /\.(png|woff|woff2|svg|eot|ttf|gif|jpe?g)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 1000,
                                // for ManifestStaticFilesStorage reuse, use [path][name].[md5:hash:hex:12].[ext]
                                name: '[path][name].[ext]',
                            },
                        },
                        // {
                        //     loader: "image-webpack-loader",
                        //     options: {
                        //         query: {
                        //             bypassOnDebug: "true",
                        //             mozjpeg: {
                        //                 progressive: true
                        //             },
                        //             gifsicle: {
                        //                 interlaced: true
                        //             },
                        //             optipng: {
                        //                 optimizationLevel: 7
                        //             }
                        //         }
                        //     }
                        // }
                    ],
                },
                {
                    test: /datatables\.net.*/,
                    loader: 'imports-loader?define=>false',
                },
                {
                    test: /pnotify.*\.js$/,
                    loader: 'imports-loader?define=>false,global=>window',
                },
                {
                    test: /\.md$/,
                    use: [
                        {
                            loader: 'html-loader',
                        },
                        {
                            loader: 'markdown-loader',
                            options: {
                                pedantic: true,
                                renderer,
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            // extensions: ['.js', '.jsx'],
            modules: ['mywebsite_society_manager/assets', 'node_modules'],
            alias: {
                jquery: require.resolve('jquery'),
                node_modules: path.join(__dirname, 'node_modules'),
                core_assets: path.join(
                    __dirname,
                    'mywebsite_society_manager/static/mywebsite'
                ),
                society_manager_assets: path.join(
                    __dirname,
                    'mywebsite_society_manager/assets'
                ),
                static: path.join(
                    __dirname,
                    'mywebsite_society_manager/static'
                ),
            },
        },
        devServer: {
            writeToDisk: true,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        },
    };

    // config.module.rules.unshift({
    //     parser: {
    //         amd: false,
    //     }
    // });

    return config;
};
