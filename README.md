# MyWebsite-society-manager

This is the society_manager package of the modular WEB application for my business. It is written in Python/Django.

---

License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under http://www.d-fsl.org.

---

If you like my work, I would appreciate a donation. You can find several options on my website www.dsoft-app-dev.de at section crowdfunding. Thank you!

This application depends on some Python modules, which should be installed into the virtual environment the WEB application is running from within. For the development system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/development.txt

For the production system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/production.txt

Create your own copy of settings_secure.py and tweak the settings:

    $ cp custom_website/settings/settings_secure.example.py custom_website/settings/settings_secure.py

To generate a new SECRET_KEY use:

    $ function django_secret() { python -c "import random,string;print(''.join([random.SystemRandom().choice(\"{}{}{}\".format(string.ascii_letters, string.digits, string.punctuation)) for i in range(63)]).replace('\\'','\\'\"\\'\"\\''))"; }
    $ echo "DJANGO_SECRET_KEY='$(django_secret)'"

To create the database structure and group permissions use:

    $ ./manage.py migrate
    $ ./manage.py create_society_manager_permissions

Now collect all static files. But before running the command, you have to collect all node_modules from third party vendors:

    $ yarn

For the production system use:

    $ yarn --production

Then you can run the command:

    $ ./manage.py collectstatic

Rename "cookiecutter_template_dir" in templates/\*/home/base.html to your choice of string "template_dir".

The app 'extendcmds' allows to create a superuser with a given password:

    $ ./manage.py createsuperuser --username <account name> --email <email> --password <secret password> --preserve --noinput

To import sample data, use:

    $ ./manage.py loaddata .development/mysql/sample_data.json

To dump the whole database use:

    $ ./manage.py dumpdata --indent 2 > .development/mysql/dump.json

To dump a single database table use:

    $ ./manage.py dumpdata --indent 2 mywebsite_home.websitesettings > .development/mysql/dump_websettings.json

It is possible to exclude unneccessary tables during the database dump, e.g.:

    $ ./manage.py dumpdata --natural-foreign --natural-primary --indent 2 -e filebrowser -e reversion -e hitcount -e sessions -e admin -e captcha -e contenttypes -e auth -e dashboard -e mywebsite_members  > .development/mysql/sample_data.json

## Settings for mywebsite_home app

The mywebsite_home app represents the website with its index page. In order to get the correct pages for menu entries 'Crowdfunding', 'About', 'Disclosures' and 'Data privacy', you have to create 3 article pages in the mywebsite_blog app and name the slug field with the names 'crowdfunding', 'about-me', 'disclosures' and 'data-privacy'.

## Unit tests

Unit tests for Javascript is done by using QUnit, see https://qunitjs.com
To execute tests, you have 2 possibilities:

1. Run "python -m http.server" instead of "./manage.py runserver" from command line and browse http://localhost:8000/test/tests.html

or

2. Install QUnit (npm install -g qunit) and simply run it from command line "qunit"

All other unit tests can be run with

    $ tox

## More MyWebsite modules

-   [mywebsite_base](https://bitbucket.org/chrda81/mywebsite-base)
-   [mywebsite_celery](https://bitbucket.org/chrda81/mywebsite-celery)
-   [mywebsite_members](https://bitbucket.org/chrda81/mywebsite-members)
-   [mywebsite_project](https://bitbucket.org/chrda81/mywebsite-project)

To be continued ...

## Build package

To build package via setup.py use:

    $ python setup.py sdist --formats=zip --dist-dir .tox/dist

To install this built package locally in your Python virtual environment, use e.g.:

    $ pip install --exists-action w .tox/dist/mywebsite_base_django-1.0.9.zip

## GIT pre-commit example

To change the build no. automatically, create a file '.git/pre-commit' with the following content:

    #!/bin/bash

    # extract version and write it to VERSION file before commit
    VERSION_NO=`cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print(obj['version']);"`

    echo $VERSION_NO > ./VERSION
    cp ./mywebsite_society_manager/__init__.py ./mywebsite_society_manager/__init__.py.bak
    sed "/__version__/s/.*/__version__ = '$VERSION_NO'/" ./mywebsite_society_manager/__init__.py.bak > ./mywebsite_society_manager/__init__.py
    rm ./mywebsite_society_manager/__init__.py.bak

## VSCode custom configuration

Custum configuration files are stored in the .vscode folder under the project path. The file _settings.json_ contains
full path settings to programs, e.g. python, pep8 and so forth. It is recommended to exclude this file in git, using
_.gitignore_. Here is a template for _settings.json_:

    {
        "python.pythonPath": "<path to bin/python>",
        "python.linting.pep8Path": "<path to bin/pep8>",
        "python.linting.pep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pep8Enabled": true,
        "python.formatting.autopep8Path": "<path to bin/autopep8>",
        "python.formatting.autopep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pylintPath": "<path bin/pylint>",
        "python.linting.pylintArgs": [
            "--errors-only",
            "--load-plugins",
            "pylint_django"
        ],
        "python.linting.pylintEnabled": true,
        "html.format.enable": false,
        "eslint.enable": false,
        "editor.formatOnSave": true,
        "editor.rulers": [
            119,
            140
        ],
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            ".vscode": false,
            ".idea": true,
            "**/*,pyc": true,
            "*/__pycache__": true
        },
        "workbench.editor.enablePreview": false,
        "editor.find.globalFindClipboard": true,
        "search.globalFindClipboard": true,
        "editor.minimap.side": "left",
        "git.promptToSaveFilesBeforeCommit": true,
        "markdown-pdf.type": [
            "html",
        ],
        "markdown-pdf.convertOnSave": true,
        "markdown-pdf.outputDirectory": "templates/toods/admin_doc",
        "markdown-pdf.convertOnSaveExclude": [
            "^(?!MANUAL.*\\.md$)",
        ],
        "markdown-pdf.includeDefaultStyles": false,
        "markdown-pdf.styles": [
            "static/css/bootstrap.min.css",
        ],
    }

## Change history

    1.2.3:

-   feat(): Hide SocietyMember form's payment and service tab
-   feat(): Don't export internal (private) ticket comments
-   feat(): Show prospect labels in member view
-   feat(): Prefill prospect_form with extracted ticket body
-   fix(): Add missing ajax data to send-mail
-   chore(): Update weasyprint package version to 59
-   fix(): ProgrammingError on app_settings
-   fix(): Representation of labels
-   feat(): Show settings.EMAILTO as bcc field value
-   feat(): Add notes field for member form

    1.2.2:

-   fix(style): Change nav_title height for logo
-   chore(): Rename company_name_field to prospect_name_field

    1.2.1:

-   fix(deps): Upgrade mywebsite-django-base to v1.2.3 and mywebsite-members-django to v1.1.3
-   fix(deps): Replace node-sass with sass for nodejs > v16
-   chore(): Add site_vars to templates
-   chore(): Add SITE_NAME from settings
-   feat(settings): Add 'app_url_prefix' to load package with custom url and add 'show_members' to show/hide members list
-   feat(locales): Add formats for datetime inputs and views

    1.2.0:

-   feat(search): Add field for members active
-   fix(ticket): Add missing content_name attribute to mail_templates
-   fix(): Some sanity checks
-   fix(base): Change PHONENUMBER_DEFAULT_FORMAT from International to E164

    1.1.2:

-   fix(deps): Upgrade mywebsite-django-base to v1.2.2 and mywebsite-members-django to v1.1.2

    1.1.1:

-   fix(deps): Set correct versions of datatable bootstrap4 dependencies
-   fix(conf): Set PHONENUMBER_DEFAULT_FORMAT to 'INTERNATIONAL'

    1.1.0:

-   feat(deps): Upgrade mywebsite-django-base to version 1.2.1 and mywebsite-members-django to v1.1.1
-   fix(deps): Handle RemovedInDjango40Warning and replace ugettext_lazy and ugettext

    1.0.18:

-   fix(deps): Updated packages mywebsite-base-django to v1.1.23, mywebsite-members-django to v1.0.16

    1.0.17:

-   fix(deps): Updated packages mywebsite-base-django to v1.1.21, mywebsite-members-django to v1.0.14
-   fix(test): Updated test methods and create folder fixtures

    1.0.16:

-   Updated packages mywebsite-base-django to v1.1.18, mywebsite-members-django to v1.0.11.

    1.0.15:

-   Updated packages mywebsite-base-django to v1.1.15, mywebsite-members-django to v1.0.9 and made depending changes.

    1.0.14:

-   Bugfix for has_collapsednavbar in set_collapsednavbar_jx()
-   Replaced xhtml2pdf with weasyprint. Made depending changes.

    1.0.13:

-   Added package mywebsite-base-django v1.1.13 and made depending changes.
-   Added truncatechars function to models display name for Comments and Microtasks.

    1.0.12:

-   Added package mywebsite-base-django v1.1.12 and made depending changes.
-   Replaced django-auditlog with django-reversion. Made depending changes.
-   Changed some templates to use POST for reversion middleware instead of GET.
-   Members sequence is only incremented, if new member-nr contains value of get_next_value('members').
-   Added TicketAdminForm and javascript functions for tagifying labels in ModelAdmin.
-   Bugfix for OperationalError during manage.py migrate.
-   Bugfix for loading app settings json dict.

    1.0.11:

-   Updated package django-sequences to version 2.4.
-   Bugfix for Ticket's sequence in manage_ticket().
-   Moved fetching of app_settings from database to module settings.
-   Created new settings value for Member's sequence.

    1.0.10:

-   Added datetime range filter to TicketAdmin, ProspectiveCompanyAdmin and SocietyMemberAdmin.
-   Added CSV export function for ModelAdmin.
-   Added comment field to Ticket search.

    1.0.9:

-   Added packages mywebsite-base-django v1.1.10 and mywebsite-members-django v1.0.7.
-   Added package django-notifications-hq for showing notifications.
-   Modified mywebsite_society_manager/**init**.py

    1.0.8:

-   Fixed admin view for attachments, comments and microtasks.
-   Changed database field 'file_name' of attachments to store 500 chars.
-   Catch SuspiciousFileOperation exception of core_settings_data method filename_text.
-   Order actual files database selection of members, prospects and tickets by file_name.
-   Fixed datatables search for members.

    1.0.7:

-   Fixed html chars of company names for select2 combobox in create/manage ticket view.
-   Fixed database query for ProspectiveCompanyListJson() to show only prospects and no members.
-   Show database integrity errors in popup dialog for add_company_jx().
-   Added setting for show_mail_popup. Don't forget to add this to your application settings for 'society_manager'!

    1.0.6:

-   Reworked datatables to handle server side data for better performance.

    1.0.5:

-   Added button on member's view to create ticket for editing company details.
-   Fixed responsive control for master data and setting tables.
-   Added list and search options for ModelAdmin views.
-   Reworked member's used services view.
-   Clean orphaned functions.
-   Added used services to export.
-   Added shortcode to priorities model.

    1.0.4:

-   Added custom title for ticket's, prospective company's and member's view.
-   Added company data for member's export view.
-   Fixed prospective company's and member's view, when opening tickets from comments tab.
-   Added some translations.
-   Added prospective company field 'additional_name'.
-   Added datatable settings for master data tables.
-   Fixed visibility for FontAwesome calendar symbol in datatable filter.
-   Changed group name from 'society_managers' to 'soma_managers'.
-   Added groups 'soma_operators' and 'soma_superusers'.
