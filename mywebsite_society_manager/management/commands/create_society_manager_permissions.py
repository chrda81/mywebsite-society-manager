# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from mywebsite_society_manager import models


class Command(BaseCommand):
    help = 'Create auth groups for the society_manager app with all necessary permissions.'

    def handle(self, *args, **options):
        # Create read-only group for operators
        ro_group, ro_created = Group.objects.get_or_create(name='soma_operators')

        # Create groups for superusers, managers with write permissions
        rw_group, rw_created = Group.objects.get_or_create(name='soma_managers')
        srw_group, srw_created = Group.objects.get_or_create(name='soma_superusers')

        # Add permissions to group 'soma_managers' on model 'BasicSubscription'
        ct = ContentType.objects.get_for_model(models.BasicSubscription)
        can_add = Permission.objects.get(name='Can add Basic Subscription', content_type=ct)
        can_change = Permission.objects.get(name='Can change Basic Subscription', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Basic Subscription', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'ProspectiveCompany'
        ct = ContentType.objects.get_for_model(models.ProspectiveCompany)
        can_add = Permission.objects.get(name='Can add Prospective Company', content_type=ct)
        can_change = Permission.objects.get(name='Can change Prospective Company', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Prospective Company', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'Country'
        ct = ContentType.objects.get_for_model(models.Country)
        can_add = Permission.objects.get(name='Can add Country', content_type=ct)
        can_change = Permission.objects.get(name='Can change Country', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Country', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'EnrollmentFee'
        ct = ContentType.objects.get_for_model(models.EnrollmentFee)
        can_add = Permission.objects.get(name='Can add Enrollment Fee', content_type=ct)
        can_change = Permission.objects.get(name='Can change Enrollment Fee', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Enrollment Fee', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'MembershipType'
        ct = ContentType.objects.get_for_model(models.MembershipType)
        can_add = Permission.objects.get(name='Can add Membership Type', content_type=ct)
        can_change = Permission.objects.get(name='Can change Membership Type', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Membership Type', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'PaymentInterval'
        ct = ContentType.objects.get_for_model(models.PaymentInterval)
        can_add = Permission.objects.get(name='Can add Payment Interval', content_type=ct)
        can_change = Permission.objects.get(name='Can change Payment Interval', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Payment Interval', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'PaymentType'
        ct = ContentType.objects.get_for_model(models.PaymentType)
        can_add = Permission.objects.get(name='Can add Payment Type', content_type=ct)
        can_change = Permission.objects.get(name='Can change Payment Type', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Payment Type', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)

        # Add permissions to group 'soma_managers' on model 'SocietyMember'
        ct = ContentType.objects.get_for_model(models.SocietyMember)
        can_add = Permission.objects.get(name='Can add Society Member', content_type=ct)
        can_change = Permission.objects.get(name='Can change Society Member', content_type=ct)
        can_delete = Permission.objects.get(name='Can delete Society Member', content_type=ct)
        rw_group.permissions.add(can_add)
        rw_group.permissions.add(can_change)
        srw_group.permissions.add(can_add)
        srw_group.permissions.add(can_change)
        srw_group.permissions.add(can_delete)
