# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.db.utils import OperationalError
from django.utils.translation import gettext_lazy as _


class SocietyManagerConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_society_manager``."""
    name = 'mywebsite_society_manager'
    verbose_name = _("Society Manager")

    def ready(self):
        """Wire up the signals"""
        import mywebsite_society_manager.signals
        """Import module settings"""
        try:
            import mywebsite_society_manager.settings
        except OperationalError:
            pass
