# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

import os
import re

from django import template
from django.conf import settings as settings_file
from django.core.exceptions import SuspiciousFileOperation
from django.utils import formats

from mywebsite_home.templatetags.i18n_custom import do_translate_url

from mywebsite_society_manager.models import (
    ProspectiveCompany, ProspectiveCompanyLabels, Queue, SocietyMember, State, Ticket
)
from mywebsite_society_manager.rights import get_queues_as_q_for_queue_model

register = template.Library()


@register.filter
def abssub(value, arg):
    try:
        return abs(value - arg)
    except Exception:
        return 'error'


@register.filter
def get_type(value):
    if type(value) is bool:
        return 'checkbox'
    elif type(value) is int:
        return 'number'
    elif type(value) is str:
        return 'text'
    else:
        return type(value)


@register.simple_tag
def site_vars():
    site_vars_data = {}
    site_vars_data['name'] = settings_file.SITE_NAME
    site_vars_data['version'] = settings_file.SITE_VERSION
    site_vars_data['debug'] = settings_file.DEBUG
    return site_vars_data


@register.simple_tag
def app_settings():
    settings = {}
    settings['show_members'] = settings_file.SHOW_MEMBERS
    settings['show_members_payment_data'] = settings_file.SHOW_MEMBERS_PAYMENT_DATA
    settings['show_members_services'] = settings_file.SHOW_MEMBERS_SERVICES
    settings['show_microtasks'] = settings_file.SHOW_MICROTASKS
    settings['show_log'] = settings_file.SHOW_LOG
    settings['app_url_prefix'] = do_translate_url(settings_file.APP_URL_PREFIX)
    settings['MOMENT_DATETIME_FORMAT'] = formats.get_format('MOMENT_DATETIME_FORMAT')
    return settings


@register.simple_tag
def all_states():
    state_objs = State.objects.filter(is_archived=False)
    return state_objs


@register.simple_tag(takes_context=True, name='all_queues_for_user')
def all_queues_for_user(context, perm_level):
    request = context['request']
    try:
        queue_objs = Queue.objects.filter(get_queues_as_q_for_queue_model(
            request.user, perm_level))
    except Exception:
        queue_objs = Queue.objects.none()
    return queue_objs


@register.simple_tag
def all_labels():
    label_objs = ProspectiveCompanyLabels.objects.values_list('name', flat=True).order_by('name').distinct()
    return label_objs


@register.simple_tag()
def all_labels_of_prospect_or_member(form):
    if isinstance(form.instance, SocietyMember):
        prospect = form.instance.company
    elif isinstance(form.instance, ProspectiveCompany):
        prospect = form.instance
    else:
        prospect = None

    if prospect:
        label_objs = prospect.labels.values_list('name', flat=True).order_by('name').distinct()
        return label_objs
    else:
        return []


@register.simple_tag
def all_members():
    members = ProspectiveCompany.objects.filter(
        pk__in=SocietyMember.objects.all().values_list('company__pk', flat=True),
        is_archived=False
    ).order_by('name').values_list('name', flat=True).distinct()
    return list(members)


@register.simple_tag
def status_name(shortcode):
    status_obj_name = State.objects.get(shortcode=shortcode)
    return status_obj_name.name


@register.filter(name='filename_text')
def filename_text(value):
    try:
        filename = os.path.basename(value.file.name)
    except SuspiciousFileOperation:
        filename = value
    return filename


@register.filter(name='filename_icon')
def filename_icon(value):
    if bool(re.search('(bmp|gif|ico|jpg|jpeg|png|svg|tiff)$', value)):
        return value
    else:
        return '/static/society_manager/img/document.png'


@register.filter(name='sort')
def listsort(value):
    if isinstance(value, dict):
        a = []
        key_list = []
        # append all values w/o sub-items
        [key_list.append(val) for val in value.keys() if not isinstance(value[val], dict)]
        key_list = sorted(key_list)
        for key in key_list:
            a.append((key, value[key]))

        key_list = []
        # append all values w/ sub-items
        [key_list.append(val) for val in value.keys() if isinstance(value[val], dict)]
        key_list = sorted(key_list)
        for key in key_list:
            a.append((key, value[key]))
        return a
    elif isinstance(value, list):
        return sorted(value)
    else:
        return value


listsort.is_safe = True
