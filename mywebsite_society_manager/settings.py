# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json

from django.conf import settings as settings_file
from mywebsite_home.models import Settings
from django.db.utils import OperationalError, ProgrammingError

from mywebsite_society_manager.utils import get_object_or_none

if not hasattr(settings_file, 'CONTINUOUS_MEMBER_ID'):
    # set default
    settings_file.CONTINUOUS_MEMBER_ID = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'continuous_member_id' in json_dict:
                settings_file.CONTINUOUS_MEMBER_ID = json_dict['continuous_member_id']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'EMAILTEST'):
    # set default
    settings_file.EMAILTEST = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'emailtest' in json_dict:
                settings_file.EMAILTEST = json_dict['emailtest']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'EMAILTO'):
    # set default
    settings_file.EMAILTO = ''

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'emailto' in json_dict:
                settings_file.EMAILTO = json_dict['emailto']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'GLOBAL_QUEUE_SHORTCODE'):
    # set default
    settings_file.GLOBAL_QUEUE_SHORTCODE = None

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'global_queue_shortcode' in json_dict:
                settings_file.GLOBAL_QUEUE_SHORTCODE = json_dict['global_queue_shortcode']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'MEMBERS_NR_PREFIX'):
    # set default
    settings_file.MEMBERS_NR_PREFIX = ''

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'members' in json_dict and 'nr_prefix' in json_dict['members']:
                settings_file.MEMBERS_NR_PREFIX = json_dict['members']['nr_prefix']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'MEMBERS_NR_WIDTH'):
    # set default
    settings_file.MEMBERS_NR_WIDTH = 1

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'members' in json_dict and 'nr_width' in json_dict['members']:
                settings_file.MEMBERS_NR_WIDTH = json_dict['members']['nr_width']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'MEMBERS_QUEUE_SHORTCODE'):
    # set default
    settings_file.MEMBERS_QUEUE_SHORTCODE = None

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'members' in json_dict and 'queue_shortcode' in json_dict['members']:
                settings_file.MEMBERS_QUEUE_SHORTCODE = json_dict['members']['queue_shortcode']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'PROSPECTS_QUEUE_SHORTCODE'):
    # set default
    settings_file.PROSPECTS_QUEUE_SHORTCODE = None

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'prospects' in json_dict and 'queue_shortcode' in json_dict['prospects']:
                settings_file.PROSPECTS_QUEUE_SHORTCODE = json_dict['prospects']['queue_shortcode']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'SHOW_MEMBERS'):
    # set default
    settings_file.SHOW_MEMBERS = True

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_members' in json_dict:
                settings_file.SHOW_MEMBERS = json_dict['show_members']
    except (OperationalError, ProgrammingError):
        pass

if not hasattr(settings_file, 'SHOW_MEMBERS_PAYMENT_DATA'):
    # set default
    settings_file.SHOW_MEMBERS_PAYMENT_DATA = True

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_members_payment_data' in json_dict:
                settings_file.SHOW_MEMBERS_PAYMENT_DATA = json_dict['show_members_payment_data']
    except (OperationalError, ProgrammingError):
        pass

if not hasattr(settings_file, 'SHOW_MEMBERS_SERVICES'):
    # set default
    settings_file.SHOW_MEMBERS_SERVICES = True

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_members_services' in json_dict:
                settings_file.SHOW_MEMBERS_SERVICES = json_dict['show_members_services']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'SHOW_LOG'):
    # set default
    settings_file.SHOW_LOG = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_log' in json_dict:
                settings_file.SHOW_LOG = json_dict['show_log']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'SHOW_MAIL_POPUP'):
    # set default
    settings_file.SHOW_MAIL_POPUP = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_mail_popup' in json_dict:
                settings_file.SHOW_MAIL_POPUP = json_dict['show_mail_popup']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'SHOW_MICROTASKS'):
    # set default
    settings_file.SHOW_MICROTASKS = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'show_microtasks' in json_dict:
                settings_file.SHOW_MICROTASKS = json_dict['show_microtasks']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'APP_URL_PREFIX'):
    # set default
    settings_file.APP_URL_PREFIX = '/society_manager'

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'app_url_prefix' in json_dict:
                settings_file.APP_URL_PREFIX = json_dict['app_url_prefix']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'SYSTEM_USER'):
    # set default
    settings_file.SYSTEM_USER = False

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'system_user' in json_dict:
                settings_file.SYSTEM_USER = json_dict['system_user']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'TICKETS_NR_PREFIX'):
    # set default
    settings_file.TICKETS_NR_PREFIX = ''

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'tickets' in json_dict and 'nr_prefix' in json_dict['tickets']:
                settings_file.TICKETS_NR_PREFIX = json_dict['tickets']['nr_prefix']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'TICKETS_NR_WIDTH'):
    # set default
    settings_file.TICKETS_NR_WIDTH = 1

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'tickets' in json_dict and 'nr_width' in json_dict['tickets']:
                settings_file.TICKETS_NR_WIDTH = json_dict['tickets']['nr_width']
    except (OperationalError, ProgrammingError):
        pass


if not hasattr(settings_file, 'VERSION'):
    # set default
    settings_file.VERSION = '1'

    try:
        app_settings = get_object_or_none(Settings, name='society_manager', is_archived=False)

        if app_settings and app_settings.json:
            json_dict = app_settings.json if type(app_settings.json) is dict else json.loads(app_settings.json)

            if 'version' in json_dict:
                settings_file.VERSION = json_dict['version']
    except (OperationalError, ProgrammingError):
        pass
