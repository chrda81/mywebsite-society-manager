# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
import os
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, Group
from django.core.validators import ValidationError
from django.db import models
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import truncatechars
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django_fsm import FSMField, transition
from phonenumber_field.modelfields import PhoneNumberField
from sequences import get_last_value, get_next_value

from .utils import YearlessDateField

# User model alias
User = get_user_model()


# Abstract model
class TimeStampedModelMixin(models.Model):
    """
    Abstract Mixin model to add timestamp
    """
    # Timestamp
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('created_field')
    )
    updated = models.DateTimeField(
        auto_now=True,
        db_index=True,
        verbose_name=_('updated_field')
    )

    class Meta:
        abstract = True


# Model for basic subscription
class BasicSubscription(TimeStampedModelMixin):
    subscription = models.CharField(
        max_length=10,
        verbose_name=_('subscription_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_basicsubscription'
        verbose_name = _('Basic Subscription')
        verbose_name_plural = _('Basic Subscriptions')
        ordering = ['subscription']

    def __unicode__(self):
        return self.subscription

    def __str__(self):
        return self.subscription


# Model for country
class Country(TimeStampedModelMixin):
    name = models.CharField(
        max_length=50,
        verbose_name=_('name_field')
    )
    symbol = models.CharField(
        max_length=5,
        verbose_name=_('symbol_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_country'
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for enrollment fee
class EnrollmentFee(TimeStampedModelMixin):
    subscription = models.CharField(
        max_length=10,
        verbose_name=_('subscription_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_enrollmentfee'
        verbose_name = _('Enrollment Fee')
        verbose_name_plural = _('Enrollment Fees')
        ordering = ['subscription']

    def __unicode__(self):
        return self.subscription

    def __str__(self):
        return self.subscription


# Model for membership type
class MembershipType(TimeStampedModelMixin):
    name = models.CharField(
        max_length=100,
        verbose_name=_('name_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_membershiptype'
        verbose_name = _('Membership Type')
        verbose_name_plural = _('Membership Types')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for payment interval
class PaymentInterval(TimeStampedModelMixin):
    name = models.CharField(
        max_length=50,
        verbose_name=_('name_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_paymentinterval'
        verbose_name = _('Payment Interval')
        verbose_name_plural = _('Payment Intervals')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for payment type
class PaymentType(TimeStampedModelMixin):
    name = models.CharField(
        max_length=50,
        verbose_name=_('name_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_paymenttype'
        verbose_name = _('Payment Type')
        verbose_name_plural = _('Payment Types')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for prospective company
class ProspectiveCompany(TimeStampedModelMixin):
    name = models.CharField(
        max_length=250,
        verbose_name=_('prospect_name_field'),
        unique=True
    )
    additional_name = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('additional_name_field'),
    )
    contact_title = models.CharField(
        max_length=25,
        blank=True,
        verbose_name=_('contact_title_field')
    )
    contact_first_name = models.CharField(
        max_length=30,
        verbose_name=_('contact_first_name_field')
    )
    contact_last_name = models.CharField(
        max_length=150,
        verbose_name=_('contact_last_name_field')
    )
    country = models.ForeignKey(
        Country,
        related_name='company_rel',
        on_delete=models.CASCADE,
        verbose_name=_('country_field')
    )
    zipcode = models.CharField(
        max_length=10,
        verbose_name=_('zipcode_field')
    )
    location = models.CharField(
        max_length=250,
        verbose_name=_('location_field')
    )
    address = models.CharField(
        max_length=250,
        verbose_name=_('address_field')
    )
    website = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('website_field')
    )
    email = models.EmailField(
        blank=False,
        verbose_name=_('email_field')
    )
    mobile = PhoneNumberField(
        blank=True,
        null=True,
        verbose_name=_('mobile_field')
    )
    phone = PhoneNumberField(
        verbose_name=_('phone_field')
    )
    fax = PhoneNumberField(
        blank=True,
        null=True,
        verbose_name=_('fax_field')
    )
    sepa_bank_name = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('sepa_bank_name_field')
    )
    sepa_account_name = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('sepa_account_name_field')
    )
    sepa_iban = models.CharField(
        max_length=34,
        blank=True,
        null=True,
        verbose_name=_('sepa_iban_field')
    )
    sepa_bic = models.CharField(
        max_length=11,
        blank=True,
        null=True,
        verbose_name=_('sepa_bic_field')
    )
    is_applying_for_membership = models.BooleanField(
        default=False,
        verbose_name=_('is_applying_for_membership_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_prospective_company'
        verbose_name = _('Prospective Company')
        verbose_name_plural = _('Prospective Companies')
        ordering = ['name']

    def __unicode__(self):
        return f"{self.name} ({self.contact_last_name}, {self.contact_first_name})"

    def __str__(self):
        return f"{self.name} ({self.contact_last_name}, {self.contact_first_name})"

    def as_json(self):
        return {
            _('prospect_name_field'): str(self.name),
            _('additional_name_field'): str(self.additional_name),
            _('contact_title_field'): str(self.contact_title),
            _('contact_first_name_field'): str(self.contact_first_name),
            _('contact_last_name_field'): str(self.contact_last_name),
            _('address_field'): str(self.address),
            _('zipcode_field'): str(self.zipcode),
            _('location_field'): str(self.location),
            _('country_field'): str(self.country),
            _('website_field'): str(self.website),
            _('email_field'): str(self.email),
            _('mobile_field'): str(self.mobile),
            _('phone_field'): str(self.phone),
            _('fax_field'): str(self.fax),
            _('sepa_bank_name_field'): str(self.sepa_bank_name),
            _('sepa_account_name_field'): str(self.sepa_account_name),
            _('sepa_bic_field'): str(self.sepa_bic),
            _('sepa_iban_field'): str(self.sepa_iban),
            _('created_field'): str(self.created.strftime(settings.DATE_FORMAT_JSON)),
        }


# Model for ProspectiveCompanyLabels
class ProspectiveCompanyLabels(TimeStampedModelMixin):
    name = models.CharField(
        max_length=250,
        verbose_name=_('name_field')
    )
    company = models.ForeignKey(
        ProspectiveCompany,
        related_name='labels',
        on_delete=models.CASCADE,
        verbose_name=_('company_field')
    )
    tagify_data = models.CharField(
        max_length=255,
        verbose_name=_('tagify_data_field'),
    )

    class Meta:
        db_table = 'society_manager_prospective_company_labels'
        verbose_name = _('Prospective Company Label')
        verbose_name_plural = _('Prospective Company Labels')
        ordering = ['name']

    def __unicode__(self):
        return f"{self.name} ({self.company.name})"

    def __str__(self):
        return f"{self.name} ({self.company.name})"


# Model for service
class Service(TimeStampedModelMixin):
    name = models.CharField(
        max_length=125,
        verbose_name=_('name_field')
    )
    description = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_('description_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_service'
        verbose_name = _('Service')
        verbose_name_plural = _('Services')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


# Model for society member
class SocietyMember(TimeStampedModelMixin):
    nr = models.CharField(
        max_length=10,
        unique=True,
        verbose_name=_('member_nr_field')
    )
    company = models.OneToOneField(
        ProspectiveCompany,
        related_name='members',
        on_delete=models.CASCADE,
        verbose_name=_('company_field')
    )
    issue_date = models.DateTimeField(
        default=datetime.strptime('1970-01-01 00:00', '%Y-%m-%d %H:%M'),
        verbose_name=_('issue_date_field')
    )
    exit_date = models.DateTimeField(
        default=datetime.strptime('1970-01-01 00:00', '%Y-%m-%d %H:%M'),
        verbose_name=_('exit_date_field')
    )
    recommendation = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('recommendation_field')
    )
    notes = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('notes_field')
    )
    membership = models.ForeignKey(
        MembershipType,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('membership_field')
    )
    consultant = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('consultant_field')
    )
    enrollment_fee = models.ForeignKey(
        EnrollmentFee,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('enrollment_fee_field')
    )
    basic_subscription = models.ForeignKey(
        BasicSubscription,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('basic_subscription_field')
    )
    payment_type = models.ForeignKey(
        PaymentType,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('payment_type_field')
    )
    payment_interval = models.ForeignKey(
        PaymentInterval,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('payment_interval_field')
    )
    payment_due_date = YearlessDateField(
        blank=True,
        null=True,
        verbose_name=_('payment_due_date_field')
    )
    accounts_receivable_nr = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        verbose_name=_('accounts_receivable_nr_field')
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name=_('is_active_field')
    )
    is_paying = models.BooleanField(
        default=True,
        verbose_name=_('is_paying_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_societymember'
        verbose_name = _('Society Member')
        verbose_name_plural = _('Society Members')
        ordering = ['nr']

    def __unicode__(self):
        return self.nr

    def __str__(self):
        return self.nr

    def get_next_id(self):
        if settings.CONTINUOUS_MEMBER_ID:
            # we want to increment the members sequence only on saving a new member instance
            return get_last_value('members') + 1
        else:
            return get_next_value('members')

    def as_json(self):
        return {
            _('member_nr_field'): str(self.nr),
            _('company_field'): str(self.company),
            _('issue_date_field'): str(self.issue_date.strftime(settings.DATE_FORMAT_JSON)),
            _('exit_date_field'): str(self.exit_date.strftime(settings.DATE_FORMAT_JSON)),
            _('recommendation_field'): str(self.recommendation),
            _('membership_field'): str(self.membership),
            _('consultant_field'): str(self.consultant),
            _('enrollment_fee_field'): str(self.enrollment_fee),
            _('basic_subscription_field'): str(self.basic_subscription),
            _('payment_type_field'): str(self.payment_type),
            _('payment_interval_field'): str(self.payment_interval),
            _('payment_due_date_field'): str(self.payment_due_date),
            _('accounts_receivable_nr_field'): str(self.accounts_receivable_nr),
            _('is_active_field'): str(self.is_active),
            _('is_paying_field'): str(self.is_paying),
            _('created_field'): str(self.created.strftime(settings.DATE_FORMAT_JSON)),
        }


# Model for used services
class UsedServices(TimeStampedModelMixin):
    service = models.ForeignKey(
        Service,
        related_name='used_services',
        on_delete=models.CASCADE,
        verbose_name=_('service_field')
    )
    member = models.ForeignKey(
        SocietyMember,
        related_name='used_services',
        on_delete=models.CASCADE,
        verbose_name=_('service_member_field')
    )
    description = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_('description_field')
    )
    is_checked = models.BooleanField(
        default=False,
        verbose_name=_('is_checked_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_used_services'
        verbose_name = _('UsedServices')
        verbose_name_plural = _('UsedServicess')

    def __unicode__(self):
        return f"{self.service.name} ({self.member.nr})"

    def __str__(self):
        return f"{self.service.name} ({self.member.nr})"

    def as_json(self):
        return {
            _('name_field'): str(self.service.name),
            # _('service_member_field'): '%s (%s)' % (str(self.member.company.name), str(self.member.nr)),
            _('description_field'): str(self.description),
            _('is_checked_field'): str(self.is_checked),
            _('is_archived_field'): str(self.is_archived),
            _('updated_field'): str(self.updated.strftime(settings.DATE_FORMAT_JSON)),
        }


# Model for UserType (Operator or simple user)
class UserType(TimeStampedModelMixin):
    status = models.CharField(
        max_length=20
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_usertype'
        verbose_name = _('User Type')
        verbose_name_plural = _('User Types')

    def __str__(self):
        return self.status

    def __unicode__(self):
        return self.status


# Model for Profile class
class Profile(TimeStampedModelMixin):
    user = models.OneToOneField(
        User,
        related_name='profile_society_manager',
        on_delete=models.CASCADE,
        verbose_name=_('user_field')
    )
    status_rel = models.ForeignKey(
        UserType,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_('status_rel_field')
    )
    avatar = models.FileField(
        upload_to='avatar/',
        null=True,
        blank=True,
        verbose_name=_('avatar_field')
    )
    ticket_columns = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name=_('ticket_columns_field')
    )
    prospect_columns = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name=_('prospect_columns_field')
    )
    member_columns = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name=_('member_columns_field')
    )
    rssfeed = models.CharField(
        max_length=400,
        null=True,
        blank=True,
        verbose_name=_('rssfeed_field')
    )
    has_collapsednavbar = models.BooleanField(
        default=False,
        verbose_name=_('has_collapsednavbar_field')
    )

    class Meta:
        db_table = 'society_manager_profile'
        ordering = ['user']
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username


# Model for Queue
class Queue(TimeStampedModelMixin):
    name = models.CharField(
        max_length=100,
        verbose_name=_('name_field')
    )
    shortcode = models.CharField(
        max_length=16,
        verbose_name=_('shortcode_field')
    )
    description = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        verbose_name=_('description_field')
    )
    is_changeable = models.BooleanField(
        default=True,
        verbose_name=_('is_changeable_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_queue'
        verbose_name = _('Queue')
        verbose_name_plural = _('Queues')
        ordering = ['name']

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


# Model for Rights
class Rights(TimeStampedModelMixin):
    is_enabled = models.BooleanField(
        default=True,
        verbose_name=_('is_enabled_field')
    )
    grp_src = models.ForeignKey(
        Group,
        related_name="src_grp",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('grp_src_field')
    )
    queue_dst = models.ForeignKey(
        Queue,
        related_name="dst_queue",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('queue_dst_field')
    )
    # Permited actions
    can_view = models.BooleanField(
        default=False,
        verbose_name=_('can_view_field')
    )
    can_create = models.BooleanField(
        default=False,
        verbose_name=_('can_create_field')
    )
    can_archive = models.BooleanField(
        default=False,
        verbose_name=_('can_archive_field')
    )
    can_edit = models.BooleanField(
        default=False,
        verbose_name=_('can_edit_field')
    )
    can_comment = models.BooleanField(
        default=False,
        verbose_name=_('can_comment_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_right'
        verbose_name = _('Rights')
        verbose_name_plural = _('Rightss')
        ordering = ['grp_src', 'queue_dst']

    def __unicode__(self):
        return f"{self.grp_src.name} ({self.queue_dst.name})"

    def __str__(self):
        return f"{self.grp_src.name} ({self.queue_dst.name})"

    """
    Check at database state if registry is created or we can create it:
    If you use the admin, to mantain the non-duplicity of the rules, we make a
    secondary check at time to save the object to the DB.
    Yes, you have 2 queries but this is the unique way to avoid errors if you
    use the admin panel to insert some righths
    """

    def detect_rights_exists(self, grp, queue):
        return_query = {}
        obj_query = Rights.objects.filter(grp_src=grp, queue_dst=queue)
        if obj_query:
            return_query['status'] = True
            return_query['numbers'] = [i.id for i in obj_query]
            return return_query
        else:
            return_query['status'] = False
            return return_query

    # Only for new records (self.pk check)
    def save(self, *args, **kwargs):
        detect_function = self.detect_rights_exists(
            self.grp_src, self.queue_dst)
        if not self.pk and detect_function['status']:
            raise ValidationError(
                _("Rule already created: model output ") + str(
                    detect_function['numbers']) + "")
        else:
            super(Rights, self).save(*args, **kwargs)


# Model for State
class State(TimeStampedModelMixin):
    name = models.CharField(
        max_length=30,
        verbose_name=_('name_field')
    )
    shortcode = models.CharField(
        max_length=16,
        verbose_name=_('shortcode_field')
    )
    description = models.CharField(
        max_length=150,
        null=True,
        blank=True,
        verbose_name=_('description_field')
    )
    color = models.CharField(
        default='008ac6',
        max_length=10,
        null=True,
        blank=True,
        verbose_name=_('color_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_state'
        verbose_name = _('State')
        verbose_name_plural = _('States')
        ordering = ['pk']

    def save(self, *args, **kwargs):
        if self.color.startswith("# "):
            self.color = self.color[1:]
        super(State, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


# Model for Priority
class Priority(TimeStampedModelMixin):
    name = models.CharField(
        max_length=30,
        verbose_name=_('name_field')
    )
    shortcode = models.CharField(
        max_length=16,
        verbose_name=_('shortcode_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_priority'
        verbose_name = _('Priority')
        verbose_name_plural = _('Priorities')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


# Model for Ticket
class Ticket(TimeStampedModelMixin):
    nr = models.CharField(
        max_length=10,
        unique=True,
        verbose_name=_('ticket_nr_field')
    )
    create_user = models.ForeignKey(
        User,
        related_name="c_user",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('create_user_field')
    )
    subject = models.CharField(
        max_length=128,
        verbose_name=_('subject_field')
    )
    body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('ticket_body_field')
    )
    assigned_user = models.ForeignKey(
        User,
        related_name="a_user",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('assigned_user_field')
    )
    assigned_queue = models.ForeignKey(
        Queue,
        related_name="rel_ticket",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('assigned_queue_field')
    )
    assigned_company = models.ForeignKey(
        ProspectiveCompany,
        related_name="rel_ticket",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('assigned_company_field')
    )
    assigned_state = models.ForeignKey(
        State,
        on_delete=models.CASCADE,
        verbose_name=_('assigned_state_field')
    )
    assigned_prio = models.ForeignKey(
        Priority,
        related_name="rel_ticket",
        on_delete=models.CASCADE,
        verbose_name=_('assigned_prio_field')
    )
    pending = models.DateTimeField(
        default=datetime.strptime('1970-01-01 00:00', '%Y-%m-%d %H:%M'),
        verbose_name=_('pending_field')
    )
    percentage = models.IntegerField(
        default=0,
        blank=True,
        null=True,
        verbose_name=_('percentage_field')
    )
    labels = models.CharField(
        max_length=2048,
        blank=True,
        verbose_name=_('labels_field')
    )
    fsm_state = FSMField(
        default='new',
        blank=True,
        verbose_name=_('fsm_state_field')
    )
    is_mailed = models.BooleanField(
        default=False,
        verbose_name=_('is_mailed_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_ticket'
        verbose_name = _('Ticket')
        verbose_name_plural = _('Tickets')
        ordering = ['created']
        # unique_together = ('subject', 'assigned_company')

    def __unicode__(self):
        return self.nr

    def __str__(self):
        return self.nr

    def get_next_id(self):
        return get_next_value('tickets')

    """Return a empty string to avoid problems in JSON serialization"""

    def str_assigned_user_name(self):
        try:
            assigned_user_data = "" + str(self.assigned_user.first_name) + \
                " " + str(self.assigned_user.last_name) + ""
        except Exception:
            assigned_user_data = ""
        return assigned_user_data

    def str_creator_user_name(self):
        try:
            creator_user_data = "" + str(self.create_user.first_name) + " " + \
                str(self.create_user.last_name) + ""
        except Exception:
            creator_user_data = ""
        return creator_user_data

    def get_label_list(self):
        # collect string from tagify data and remove possible commas, whitespaces
        # at the begin or end to jsonify the clean data
        dirty_labels = self.labels.strip(' ,')
        cleaned_labels = ''
        if str(dirty_labels):
            cleaned_labels_json = json.loads(dirty_labels)
            for tagify_data in cleaned_labels_json:
                value = tagify_data.get('value', None)
                # append tagify value to string list
                cleaned_labels = '{},{}'.format(cleaned_labels, value)
            return cleaned_labels.strip(' ,')
        else:
            return ''

    def get_label_data(self, content):
        # collect string from tagify data and remove possible commas, whitespaces
        # at the begin or end to jsonify the clean data
        dirty_labels = self.labels.strip(' ,')
        if str(dirty_labels):
            cleaned_labels_json = json.loads(dirty_labels)
            for tagify_data in cleaned_labels_json:
                # find content in json values
                if tagify_data['value'] == content:
                    # build json like string without encoding to utf-8, which tagify understands
                    json_str = str(tagify_data).replace("'", '"')
                    json_str = "[{}]".format(json_str)
                    return json_str
        else:
            return ''

    def as_json(self):
        return dict(
            id=str(self.id),
            nr=str(self.nr),
            date=str(self.created.strftime(settings.DATE_FORMAT_JSON)),
            subject_data=str(self.subject),
            body_data=str(self.body),
            state_data=str(self.assigned_state.name),
            state_data_id=str(self.assigned_state.id),
            state_color_data=str(self.assigned_state.color),
            percentage_data=str(self.percentage),
            queue_shortcode=str(self.assigned_queue.shortcode),
            priority=str(self.assigned_prio),
            create_user=self.str_creator_user_name(),
            assigned_user_data=self.str_assigned_user_name(),
        )

    """ Define properties here """

    @property
    def str_create_user(self):
        if self.create_user is None:
            return '--'
        else:
            return self.create_user.username

    @property
    def str_assigned_user(self):
        if self.assigned_user is None:
            return '--'
        else:
            return self.assigned_user.username

    @property
    def can_send_mail(self):
        send_mail = False
        if self.assigned_company and self.assigned_company.email:
            send_mail = True

        return send_mail

    @property
    def needs_to_send_mail(self):
        send_mail = False

        if self.can_send_mail and not self.is_mailed and settings.SHOW_MAIL_POPUP:
            send_mail = True

        return send_mail

    @property
    def previous_instance(self):
        # get previous ticket object
        old_ticket_instance = None
        if self.id:
            old_ticket_instance = get_object_or_404(Ticket, pk=self.id)

        return old_ticket_instance

    """ Define FSM transistion methods here """

    # New: Ticket instance doesn't exist yet
    @transition(field=fsm_state, source='new', target='new')
    def new(self):
        self.assigned_state = get_object_or_404(State, shortcode='open')

    # Open: Ticket is open
    @transition(field=fsm_state, source='*', target='open')
    def open(self):
        self.assigned_state = get_object_or_404(State, shortcode='open')

        # change assigned_state: open => in_progress
        if self.previous_instance and self.previous_instance.assigned_state.shortcode == "open" and \
                self.assigned_user is not None and self.fsm_state != 'new':
            self.in_progress()

        # change assigned_state: closed => open
        if self.previous_instance and self.previous_instance.assigned_state.shortcode == "closed":
            # set percentage value to 0%
            self.percentage = 0

    # In Progress: Ticket is in progress
    @transition(field=fsm_state, source=['new', 'open', 'in_progress', 'is_pending'], target='in_progress')
    def in_progress(self):
        self.assigned_state = get_object_or_404(State, shortcode='in_progress')

        # change assigned_state: in_progress => Open
        if self.previous_instance and self.previous_instance.assigned_state.shortcode == "in_progress" and \
                self.previous_instance.assigned_user != self.assigned_user:
            self.open()

    # Is Pending: Ticket is pending
    @transition(field=fsm_state, source=['new', 'open', 'in_progress', 'is_pending'], target='is_pending')
    def is_pending(self):
        self.assigned_state = get_object_or_404(State, shortcode='is_pending')

    # Closed: Ticket is closed
    @transition(field=fsm_state, source='*', target='closed')
    def close(self):
        self.assigned_state = get_object_or_404(State, shortcode='closed')

        # set percentage value to 100%
        self.percentage = 100

        # reset mail state
        if self.can_send_mail:
            self.is_mailed = False


#  Method to store tickets attachments inside folders called with related ticket id
def get_attachment_upload_path(instance, filename):
    return os.path.join(settings.FILEBROWSER_DIRECTORY,
                        "ticket_files/%s" % instance.ticket_rel.id, filename)


# Model for Attachment
class Attachment(TimeStampedModelMixin):
    ticket_rel = models.ForeignKey(
        Ticket,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_('ticket_rel_field')
    )
    file_name = models.FileField(
        upload_to=get_attachment_upload_path,
        null=True,
        blank=True,
        max_length=500,
        verbose_name=_('file_name_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_attachment'
        verbose_name = _('Attachment')
        verbose_name_plural = _('Attachments')

    def __unicode__(self):
        return f"{self.file_name} ({self.ticket_rel.nr})"

    def __str__(self):
        return f"{self.file_name} ({self.ticket_rel.nr})"

    def shortfilename(self):
        return os.path.basename(self.file_name.name)


# Model for Comments
class Comments(TimeStampedModelMixin):
    ticket_rel = models.ForeignKey(
        Ticket,
        related_name='ticket_rel_comm',
        on_delete=models.CASCADE,
        verbose_name=_('ticket_rel_field')
    )
    user_rel = models.ForeignKey(
        User,
        related_name='user_rel_comm',
        on_delete=models.CASCADE,
        verbose_name=_('user_rel_field')
    )
    comment = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('comment_field')
    )
    is_mailed = models.BooleanField(
        default=False,
        verbose_name=_('is_mailed_field')
    )
    is_private = models.BooleanField(
        default=False,
        verbose_name=_('is_private_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_comments'
        verbose_name = _('Comments')
        verbose_name_plural = _('Commentss')

    def __unicode__(self):
        return f"{truncatechars(self.comment, 30)} ({self.ticket_rel.nr})"

    def __str__(self):
        return f"{truncatechars(self.comment, 30)} ({self.ticket_rel.nr})"


# Model for Microtasks
class Microtasks(TimeStampedModelMixin):
    ticket_rel = models.ForeignKey(
        Ticket,
        related_name='ticket_rel_mtask',
        on_delete=models.CASCADE,
        verbose_name=_('ticket_rel_field')
    )
    subject = models.CharField(
        max_length=40,
        verbose_name=_('subject_field')
    )
    body = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('microtask_body_field')
    )
    assigned_state = models.ForeignKey(
        State,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_('assigned_state_field')
    )
    percentage = models.IntegerField(
        default=0,
        blank=True,
        null=True,
        verbose_name=_('percentage_field')
    )
    fsm_state = FSMField(
        default='new',
        blank=True,
        verbose_name=_('fsm_state_field')
    )
    is_mailed = models.BooleanField(
        default=False,
        verbose_name=_('is_mailed_field')
    )
    is_archived = models.BooleanField(
        default=False,
        verbose_name=_('is_archived_field')
    )

    class Meta:
        db_table = 'society_manager_microtasks'
        verbose_name = _('Microtasks')
        verbose_name_plural = _('Microtaskss')

    def __unicode__(self):
        return f"{truncatechars(self.subject, 30)} ({self.ticket_rel.nr})"

    def __str__(self):
        return f"{truncatechars(self.subject, 30)} ({self.ticket_rel.nr})"

    def as_json(self):
        return dict(
            # UTF8 in order to avoid encoding problems
            id=str(self.id),
            subject_data=str(self.subject),
            body_data=str(self.body),
            state_data=str(self.assigned_state),
            state_data_id=str(self.assigned_state.id),
            state_color_data=str(self.assigned_state.color),
            date_data=str(self.created.strftime('%d/%m/%y %H:%M:%S')),
            percentage_data=int(self.percentage)
        )
