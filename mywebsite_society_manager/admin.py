# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2019 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group, User
from django.utils.translation import gettext_lazy as _

from jet.filters import DateRangeFilter
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberInternationalFallbackWidget, PhoneNumberPrefixWidget
from related_admin import RelatedFieldAdmin
from reversion_compare.admin import CompareVersionAdmin

from . import models
from .forms import TicketAdminForm
from .mixins import ExportCsvMixin


@admin.register(models.BasicSubscription)
class BasicSubscriptionAdmin(CompareVersionAdmin):
    list_display = ('subscription', 'updated', 'is_archived')
    search_fields = ('subscription',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Country)
class CountryAdmin(CompareVersionAdmin):
    list_display = ('name', 'symbol', 'updated', 'is_archived')
    search_fields = ('name', 'symbol',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.EnrollmentFee)
class EnrollmentFeeAdmin(CompareVersionAdmin):
    list_display = ('subscription', 'updated', 'is_archived')
    search_fields = ('subscription',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.MembershipType)
class MembershipTypeAdmin(CompareVersionAdmin):
    list_display = ('name', 'updated', 'is_archived')
    search_fields = ('name',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.PaymentInterval)
class PaymentIntervalAdmin(CompareVersionAdmin):
    list_display = ('name', 'updated', 'is_archived')
    search_fields = ('name',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.PaymentType)
class PaymentTypeAdmin(CompareVersionAdmin):
    list_display = ('name', 'updated', 'is_archived')
    search_fields = ('name',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.ProspectiveCompany)
class ProspectiveCompanyAdmin(CompareVersionAdmin, ExportCsvMixin):
    list_display = ('name', 'contact_last_name', 'contact_first_name', 'is_applying_for_membership', 'created',
                    'updated', 'is_archived')
    search_fields = ('name', 'contact_last_name', 'contact_first_name',)
    list_editable = ('is_applying_for_membership', 'is_archived',)
    list_filter = ('is_archived', 'is_applying_for_membership',
                   ('updated', DateRangeFilter), ('created', DateRangeFilter))
    formfield_overrides = {
        PhoneNumberField: {'widget': PhoneNumberPrefixWidget},
    }
    actions = ["export_as_csv"]


@admin.register(models.ProspectiveCompanyLabels)
class ProspectiveCompanyLabelsAdmin(CompareVersionAdmin):
    list_display = ('name', 'company', 'updated')
    search_fields = ('name', 'company')


@admin.register(models.SocietyMember)
class SocietyMemberAdmin(RelatedFieldAdmin, CompareVersionAdmin, ExportCsvMixin):
    list_display = ('id', 'nr', 'company', 'company__contact_last_name',
                    'membership', 'is_active', 'is_paying', 'created', 'updated', 'is_archived')
    search_fields = ('nr', 'company__name', 'company__contact_last_name',)
    list_editable = ('is_active', 'is_paying', 'is_archived')
    list_filter = ('is_archived', 'is_paying', 'is_active', ('updated', DateRangeFilter), ('created', DateRangeFilter))
    actions = ["export_as_csv"]


@admin.register(models.UserType)
class UserTypeAdmin(CompareVersionAdmin):
    list_display = ('status', 'updated', 'is_archived')
    search_fields = ('status',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Profile)
class ProfileAdmin(CompareVersionAdmin):
    list_display = ('user', 'status_rel', 'avatar', 'rssfeed', 'has_collapsednavbar', 'updated')
    search_fields = ('user',)
    list_editable = ('has_collapsednavbar',)


@admin.register(models.Queue)
class QueueAdmin(CompareVersionAdmin):
    list_display = ('name', 'shortcode', 'description', 'updated', 'is_archived')
    search_fields = ('name', 'shortcode', 'description',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Rights)
class RightsAdmin(CompareVersionAdmin):
    list_display = ('id', 'grp_src', 'queue_dst', 'can_view', 'can_create', 'can_archive', 'can_edit', 'can_comment',
                    'is_enabled', 'updated', 'is_archived')
    search_fields = ('grp_src', 'queue_dst',)
    list_editable = ('can_view', 'can_create', 'can_archive', 'can_edit', 'can_comment', 'is_enabled', 'is_archived')
    list_filter = ('is_enabled', 'is_archived')


@admin.register(models.State)
class StateAdmin(CompareVersionAdmin):
    list_display = ('name', 'shortcode', 'description', 'color', 'updated', 'is_archived')
    search_fields = ('name', 'shortcode', 'description',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Priority)
class PriorityAdmin(CompareVersionAdmin):
    list_display = ('name', 'shortcode', 'updated', 'is_archived')
    search_fields = ('name',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Ticket)
class TicketAdmin(CompareVersionAdmin, ExportCsvMixin):
    form = TicketAdminForm
    list_display = ('id', 'nr', 'subject', 'assigned_state', 'assigned_queue',
                    'create_user', 'assigned_user', 'assigned_prio', 'is_mailed', 'created', 'updated', 'is_archived')
    search_fields = ('nr', 'subject', 'body')
    list_editable = ('is_mailed', 'is_archived')
    list_filter = ('is_archived', 'is_mailed', 'create_user', 'assigned_user',
                   ('updated', DateRangeFilter), ('created', DateRangeFilter))
    actions = ["export_as_csv"]


@admin.register(models.Attachment)
class AttachmentAdmin(CompareVersionAdmin):
    list_display = ('ticket_rel', 'file_name', 'updated', 'is_archived')
    search_fields = ('ticket_rel__nr', 'file_name',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Comments)
class CommentsAdmin(CompareVersionAdmin, ExportCsvMixin):
    list_display = ('ticket_rel', 'user_rel', 'comment', 'is_mailed',
                    'is_private', 'created', 'updated', 'is_archived')
    search_fields = ('ticket_rel__nr', 'comment',)
    list_editable = ('is_mailed', 'is_private', 'is_archived')
    list_filter = ('is_archived', 'is_mailed', 'is_private', 'user_rel',
                   ('updated', DateRangeFilter), ('created', DateRangeFilter))
    actions = ["export_as_csv"]


@admin.register(models.Microtasks)
class MicrotaskAdmin(CompareVersionAdmin):
    list_display = ('ticket_rel', 'subject', 'body', 'assigned_state', 'percentage', 'updated', 'is_archived')
    search_fields = ('ticket_rel__nr', 'subject', 'body')
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.Service)
class ServiceAdmin(CompareVersionAdmin):
    list_display = ('name', 'description', 'updated', 'is_archived')
    search_fields = ('name', 'description',)
    list_editable = ('is_archived',)
    list_filter = ('is_archived',)


@admin.register(models.UsedServices)
class UsedServicesAdmin(CompareVersionAdmin):
    list_display = ('service', 'member', 'description', 'is_checked', 'updated', 'is_archived')
    search_fields = ('name', 'description',)
    list_editable = ('is_checked', 'is_archived')
    list_filter = ('is_archived', 'is_checked', 'service')


# re-add UserAdmin and GroupAdmin for reversion
class MyUserAdmin(CompareVersionAdmin, UserAdmin):
    pass


class MyGroupAdmin(CompareVersionAdmin, GroupAdmin):
    pass


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(User, MyUserAdmin)
admin.site.register(Group, MyGroupAdmin)
