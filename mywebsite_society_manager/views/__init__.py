# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings as settings_file
from django.contrib import messages
from django.contrib.auth import decorators, get_user_model
from django.db import models
from django.db.utils import OperationalError, ProgrammingError
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from mywebsite_home.models import Settings
from reversion.views import create_revision

from mywebsite_society_manager import rights
from mywebsite_society_manager.models import ProspectiveCompanyLabels, Ticket
from mywebsite_society_manager.rssfetcher import DashboardFeed
from mywebsite_society_manager.utils import query_view

# from notifications.models import Notification
# from notifications.utils import slug2id


# User model alias
User = get_user_model()


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def index(request):
    """
    Ticket listing
    """
    queues = rights.get_queues_as_q_for_ticket_model(request.user)
    # We pass always granted_queues as a roundup to query_view requirements
    # Assuming possible states are 'open', 'in_progress', 'is_pending' and 'closed'
    open_tickets = query_view(
        Ticket, request.GET, granted_queues=queues, assigned_state__shortcode='open', limit=5,
        excludes=models.Q(assigned_user_id=request.user.id))
    pending_tickets = query_view(
        Ticket, request.GET, granted_queues=queues, assigned_state__shortcode='is_pending', pending__lte=timezone.now(),
        limit=5)
    my_tickets = query_view(
        Ticket, request.GET, granted_queues=queues, assigned_state__shortcode__in=('open', 'in_progress'),
        assigned_user_id=request.user.id, limit=5)

    # rssdata = User.objects.get(id=request.user.id).rssfeed
    # if rssdata:
    #     rssfeed = DashboardFeed(rssdata).fetcher()
    return render(request, 'society_manager/dashboard/index.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def settings(request):
    return render(request, 'society_manager/settings/settings.html')


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def view_appsettings(request):
    if request.user.is_superuser:
        settings = get_object_or_404(Settings, name='society_manager', is_archived=False)
        return render(request, 'society_manager/settings/appsettings.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# helper for convert POST values to correct settings type
def convert_appsettings_post_valuetype(value, valuetype):
    if valuetype is bool:
        return bool(value)
    elif valuetype is int:
        return int(value)
    elif valuetype is dict:
        return dict(value)

    return str(value)


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
# @create_revision(manage_manually=False, using=None, atomic=True)
def save_appsettings(request):
    if request.user.is_superuser:
        # POST mode
        if request.method == 'POST':
            settings = {}
            try:
                # get last active dataset
                settings = Settings.objects.filter(name='society_manager', is_archived=False).latest('updated')
            except (models.ObjectDoesNotExist, OperationalError, ProgrammingError):
                pass

            # create settings from request
            for (key, value) in settings.json.items():
                if type(value) is dict:
                    for (subkey, subvalue) in value.items():
                        _key = '%s-%s' % (key, subkey)
                        if _key in request.POST:
                            post_value = request.POST[_key]
                            settings.json[key][subkey] = convert_appsettings_post_valuetype(post_value, type(subvalue))

                            # update django.conf.settings
                            conf_name = '%s_%s' % (str.upper(key), str.upper(subkey))
                            setattr(settings_file, conf_name, settings.json[key][subkey])

                        # check for missing settings of type bool in request.POST
                        # assuming the value is set to false in the GUI, but the key is not submitted via POST
                        elif type(value) is bool:
                            settings.json[key][subkey] = False

                            # update django.conf.settings
                            conf_name = '%s_%s' % (str.upper(key), str.upper(subkey))
                            setattr(settings_file, conf_name, False)

                else:
                    if key in request.POST:
                        post_value = request.POST[key]
                        settings.json[key] = convert_appsettings_post_valuetype(post_value, type(value))

                        # update django.conf.settings
                        conf_name = str.upper(key)
                        setattr(settings_file, conf_name, settings.json[key])

                    # check for missing settings of type bool in request.POST
                    # assuming the value is set to false in the GUI, but the key is not submitted via POST
                    elif type(value) is bool:
                        settings.json[key] = False

                        # update django.conf.settings
                        conf_name = str.upper(key)
                        setattr(settings_file, conf_name, False)

            settings.save()

        return render(request, 'society_manager/settings/appsettings.html', locals())

    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:appsettings-view')


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def get_labels_list_flat_jx(request):
    data = list()
    for item in ProspectiveCompanyLabels.objects.all().order_by('name').distinct().values_list('name', flat=True):
        data.append(item)

    return JsonResponse(data, safe=False)
