# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import GroupForm


# List groups
def list_groups(request):
    if request.user.is_superuser:
        group_list = Group.objects.all().order_by("name")
        return render(request, 'society_manager/groups/list_groups.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


def manage_group(request, group_id=None):
    if request.user.is_superuser:
        if group_id:
            actual_group = get_object_or_404(Group, pk=group_id)
        else:
            actual_group = Group()

        # POST mode
        if request.method == 'POST':
            form = GroupForm(request.POST, instance=actual_group)
            if form.is_valid():
                form.save()
                return redirect('society_manager:group-list')
        else:
            # Non-POST mode, show only
            form = GroupForm(instance=actual_group)

        return render(request, 'society_manager/groups/create_edit_group.html', locals())

    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:group-list')


# Delete group
def delete_group(request, group_id=None):
    if request.user.is_superuser:
        if group_id:
            actual_group = get_object_or_404(Group, pk=group_id)
            actual_group.delete()
            return redirect('society_manager:group-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to delete this setting'))
        return redirect('society_manager:group-list')
