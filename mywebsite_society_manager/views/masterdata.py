# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import (BasicSubscriptionForm,
                                             CountryForm, EnrollmentFeeForm,
                                             MembershipTypeForm,
                                             PaymentIntervalForm,
                                             PaymentTypeForm, ServiceForm)
from mywebsite_society_manager.models import (BasicSubscription, Country,
                                              EnrollmentFee, MembershipType,
                                              PaymentInterval, PaymentType,
                                              Service)


# List countries
def list_countries(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        countries_list = Country.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_countries.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit countries
def manage_countries(request, country_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if country_id:
            actual_country = get_object_or_404(Country, pk=country_id)
        else:
            actual_country = Country()
        # POST mode
        if request.method == 'POST':
            form = CountryForm(request.POST, instance=actual_country)
            if form.is_valid():
                form.save()
                return redirect('society_manager:countries-list')
        else:
            # Non-POST mode, show only
            form = CountryForm(instance=actual_country)
        return render(request, 'society_manager/masterdata/create_edit_country.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:countries-list')


# Archive countries
def archive_countries(request, country_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if country_id:
            actual_country = get_object_or_404(Country, pk=country_id)
            actual_country.is_archived = True
            actual_country.save()
            return redirect('society_manager:countries-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:countries-list')


# List services
def list_services(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        services_list = Service.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_services.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit service
def manage_services(request, service_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if service_id:
            actual_service = get_object_or_404(Service, pk=service_id)
        else:
            actual_service = Service()
        # POST mode
        if request.method == 'POST':
            form = ServiceForm(request.POST, instance=actual_service)
            if form.is_valid():
                form.save()
                return redirect('society_manager:services-list')
        else:
            # Non-POST mode, show only
            form = ServiceForm(instance=actual_service)
        return render(request, 'society_manager/masterdata/create_edit_service.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:services-list')


# Archive service
def archive_services(request, service_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if service_id:
            actual_service = get_object_or_404(Service, pk=service_id)
            actual_service.is_archived = True
            actual_service.save()
            return redirect('society_manager:services-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:services-list')


# List basic subscriptions
def list_basic_subscriptions(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        basic_subscriptions_list = BasicSubscription.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_basic_subscriptions.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit basic subscriptions
def manage_basic_subscriptions(request, basic_subscription_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if basic_subscription_id:
            actual_basic_subscription = get_object_or_404(BasicSubscription, pk=basic_subscription_id)
        else:
            actual_basic_subscription = BasicSubscription()
        # POST mode
        if request.method == 'POST':
            form = BasicSubscriptionForm(request.POST, instance=actual_basic_subscription)
            if form.is_valid():
                form.save()
                return redirect('society_manager:basic-subscriptions-list')
        else:
            # Non-POST mode, show only
            form = BasicSubscriptionForm(instance=actual_basic_subscription)
        return render(request, 'society_manager/masterdata/create_edit_basic_subscription.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:basic-subscriptions-list')


# Archive basic subscriptions
def archive_basic_subscriptions(request, basic_subscription_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if basic_subscription_id:
            actual_basic_subscription = get_object_or_404(BasicSubscription, pk=basic_subscription_id)
            actual_basic_subscription.is_archived = True
            actual_basic_subscription.save()
            return redirect('society_manager:basic-subscriptions-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:basic-subscriptions-list')


# List enrollment fees
def list_enrollment_fees(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        enrollment_fees_list = EnrollmentFee.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_enrollment_fees.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit enrollment fees
def manage_enrollment_fees(request, enrollment_fee_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if enrollment_fee_id:
            actual_enrollment_fee = get_object_or_404(EnrollmentFee, pk=enrollment_fee_id)
        else:
            actual_enrollment_fee = EnrollmentFee()
        # POST mode
        if request.method == 'POST':
            form = EnrollmentFeeForm(request.POST, instance=actual_enrollment_fee)
            if form.is_valid():
                form.save()
                return redirect('society_manager:enrollment-fees-list')
        else:
            # Non-POST mode, show only
            form = EnrollmentFeeForm(instance=actual_enrollment_fee)
        return render(request, 'society_manager/masterdata/create_edit_enrollment_fee.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:enrollment-fees-list')


# Archive enrollment fees
def archive_enrollment_fees(request, enrollment_fee_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if enrollment_fee_id:
            actual_enrollment_fee = get_object_or_404(EnrollmentFee, pk=enrollment_fee_id)
            actual_enrollment_fee.is_archived = True
            actual_enrollment_fee.save()
            return redirect('society_manager:enrollment-fees-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:enrollment-fees-list')


# List payment types
def list_payment_types(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        payment_types_list = PaymentType.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_payment_types.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit payment types
def manage_payment_types(request, payment_type_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if payment_type_id:
            actual_payment_type = get_object_or_404(PaymentType, pk=payment_type_id)
        else:
            actual_payment_type = PaymentType()
        # POST mode
        if request.method == 'POST':
            form = PaymentTypeForm(request.POST, instance=actual_payment_type)
            if form.is_valid():
                form.save()
                return redirect('society_manager:payment-types-list')
        else:
            # Non-POST mode, show only
            form = PaymentTypeForm(instance=actual_payment_type)
        return render(request, 'society_manager/masterdata/create_edit_payment_type.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:payment-types-list')


# Archive payment types
def archive_payment_types(request, payment_type_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if payment_type_id:
            actual_payment_type = get_object_or_404(PaymentType, pk=payment_type_id)
            actual_payment_type.is_archived = True
            actual_payment_type.save()
            return redirect('society_manager:payment-types-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:payment-types-list')


# List payment intervals
def list_payment_intervals(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        payment_intervals_list = PaymentInterval.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_payment_intervals.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit payment intervals
def manage_payment_intervals(request, payment_interval_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if payment_interval_id:
            actual_payment_interval = get_object_or_404(PaymentInterval, pk=payment_interval_id)
        else:
            actual_payment_interval = PaymentInterval()
        # POST mode
        if request.method == 'POST':
            form = PaymentIntervalForm(request.POST, instance=actual_payment_interval)
            if form.is_valid():
                form.save()
                return redirect('society_manager:payment-intervals-list')
        else:
            # Non-POST mode, show only
            form = PaymentIntervalForm(instance=actual_payment_interval)
        return render(request, 'society_manager/masterdata/create_edit_payment_interval.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:payment-intervals-list')


# Archive payment intervals
def archive_payment_intervals(request, payment_interval_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if payment_interval_id:
            actual_payment_interval = get_object_or_404(PaymentInterval, pk=payment_interval_id)
            actual_payment_interval.is_archived = True
            actual_payment_interval.save()
            return redirect('society_manager:payment-intervals-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:payment-intervals-list')


# List membership types
def list_membership_types(request):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        membership_types_list = MembershipType.objects.filter(is_archived=False)
        return render(request, 'society_manager/masterdata/list_membership_types.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit membership types
def manage_membership_types(request, membership_type_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if membership_type_id:
            actual_membership_type = get_object_or_404(MembershipType, pk=membership_type_id)
        else:
            actual_membership_type = MembershipType()
        # POST mode
        if request.method == 'POST':
            form = MembershipTypeForm(request.POST, instance=actual_membership_type)
            if form.is_valid():
                form.save()
                return redirect('society_manager:membership-types-list')
        else:
            # Non-POST mode, show only
            form = MembershipTypeForm(instance=actual_membership_type)
        return render(request, 'society_manager/masterdata/create_edit_membership_type.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:membership-types-list')


# Archive membership types
def archive_membership_types(request, membership_type_id=None):
    if request.user.is_superuser or request.user.profile_society_manager.status_rel.status == "Manager":
        if membership_type_id:
            actual_membership_type = get_object_or_404(MembershipType, pk=membership_type_id)
            actual_membership_type.is_archived = True
            actual_membership_type.save()
            return redirect('society_manager:membership-types-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:membership-types-list')
