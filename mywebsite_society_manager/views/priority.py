# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import PriorityForm
from mywebsite_society_manager.models import Priority


# List priorities
def list_priorities(request, state_id=None):
    if request.user.is_superuser:
        priorities_list = Priority.objects.filter(is_archived=False).order_by("id")
        return render(request, 'society_manager/priorities/list_priorities.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


def manage_priority(request, priority_id=None):
    if request.user.is_superuser:
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if priority_id:
            actual_priority = get_object_or_404(Priority, pk=priority_id)
        else:
            actual_priority = Priority()
        # POST mode
        if request.method == 'POST':
            form = PriorityForm(request.POST, instance=actual_priority)
            if form.is_valid():
                form.save()
                return redirect('society_manager:priority-list')
        else:
            # Non-POST mode, show only
            form = PriorityForm(instance=actual_priority)
        return render(request, 'society_manager/priorities/create_edit_priority.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:priority-list')


# Archive priotiry
def archive_priority(request, priority_id=None):
    if request.user.is_superuser:
        if priority_id:
            actual_priority = get_object_or_404(Priority, pk=priority_id)
            actual_priority.is_archived = True
            actual_priority.save()
            return redirect('society_manager:priority-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:priority-list')
