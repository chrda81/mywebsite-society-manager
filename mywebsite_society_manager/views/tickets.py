# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

import json
import os
from datetime import datetime
from string import Template

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import user_passes_test
from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMessage
from django.db import transaction
from django.db.models import Q
from django.db.utils import IntegrityError
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.context_processors import csrf
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.formats import localize
from django.utils.html import escape, strip_tags
from django.utils.translation import gettext as _
from django.views.decorators.csrf import requires_csrf_token
from django.views.generic import FormView

from crispy_forms.utils import render_crispy_form
from django_datatables_view.base_datatable_view import BaseDatatableView
from django_fsm import TransitionNotAllowed
from mywebsite.utils.tools import LazyEncoder
from mywebsite_blog.templatetags.blog_tags import unescape_raw
from mywebsite_home.templatetags import websettings_tags
from mywebsite_home.templatetags.core_settings_tags import getCoreSettingItemsFor, getTemplateByName
from reversion.models import Version

from mywebsite_society_manager import rights
from mywebsite_society_manager.filters import TicketFilter
from mywebsite_society_manager.forms import AttachmentForm, ProspectiveCompanyForm, TicketForm
from mywebsite_society_manager.models import (
    Attachment, Comments, Country, Microtasks, Priority, ProspectiveCompany, ProspectiveCompanyLabels, Queue,
    SocietyMember, State, Ticket
)
from mywebsite_society_manager.utils import get_object_or_none, get_parsed_value, getListIndex, to_bool

# Get correct user model
User = get_user_model()


# Commond data & queries used to create/edit ticktes
def common_ticket_data():
    # Queries
    users_info = User.objects.all()
    queue_info = Queue.objects.filter(is_archived=False)
    status_info = State.objects.filter(is_archived=False)
    prio_info = Priority.objects.filter(is_archived=False)
    now_str = datetime.now()

    # Get mail templates
    mail_templates = []
    for template_items in getCoreSettingItemsFor(_('Template Item'), 'App Templates'):
        if template_items and 'prospect' in template_items['name']:
            mail_templates.append(
                {'name': template_items['name'], 'content_name': websettings_tags.handleWebSettingsType(template_items['content_name'])})

    return {
        'status_info': status_info, 'prio_info': prio_info,
        'queue_info': queue_info, 'users_info': users_info,
        'now_str': now_str, 'mail_templates': mail_templates
    }


def mail_template_contains(name):
    common_data = common_ticket_data()

    for item in common_data['mail_templates']:
        if item and item['name'] == name:
            return True

    return False


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_tickets(request, ticket_id=None):
    """
    list_tickets: list functions called in urls.py used to list tickets under /tickets url
    """
    common_data = common_ticket_data()
    queues = rights.get_queues_as_q_for_ticket_model(request.user)

    # url name is needed for datatables ajax call, to build the correct initial Ticket query
    # have a look at TicketListJson.get_initial_queryset()
    url_name = request.resolver_match.url_name

    # If ticket_id is passed to function, we have to handle the 'save-signal' from function manage_ticket()
    if ticket_id and ticket_id is not None:
        ticket_instance = get_object_or_404(Ticket, pk=ticket_id)

    return render(request, 'society_manager/tickets/list_tickets.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_tickets_label(request, label):
    if not request.method == 'GET':
        return HttpResponse(status=405)
    common_data = common_ticket_data()
    queues = rights.get_queues_as_q_for_ticket_model(request.user)

    # url name is needed for datatables ajax call, to build the correct initial Ticket query
    # have a look at TicketListJson.get_initial_queryset()
    url_name = request.resolver_match.url_name
    requested_label = unescape_raw(label)  # bypass label to ajax call

    return render(request, 'society_manager/tickets/list_tickets.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_tickets_state(request, state_id):
    if not request.method == 'GET':
        return HttpResponse(status=405)
    common_data = common_ticket_data()
    queues = rights.get_queues_as_q_for_ticket_model(request.user)

    # url name is needed for datatables ajax call, to build the correct initial Ticket query
    # have a look at TicketListJson.get_initial_queryset()
    url_name = request.resolver_match.url_name
    requested_state_id = state_id  # bypass state_id to ajax call

    return render(request, 'society_manager/tickets/list_tickets.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_tickets_queue(request, queue_id):
    if not request.method == 'GET':
        return HttpResponse(status=405)
    common_data = common_ticket_data()
    queues = rights.get_queues_as_q_for_ticket_model(request.user)

    # url name is needed for datatables ajax call, to build the correct initial query
    # have a look at TicketListJson.get_initial_queryset()
    url_name = request.resolver_match.url_name
    requested_queue_id = queue_id  # bypass queue_id to ajax call

    return render(request, 'society_manager/tickets/list_tickets.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_ticket_jx(request, ticket_id=None):
    if request.is_ajax() and request.POST:
        user_obj = request.user
        """Check if we can archive ticket trought get_rights_for_ticket"""
        user_object_rights = rights.get_rights_for_ticket(
            user=user_obj, queue=None, ticket_id=ticket_id)
        if user_object_rights.can_archive or request.user.is_superuser:
            obj_to_archive = get_object_or_404(Ticket, pk=ticket_id)
            try:
                attach_to_archive = Attachment.objects.filter(ticket_rel=ticket_id)

            except Attachment.DoesNotExist:
                pass

            else:
                attach_to_archive.update(is_archived=True)
            # archive ticket itself
            obj_to_archive.is_mailed = True
            obj_to_archive.is_archived = True
            obj_to_archive.assigned_state = get_object_or_404(State, shortcode='closed')
            obj_to_archive.save()
            # archive depending mikrotasks
            mks_to_archive = Microtasks.objects.filter(ticket_rel=ticket_id)
            mks_to_archive.update(is_archived=True)
            # archive depending comments
            comments_to_archive = Comments.objects.filter(ticket_rel=ticket_id)
            comments_to_archive.update(is_archived=True)
            data = {
                'message': _("Ticket %s archived") % obj_to_archive.nr,
                'success': True,
                'redirect_url': reverse_lazy('society_manager:tickets-list', kwargs={'ticket_id': ticket_id})
            }
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _('You don\'t have permissions to archive this ticket'), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t archive ticket"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def create_ticket_jx(request):
    if request.is_ajax() and request.POST:
        # create common data
        new_ticket = Ticket()
        # create ticket id
        new_ticket_id = str(new_ticket.get_next_id())
        new_ticket.nr = settings.TICKETS_NR_PREFIX + new_ticket_id.rjust(settings.TICKETS_NR_WIDTH, '0')
        new_ticket.create_user = request.user
        new_ticket.assigned_user = get_object_or_none(User, username=settings.SYSTEM_USER)

        # auto create ticket for archiving request
        if request.POST.get('ticket_subject') == 'Request archiving of comment':
            if request.POST.get('comment_id') and request.POST.get('ticket_body'):
                comment = get_object_or_404(Comments, pk=request.POST.get('comment_id'))
                message_template = Template(
                    """ <div class="card card-default">
                            <div class="card-header">
                                <strong>$username</strong>
                                <span class="text-muted">
                                    $trans_commented $created in Ticket
                                    <a href="$href">#$ticketnr</a>
                                </span>
                                <input
                                    type="hidden"
                                    id="idPMessage"
                                    name="idPMessage"
                                    value="$comment_id"
                                >
                            </div>
                            <div class="card-body">
                                <div class="content-markdown-old">
                                    $comment_text
                                </div>
                            </div>
                        </div>
                    """)
                # create ticket data
                new_ticket.assigned_queue = get_object_or_404(
                    Queue,
                    shortcode=settings.GLOBAL_QUEUE_SHORTCODE,
                    is_archived=False)
                new_ticket.assigned_prio = get_object_or_404(Priority, shortcode='low', is_archived=False)
                new_ticket.subject = _('Request archiving of comment')
                new_ticket.body = '<p>{}</p><br /><br />{}'.format(
                    request.POST.get('ticket_body'),
                    message_template.substitute(dict(
                        username=comment.user_rel.username,
                        trans_commented=_('commented on'),
                        created=localize(comment.created),
                        href=reverse_lazy('society_manager:tickets-view', args=(comment.ticket_rel,)),
                        ticketnr=comment.ticket_rel.nr,
                        comment_id=comment.id,
                        comment_text=comment.comment
                    ))
                )
                new_ticket.save()

                data = {'message': _("Ticket %s created") % new_ticket.nr, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        # auto create ticket for applying for membership
        if request.POST.get('ticket_subject') == 'Application of membership':
            if request.POST.get('prospect_id'):
                prospect = get_object_or_404(ProspectiveCompany, pk=request.POST.get('prospect_id'))
                # create ticket data
                new_ticket.assigned_queue = get_object_or_404(
                    Queue,
                    shortcode=settings.MEMBERS_QUEUE_SHORTCODE,
                    is_archived=False)
                new_ticket.assigned_prio = get_object_or_404(Priority, shortcode='low', is_archived=False)
                new_ticket.create_user = get_object_or_none(User, username=settings.SYSTEM_USER)
                new_ticket.assigned_user = None
                new_ticket.subject = _('Application of membership for %s') % prospect.name
                new_ticket.body = _(
                    'This prospect is applying for membership. Please send all necessary info per mail.')
                new_ticket.assigned_company = prospect
                new_ticket.save()

                data = {'message': _("Ticket %s created") % new_ticket.nr, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        # auto create ticket for editing company data
        if request.POST.get('ticket_subject') == 'Edit company':
            if request.POST.get('member_id'):
                member = get_object_or_404(SocietyMember, pk=request.POST.get('member_id'))
                # create ticket data
                new_ticket.assigned_queue = get_object_or_404(
                    Queue,
                    shortcode=settings.MEMBERS_QUEUE_SHORTCODE,
                    is_archived=False)
                new_ticket.assigned_prio = get_object_or_404(Priority, shortcode='low', is_archived=False)
                new_ticket.create_user = get_object_or_none(User, username=settings.SYSTEM_USER)
                new_ticket.assigned_user = None
                new_ticket.subject = _('Edit company %s') % member.company.name
                new_ticket.body = _('Request for editing company details')
                new_ticket.assigned_company = member.company
                new_ticket.save()

                data = {'message': _("Ticket %s created") % new_ticket.nr, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t create ticket"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def manage_ticket(request, ticket_id=None):
    # Common data
    common_data = common_ticket_data()

    if ticket_id:
        # Check if exists
        ticket_rights = rights.get_rights_for_ticket(
            user=request.user, queue=None, ticket_id=ticket_id)
        if ticket_rights.can_edit or request.user.is_superuser:
            actual_ticket = get_object_or_404(Ticket, pk=ticket_id)
            actual_files = Attachment.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
            actual_microtasks = Microtasks.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
            # prepare conditions for collecting logging entries
            condition_1 = (Q(object_id__in=list(actual_comments.values_list('pk', flat=True))) &
                           Q(content_type__in=ContentType.objects.filter(model='comments')))
            condition_2 = (Q(object_id=actual_ticket.id) &
                           Q(content_type__in=ContentType.objects.filter(model='ticket')))
            if actual_ticket.assigned_company:
                show_partial_company_forms = False
                condition_3 = (Q(object_id=actual_ticket.assigned_company.id) &
                               Q(content_type__in=ContentType.objects.filter(model='prospectivecompany')))
            else:
                show_partial_company_forms = True
                condition_3 = Q()

            actual_logs = Version.objects.filter(condition_1 | condition_2 |
                                                 condition_3).order_by('-revision__date_created')
        else:
            messages.error(request, _('You don\'t have permissions to edit this ticket'))
            return redirect('society_manager:tickets-list')

    else:
        # If not, assign a new ticket instance to be use as instance of form
        queue = get_object_or_404(Queue, shortcode=settings.PROSPECTS_QUEUE_SHORTCODE)
        ticket_rights = rights.get_rights_for_ticket(
            user=request.user, queue=queue, ticket_id=None)
        if ticket_rights.can_create or request.user.is_superuser:
            actual_ticket = Ticket()
            # create new ticket id
            if 'ticket-nr' not in request.POST:
                new_ticket_id = str(actual_ticket.get_next_id())
                actual_ticket.nr = settings.TICKETS_NR_PREFIX + new_ticket_id.rjust(settings.TICKETS_NR_WIDTH, '0')
            show_partial_company_forms = True
        else:
            messages.error(request, _('You don\'t have permissions to create tickets'))
            return redirect('society_manager:tickets-list')

    # POST mode
    if request.method == 'POST':
        form_ticket = TicketForm(request.POST, instance=actual_ticket, request=request, prefix="ticket")
        form_attach = AttachmentForm(request.POST, request.FILES or None, prefix="attach")
        form_prospect = ProspectiveCompanyForm(instance=None, prefix="company",
                                               required_fields_only=show_partial_company_forms)

        # The company part (optional), if available
        company_form_is_valid = True
        company_instance = None
        if actual_ticket.assigned_company and 'company-name' in request.POST:
            form_prospect = ProspectiveCompanyForm(request.POST, instance=actual_ticket.assigned_company,
                                                   prefix="company", required_fields_only=show_partial_company_forms)
            if form_prospect.is_valid():
                company_instance = form_prospect.save()
            else:
                company_form_is_valid = False
        else:
            request_company_id = request.POST.get('ticket-assigned_company', None)
            if request_company_id:
                # set initial value of combobox from request
                form_ticket.fields['assigned_company'].initial = request_company_id

        if form_ticket.is_valid() and company_form_is_valid and form_attach.is_valid():
            # The ticket part, because it references the company part
            ticket_instance = form_ticket.save(commit=False)
            # set create_user with current user to have a work log
            if not ticket_instance.create_user:
                ticket_instance.create_user = request.user
            # override 'assigned_user'
            if 'assigned_user' not in form_ticket.changed_data:
                ticket_instance.assigned_user = request.user
            if hasattr(request, 'percentage'):
                ticket_instance.percentage = request.percentage

            if company_instance:
                ticket_instance.assigned_company = company_instance

            try:
                ticket_instance.save()
            except TransitionNotAllowed:
                messages.error(request, _('You can\'t switch to assigned state \'%s\'') %
                               ticket_instance.assigned_state.name)

            # The attach part
            # looping through all other documents and uploading
            for i in range(1, 11):
                if (str(form_attach.cleaned_data.get('file_name_%s' % str(i))) != 'None'):
                    Attachment.objects.create(ticket_rel=ticket_instance,
                                              file_name=request.FILES['attach-file_name_%s' % str(i)])

            if 'update-signal' in request.POST:
                return redirect('society_manager:tickets-edit', ticket_id)
            elif 'save-signal' in request.POST:
                return redirect('society_manager:tickets-list', ticket_id=ticket_instance.id)
    else:
        # Non-POST mode, show only

        # prepare form_prospect fields, if actual_ticket.assigned_company is None
        if not actual_ticket.assigned_company:
            assigned_company_helper = ProspectiveCompany()
            assigned_company_helper.name = get_parsed_value(actual_ticket.body, _('prospect_name_field'))
            assigned_company_helper.contact_first_name = get_parsed_value(
                actual_ticket.body, _('contact_first_name_field'))
            assigned_company_helper.contact_last_name = get_parsed_value(
                actual_ticket.body, _('contact_last_name_field'))

            parsed_country = get_parsed_value(actual_ticket.body, _('country_field'))
            if parsed_country:
                assigned_company_helper.country = Country.objects.get(name=parsed_country)

            assigned_company_helper.zipcode = get_parsed_value(actual_ticket.body, _('zipcode_field'))
            assigned_company_helper.location = get_parsed_value(actual_ticket.body, _('location_field'))
            assigned_company_helper.address = get_parsed_value(actual_ticket.body, _('address_field'))
            assigned_company_helper.email = get_parsed_value(actual_ticket.body, _('email_field'))
            assigned_company_helper.phone = get_parsed_value(actual_ticket.body, _('phone_field'))

        form_ticket = TicketForm(instance=actual_ticket, request=request, prefix="ticket")
        form_prospect = ProspectiveCompanyForm(instance=actual_ticket.assigned_company if actual_ticket.assigned_company else assigned_company_helper,
                                               prefix="company", required_fields_only=show_partial_company_forms)
        form_attach = AttachmentForm(prefix="attach")

    return render(request, 'society_manager/tickets/create_edit_ticket.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def view_ticket(request, ticket_id=None):
    # Common data
    common_data = common_ticket_data()
    if ticket_id:
        # Check if exists
        ticket_rights = rights.get_rights_for_ticket(
            user=request.user, queue=None, ticket_id=ticket_id)
        if ticket_rights.can_view:
            actual_ticket = get_object_or_404(Ticket, pk=ticket_id)
            actual_files = Attachment.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
            actual_microtasks = Microtasks.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
            # prepare conditions for collecting logging entries
            condition_1 = (Q(object_id__in=list(actual_comments.values_list('pk', flat=True))) &
                           Q(content_type__in=ContentType.objects.filter(model='comments')))
            condition_2 = (Q(object_id=actual_ticket.id) &
                           Q(content_type__in=ContentType.objects.filter(model='ticket')))
            if actual_ticket.assigned_company:
                condition_3 = (Q(object_id=actual_ticket.assigned_company.id) &
                               Q(content_type__in=ContentType.objects.filter(model='prospectivecompany')))
            else:
                condition_3 = Q()

            actual_logs = Version.objects.filter(condition_1 | condition_2 |
                                                 condition_3).order_by('-revision__date_created')

            if ticket_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to view this ticket'))
            return redirect('society_manager:tickets-list')
    else:
        # If not, assign a new ticket instance to be use as instance of form
        messages.error(request, _("No ticket selected"))
        return redirect('society_manager:tickets-list')

    form_ticket = TicketForm(instance=actual_ticket, request=request, prefix="ticket")
    form_prospect = ProspectiveCompanyForm(instance=actual_ticket.assigned_company,
                                           prefix="company", readonly_form=True)
    form_attach = AttachmentForm(prefix="attach")
    return render(request, 'society_manager/tickets/view.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def save_comment(request, comment_data=None, private_data=None, ticket_data=None):
    # check, if email should be sent
    send_as_mail = False
    if request.POST.get('send_as_mail') == 'true':
        send_as_mail = True
    # Save data
    inst_ticket = Ticket.objects.get(id=ticket_data)
    inst_data = Comments.objects.create(comment=comment_data, is_private=private_data,
                                        ticket_rel=inst_ticket, is_mailed=send_as_mail, user_rel=request.user)
    inst_data.save()
    return {'message': _("Comment saved"), 'success': True}


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_comment(request, message_id=None):
    """Check if we can archive comment trought get_rights_for_ticket"""
    user_object_rights = rights.get_rights_for_ticket(
        user=request.user, queue=None, ticket_id=None)
    if user_object_rights.can_archive or request.user.is_superuser:
        msg_to_archive = Comments.objects.get(id=message_id)
        msg_to_archive.is_archived = True
        msg_to_archive.save()
        return {'message': _("Comment archived"), 'success': True}
    else:
        return {'message': _("You can\'t archive this comment"), 'success': False}


# AJAX comments
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def add_comment_jx(request, ticket_id):
    if request.is_ajax() and request.POST:
        if request.POST.get('message_text'):
            """Check if we can add comment trought get_rights_for_ticket"""
            user_object_rights = rights.get_rights_for_ticket(
                user=request.user, queue=None, ticket_id=ticket_id)
            if user_object_rights.can_comment or request.user.is_superuser:
                message_data = request.POST.get('message_text')
                send_as_mail = request.POST.get('send_as_mail')
                data = save_comment(
                    request=request, comment_data=message_data, private_data=(not to_bool(send_as_mail)),
                    ticket_data=ticket_id)
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
            else:
                data = {'message': _("You don\'t have rights to comment"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
        else:
            data = {'message': _("You have to enter a message"), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t add comment"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_comment_jx(request):
    if request.is_ajax() and request.POST:
        status = 0
        if request.POST.get('message_id') and request.POST.get('ticket_id'):
            ticket_id = int(request.POST.get('ticket_id'))
            """Check if we can archive comment trought get_rights_for_ticket"""
            user_object_rights = rights.get_rights_for_ticket(
                user=request.user, queue=None, ticket_id=ticket_id)
            if user_object_rights.can_archive or request.user.is_superuser:
                message_data = request.POST.get('message_id')
                data = archive_comment(request=request, message_id=message_data)
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
            else:
                data = {'message': _("You don\'t have rights to archive this comment"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _("You have to enter a valid message ID and ticket ID"), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("You can\'t archive this comment"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def get_comments_jx(request, ticket_id=None):
    qry = Comments.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
    data = []

    # build up comments html data
    for item in qry:
        comment_template = get_template('society_manager/includes/ticket_comment.html')
        context = {
            'comm': item,
            'request': request
        }
        content = comment_template.render(context)
        data.append(content)

    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def set_percentage_jx(request, ticket_id=None):
    if request.is_ajax() and request.POST:
        percentage_val = request.POST.get('range_value')
        if percentage_val:
            percent_update = Ticket.objects.get(pk=ticket_id)
            percent_update.percentage = percentage_val
            percent_update.save()
            data = {'message': _("Percentage value updated"), 'success': True}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t set percentage"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def get_percentage_jx(request, ticket_id=None):
    qry = Ticket.objects.get(id=ticket_id)
    data = qry.as_json()
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def save_microtask(
        request, subject_data=None, body_data=None, state_data=None, percentage_data=None,
        ticket_data=None):
    # Save data
    inst_ticket = Ticket.objects.get(id=ticket_data)
    inst_data = Microtasks.objects.create(
        ticket_rel=inst_ticket, assigned_state=state_data, subject=subject_data,
        body=body_data, percentage=percentage_data)
    inst_data.save()
    return {'message': _("Microtask saved"), 'success': True}


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def update_microtask(
        request, subject_data=None, body_data=None, state_data=None, percentage_data=None,
        mk_data=None):
    # inst_data = Microtasks.objects.filter(id=mk_data).update(
    # assigned_state=state_data, subject=subject_data, body=body_data, percentage=percentage_data)
    inst_data = Microtasks.objects.get(id=mk_data)
    inst_data.assigned_state = state_data
    inst_data.subject = subject_data
    inst_data.body = body_data
    inst_data.percentage = percentage_data
    inst_data.save()
    return {'message': _("Microtask updated"), 'success': True}


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_microtask(request, mk_id=None):
    """Check if we can archive microtask trought get_rights_for_ticket"""
    user_object_rights = rights.get_rights_for_ticket(user=request.user, queue=None, ticket_id=None)
    if user_object_rights.can_archive or request.user.is_superuser:
        mk_to_archive = Microtasks.objects.get(id=mk_id)
        mk_to_archive.is_archived = True
        mk_to_archive.save()
        return {'message': _("Microtask archived"), 'success': True}
    else:
        raise {'message': _("Microtask can\'t be archived"), 'success': False}


# AJAX microtask
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def add_microtask_jx(request, ticket_id=None):
    if request.is_ajax() and request.POST:
        """if mk_id exists"""
        if request.POST.get('id_mk'):
            if (
                request.POST.get('subject_text') and request.POST.get('body_text') and
                    request.POST.get('state_id')):
                user_obj = request.user
                """Check if we can add comment trought get_rights_for_ticket"""
                user_object_rights = rights.get_rights_for_ticket(
                    user=user_obj, queue=None, ticket_id=ticket_id)
                if user_object_rights.can_edit or request.user.is_superuser:
                    mk_clean = request.POST.get('id_mk')
                    subject_clean = request.POST.get('subject_text')
                    body_clean = request.POST.get('body_text')
                    state_clean = State.objects.get(id=int(request.POST.get('state_id')))
                    percentage_clean = request.POST.get('percentage_num')
                    data = update_microtask(
                        request=request, subject_data=subject_clean, body_data=body_clean,
                        state_data=state_clean, percentage_data=percentage_clean, mk_data=mk_clean)
                    return JsonResponse(data, encoder=LazyEncoder, safe=False)
                else:
                    data = {'message': _("Some fields missing"), 'success': False}
                    return JsonResponse(data, encoder=LazyEncoder, safe=False)
            else:
                data = {'message': _("You don\'t have rights to comment"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
        else:
            if (
                request.POST.get('subject_text') and request.POST.get('body_text') and
                    request.POST.get('state_id')):
                user_obj = request.user
                """Check if we can add comment trought get_rights_for_ticket"""
                user_object_rights = rights.get_rights_for_ticket(
                    user=user_obj, queue=None, ticket_id=ticket_id)
                if user_object_rights.can_edit or request.user.is_superuser:
                    subject_clean = request.POST.get('subject_text')
                    body_clean = request.POST.get('body_text')
                    state_clean = State.objects.get(id=int(request.POST.get('state_id')))
                    percentage_clean = request.POST.get('percentage_num')
                    data = save_microtask(
                        request=request, subject_data=subject_clean, body_data=body_clean,
                        state_data=state_clean, percentage_data=percentage_clean,
                        ticket_data=ticket_id)
                    return JsonResponse(data, encoder=LazyEncoder, safe=False)
                else:
                    data = {'message': _("Some fields missing"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
            else:
                data = {'message': _("You don\'t have rights to comment"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Microtask id not found"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def get_microtasks_jx(request, ticket_id=None):
    qry = Microtasks.objects.filter(ticket_rel=ticket_id, is_archived=False).order_by('-created')
    data = []

    # build up microtasks html data
    for item in qry:
        microstask_template = get_template('society_manager/includes/ticket_microtask.html')
        context = {
            'mk': item
        }
        content = microstask_template.render(context)
        data.append(content)

    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_microtask_jx(request, ticket_id=None):
    if request.is_ajax() and request.POST:
        status = 0
        if request.POST.get('mk_id'):
            user_obj = request.user
            """Check if we can archive microtask trought get_rights_for_ticket"""
            user_object_rights = rights.get_rights_for_ticket(
                user=user_obj, queue=None, ticket_id=ticket_id)
            if user_object_rights.can_archive or request.user.is_superuser:
                mk_data = request.POST.get('mk_id')
                status = archive_microtask(request=request, mk_id=mk_data)
                data = {'message': status, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)
            else:
                data = {'message': _("You don\'t have rights to archive this microtask"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        data = {'message': _("You have to enter a valid microtask ID"), 'success': False}
        return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Microtask can\'t be archived"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def get_microtask_jx(request, mk_id=None):
    qry = Microtasks.objects.get(id=mk_id)
    data = qry.as_json()
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


# AJAX company
@requires_csrf_token
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def add_company_jx(request):
    if request.is_ajax() and request.POST:
        form = ProspectiveCompanyForm(request.POST, prefix="company", required_fields_only=True)
        if form.is_valid():
            status = _("Company '%s' added") % request.POST.get('company-name')

            try:
                with transaction.atomic():
                    form.save()
            except IntegrityError as ex:
                # ensure CSRF token is placed in, before using render_crispy_form
                ctx = {}
                ctx.update(csrf(request))
                # render AJAX response with django-crispy-forms
                html_form = render_crispy_form(form, context=ctx)
                data = {'message': _("Database error: %s") % ex.args, 'html_form': html_form, 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

            data = {'message': status, 'company_name': form.cleaned_data['name'],
                    'company_id': form.instance.id, 'success': True}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)
        else:
            # ensure CSRF token is placed in, before using render_crispy_form
            ctx = {}
            ctx.update(csrf(request))
            # render AJAX response with django-crispy-forms
            html_form = render_crispy_form(form, context=ctx)
            data = {'message': _("Please correct the error below."), 'html_form': html_form, 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t create company"), 'html_form': html_form, 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


# AJAX render email template
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def render_mail_jx(request, ticket_id):
    # Common data
    common_data = common_ticket_data()

    if request.is_ajax() and request.POST:
        if request.POST.get('ticket_state'):
            actual_ticket = Ticket.objects.get(id=ticket_id)
            ticket_state = request.POST.get('ticket_state')
            message_data = request.POST.get('message_text', None)
            message_template = request.POST.get('message_template', None)

            contact_fullname = '{} {} {}'.format(
                actual_ticket.assigned_company.contact_title,
                actual_ticket.assigned_company.contact_first_name,
                actual_ticket.assigned_company.contact_last_name
            )

            # reset attachments from mail_template
            attachments = None

            if ticket_state not in ['view', 'closed'] and actual_ticket.needs_to_send_mail:
                # Build mail structure with the contact information
                template_dict = getTemplateByName('Template ticket-new')
                # .. email subject
                mail_subject = template_dict['subject']
                context = {
                    'ticket_nr': actual_ticket.nr,
                    'subject': actual_ticket.subject
                }
                subject = mail_subject.render(context)
                # .. email message
                mail_template = template_dict['template']
                context = {
                    'contact_name': contact_fullname,
                    'contact_first_name': actual_ticket.assigned_company.contact_first_name,
                    'assigned_user_first_name': actual_ticket.assigned_user.first_name if actual_ticket.assigned_user else '',
                    'assigned_user_last_name': actual_ticket.assigned_user.last_name if actual_ticket.assigned_user else '',
                    'ticket_nr': actual_ticket.nr
                }
                content = mail_template.render(context)
                # append file attachments from mail_template
                attachments = template_dict['attachments']

            if 'view' in ticket_state:
                # Build mail structure with the contact information
                if not message_template or not mail_template_contains(message_template):
                    template_dict = getTemplateByName('Template ticket-comment')
                else:
                    template_dict = getTemplateByName(message_template)
                # .. email subject
                mail_subject = template_dict['subject']
                context = {
                    'ticket_nr': actual_ticket.nr,
                    'subject': actual_ticket.subject
                }
                subject = mail_subject.render(context)
                # .. email message
                mail_template = template_dict['template']
                context = {
                    'contact_name': contact_fullname,
                    'contact_first_name': actual_ticket.assigned_company.contact_first_name,
                    'assigned_user_first_name': actual_ticket.assigned_user.first_name if actual_ticket.assigned_user else '',
                    'assigned_user_last_name': actual_ticket.assigned_user.last_name if actual_ticket.assigned_user else '',
                    'ticket_nr': actual_ticket.nr,
                    'message': message_data
                }
                content = mail_template.render(context)
                # append file attachments from mail_template
                attachments = template_dict['attachments']

            if 'closed' in ticket_state and actual_ticket.needs_to_send_mail:
                # Build mail structure with the contact information
                template_dict = getTemplateByName('Template ticket-closed')
                # .. email subject
                mail_subject = template_dict['subject']
                context = {
                    'ticket_nr': actual_ticket.nr,
                    'subject': actual_ticket.subject
                }
                subject = mail_subject.render(context)
                # .. email message
                mail_template = template_dict['template']
                context = {
                    'contact_name': contact_fullname,
                    'contact_first_name': actual_ticket.assigned_company.contact_first_name,
                    'assigned_user_first_name': actual_ticket.assigned_user.first_name if actual_ticket.assigned_user else '',
                    'assigned_user_last_name': actual_ticket.assigned_user.last_name if actual_ticket.assigned_user else '',
                    'ticket_nr': actual_ticket.nr
                }
                content = mail_template.render(context)
                # append file attachments from mail_template
                attachments = template_dict['attachments']

            data = {
                'to': actual_ticket.assigned_company.email,
                'bcc': settings.EMAILTO,
                'subject': subject,
                'html_content': content,
                'plain_content': strip_tags(content),
                'needs_to_send_mail': actual_ticket.needs_to_send_mail,
                'attachments': attachments,
                'success': True
            }
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _("You have to enter a ticket state"), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t render mail"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


# AJAX send mail
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def send_mail_jx(request, ticket_id):
    if request.is_ajax() and request.POST:
        if request.POST.get('message_text'):
            """Check if we can add comment trought get_rights_for_ticket"""
            user_object_rights = rights.get_rights_for_ticket(
                user=request.user, queue=None, ticket_id=ticket_id)
            if user_object_rights.can_comment or request.user.is_superuser:
                actual_ticket = Ticket.objects.get(id=ticket_id)
                message_data = request.POST.get('message_text')
                mail_subject = request.POST.get('mail_subject', None)
                mail_to = request.POST.get('mail_to', None)
                mail_cc = request.POST.get('mail_cc', None)
                mail_bcc = request.POST.get('mail_bcc', None)
                attachments = request.POST.get('attachments', None)

                # check, if mail should be sent
                if actual_ticket.can_send_mail:
                    # Send Email
                    mail_recipients = []
                    mail_cc_recipients = []
                    mail_bcc_recipients = []
                    if settings.EMAILTEST:
                        [mail_recipients.append(obj) for obj in settings.EMAILTO.split(',')]
                    else:
                        [mail_recipients.append(obj) for obj in mail_to.split(',')]
                    if mail_cc:
                        mail_cc_recipients = [obj for obj in mail_cc.split(',')]
                    if mail_bcc:
                        mail_bcc_recipients = [obj for obj in mail_bcc.split(',')]
                    email = EmailMessage(
                        subject=mail_subject,
                        body=message_data,
                        from_email=settings.DEFAULT_FROM_EMAIL,
                        to=mail_recipients,
                        cc=mail_cc_recipients,
                        bcc=mail_bcc_recipients
                    )

                    # attach files
                    if attachments:
                        for attachment in json.loads(attachments):
                            file_name = os.path.join(settings.MEDIA_ROOT, attachment)
                            email.attach_file(file_name)

                    email.content_subtype = 'html'
                    email.send()

                    # set is_mailed state
                    actual_ticket.is_mailed = True
                    actual_ticket.save()

                    data = {
                        'message': _("Mail sent successfully"),
                        'success': True,
                    }
                    return JsonResponse(data, encoder=LazyEncoder, safe=False)

            else:
                data = {'message': _("You don\'t have rights to comment"), 'success': False}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _("You have to enter a message"), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t send mail"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


# AJAX ignore mail
@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def ignore_mail_jx(request, ticket_id):
    if request.is_ajax() and request.POST:
        """Check if we can add comment trought get_rights_for_ticket"""
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=None, ticket_id=ticket_id)
        if user_object_rights.can_comment or request.user.is_superuser:
            actual_ticket = Ticket.objects.get(id=ticket_id)

            # set is_mailed state
            ignore_state = request.POST.get('ignore_state', False)
            actual_ticket.is_mailed = True if ignore_state == 'true' else False
            actual_ticket.save()

            data = {'message': _("Mail ignored successfully"), 'ignore_state': ignore_state, 'success': True}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _("You don\'t have rights to comment"), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t ignore mail"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)

    # Datatables viewsets


class TicketListJson(BaseDatatableView):
    # The model we're going to show
    model = Ticket

    # define the columns that will be returned
    columns = [
        'pk',
        '',
        'nr',
        'subject',
        'assigned_company.name',
        'assigned_state.name',
        'assigned_queue.name',
        'str_create_user',
        'created',
        'updated',
        'pending',
        'str_assigned_user',
        'percentage',
        'assigned_prio.name',
        'assigned_company.members.is_active'
    ]

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = [
        '',
        '',
        'nr',
        'subject',
        'assigned_company.name',
        'assigned_state.name',
        'assigned_queue.name',
        'str_create_user',
        'created',
        'updated',
        'pending',
        'str_assigned_user',
        'percentage',
        'assigned_prio.name',
        'assigned_company.members.is_active'
    ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.

        queues = rights.get_queues_as_q_for_ticket_model(self.request.user)
        tickets_list = Ticket.objects.filter(queues, is_archived=False)

        # url name is passed by datatables ajax call, to build the correct initial Ticket query
        url_name = self.request.POST.get('url_name', None)
        if url_name and url_name == 'tickets-list':
            # We pass always granted_queues as a roundup to query_view requirements
            # Assuming possible states are 'open', 'in_rogress', 'is_pending' and 'closed'
            return tickets_list.exclude(assigned_state__shortcode='closed').order_by('-created')

        if url_name and url_name == 'tickets-list-label':
            label = unescape_raw(self.request.POST.get('requested_label', None))
            # select all ids of label name from ProspectiveCompanyLabels
            company_ids_for_label = ProspectiveCompanyLabels.objects.filter(
                name=label).values_list('company', flat=True)

            return tickets_list.filter(assigned_company__in=company_ids_for_label, labels__icontains=label) \
                .order_by('-created')

        if url_name and url_name == 'tickets-list-state':
            state_id = self.request.POST.get('requested_state_id', None)
            return tickets_list.filter(assigned_state__id=state_id).order_by('-created')

        if url_name and url_name == 'tickets-list-queue':
            queue_id = self.request.POST.get('requested_queue_id', None)
            return tickets_list.filter(assigned_queue__id=queue_id).order_by('-created')

        if url_name and url_name == 'search':
            request_vars = {
                'subject': self.request.POST.get('requested_subject', None),
                'body': self.request.POST.get('requested_body', None),
                'comment': self.request.POST.get('requested_comment', None),
                'assigned_company': self.request.POST.get('requested_assigned_company', None),
                'assigned_state': self.request.POST.get('requested_assigned_state', None),
                'assigned_user': self.request.POST.get('requested_assigned_user', None),
                'create_user': self.request.POST.get('requested_create_user', None),
                'active_member': self.request.POST.get('requested_active_member', None)
            }
            if all(value == '' for value in request_vars.values()):
                return Ticket.objects.none()
            else:
                # correct values for boolean field 'active_member'
                if request_vars['active_member'] == 'unknown':
                    request_vars['active_member'] = ''
                    if all(value == '' for value in request_vars.values()):
                        return Ticket.objects.none()

                tickets_filter = TicketFilter(request_vars, queryset=tickets_list)
                return tickets_filter.qs.order_by('-created')

        return tickets_list.order_by('-created')

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # escape HTML for security reasons
        json_data = []
        for item in qs:
            json_data.append([
                item.pk,
                '',
                escape(item.nr) if item.nr else '',
                escape(item.subject) if item.subject else '',
                escape(item.assigned_company.name) if item.assigned_company else '',
                escape(item.assigned_state.name) if item.assigned_state.name else '',
                escape(item.assigned_queue.name) if item.assigned_queue.name else '',
                escape(item.str_create_user) if item.str_create_user else '',
                item.created.strftime(settings.DATE_FORMAT_JSON),
                item.updated.strftime(settings.DATE_FORMAT_JSON),
                item.pending.strftime(settings.DATE_FORMAT_JSON),
                escape(item.str_assigned_user) if item.str_assigned_user else '',
                escape(item.percentage) if item.percentage else '',
                escape(item.assigned_prio.name) if item.assigned_prio.name else '',
                item.assigned_company.members.is_active if hasattr(
                    item.assigned_company, 'members') and item.assigned_company.members.is_active else False
            ])
        return json_data

    def filter_queryset(self, qs):
        # use parameters passed in POST request to filter queryset

        # global search:
        search = self.request.POST.get('search[value]', None)
        if search:
            q = Q(nr__icontains=search) |\
                Q(subject__icontains=search) |\
                Q(assigned_company__name__icontains=search) |\
                Q(assigned_state__name__icontains=search) |\
                Q(assigned_queue__name__icontains=search) |\
                Q(create_user__username__icontains=search) |\
                Q(assigned_user__username__icontains=search) |\
                Q(percentage__icontains=search) |\
                Q(assigned_prio__name__icontains=search)
            qs = qs.filter(q)

        # search column 'nr'
        idx = getListIndex(self.columns_data, 'nr')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(nr__icontains=filter_value)

        # search column 'subject'
        idx = getListIndex(self.columns_data, 'subject')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(subject__icontains=filter_value)

        # search column 'assigned_company.name'
        idx = getListIndex(self.columns_data, 'assigned_company.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(assigned_company__name__icontains=filter_value)

        # search column 'assigned_state.name'
        idx = getListIndex(self.columns_data, 'assigned_state.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(assigned_state__pk__iexact=filter_value)

        # search column 'assigned_queue.name'
        idx = getListIndex(self.columns_data, 'assigned_queue.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(assigned_queue__pk__iexact=filter_value)

        # search column 'str_create_user'
        idx = getListIndex(self.columns_data, 'str_create_user')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(create_user__pk__iexact=filter_value)

        # search column 'created'
        idx = getListIndex(self.columns_data, 'created')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                min_date, max_date = filter_value.split(' - ')
                qs = qs.filter(created__range=[datetime.strptime(
                    min_date, settings.DATE_FORMAT_JSON), datetime.strptime(max_date, settings.DATE_FORMAT_JSON)])

        # search column 'updated'
        idx = getListIndex(self.columns_data, 'updated')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                min_date, max_date = filter_value.split(' - ')
                qs = qs.filter(updated__range=[datetime.strptime(
                    min_date, settings.DATE_FORMAT_JSON), datetime.strptime(max_date, settings.DATE_FORMAT_JSON)])

        # search column 'pending'
        idx = getListIndex(self.columns_data, 'pending')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                min_date, max_date = filter_value.split(' - ')
                qs = qs.filter(pending__range=[datetime.strptime(
                    min_date, settings.DATE_FORMAT_JSON), datetime.strptime(max_date, settings.DATE_FORMAT_JSON)])

        # search column 'str_assigned_user'
        idx = getListIndex(self.columns_data, 'str_assigned_user')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(assigned_user__pk__iexact=filter_value)

        # search column 'percentage'
        idx = getListIndex(self.columns_data, 'percentage')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(percentage__icontains=filter_value)

        # search column 'assigned_prio.name'
        idx = getListIndex(self.columns_data, 'assigned_prio.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(assigned_prio__pk__iexact=filter_value)

        # search column 'assigned_company.members.is_active'
        idx = getListIndex(self.columns_data, 'assigned_company.members.is_active')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                if filter_value == 'true':
                    qs = qs.filter(Q(assigned_company__members__is_active=True) &
                                   Q(assigned_company__members__is_archived=False))
                if filter_value == 'false':
                    qs = qs.exclude(assigned_company__members__is_active=True)

        return qs

    def get_context_data(self, *args, **kwargs):
        ret = super().get_context_data(self, *args, **kwargs)

        # add unique values for dropdown filters
        ret['col_5_selections'] = list(Ticket.objects.filter(is_archived=False).order_by(
            'assigned_state__name').values_list('assigned_state__name', 'assigned_state__pk').distinct())

        ret['col_6_selections'] = list(Ticket.objects.filter(is_archived=False).order_by(
            'assigned_queue__name').values_list('assigned_queue__name', 'assigned_queue__pk').distinct())

        ret['col_7_selections'] = []  # [{0: '--', 1: None}]
        for item in Ticket.objects.filter(is_archived=False).order_by(
                'create_user__username').values_list('create_user__username', 'create_user__pk').distinct():
            if item and item[0]:
                ret['col_7_selections'].append(item)

        ret['col_11_selections'] = []  # [{0: '--', 1: None}]
        for item in Ticket.objects.filter(is_archived=False).order_by(
                'assigned_user__username').values_list('assigned_user__username', 'assigned_user__pk').distinct():
            if item and item[0]:
                ret['col_11_selections'].append(item)

        ret['col_13_selections'] = list(Ticket.objects.filter(is_archived=False).order_by(
            'assigned_prio__name').values_list('assigned_prio__name', 'assigned_prio__pk').distinct())

        ret['col_14_selections'] = [(_('Yes'), 'true'), (_('No'), 'false')]

        return ret
