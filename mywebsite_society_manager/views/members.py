"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import decorators, mixins
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import (HttpResponse, get_object_or_404, redirect,
                              render, reverse)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.html import escape
from django.utils.translation import gettext as _
from django.views import generic
from django.views.decorators.csrf import requires_csrf_token
from django_datatables_view.base_datatable_view import BaseDatatableView
from mywebsite.utils.tools import LazyEncoder
from sequences import get_next_value

from mywebsite_society_manager import rights
from mywebsite_society_manager.filters import SocietyMembersFilter
from mywebsite_society_manager.forms import (ServiceForm, SocietyMemberForm,
                                             UsedServicesForm)
from mywebsite_society_manager.models import (Attachment, Comments,
                                              ProspectiveCompany, Queue,
                                              Service, SocietyMember,
                                              UsedServices)
from mywebsite_society_manager.utils import (get_object_or_none, getListIndex,
                                             render_to_pdf)


# Commond data & queries used to view/create/edit members
def common_member_data():
    # Static data
    main_tab_form_fields = 'nr, company, additional_name, created, issue_date, exit_date, consultant, is_active, is_paying, \
        membership, recommendation'
    payment_tab_form_fields = 'accounts_receivable_nr, enrollment_fee, basic_subscription, payment_type, \
        payment_interval, payment_due_date, sepa_bank_name, sepa_account_name, sepa_iban, sepa_bic'
    service_tab_form_fields = 'interests, service_focus, used_services'
    initial_notes = ""

    # Queries
    # users_info = User.objects.all()
    assigned_queue = get_object_or_404(Queue, shortcode=settings.MEMBERS_QUEUE_SHORTCODE)

    return {
        'main_tab_form_fields': main_tab_form_fields,
        'payment_tab_form_fields': payment_tab_form_fields,
        'service_tab_form_fields': service_tab_form_fields,
        'initial_notes': initial_notes,
        'assigned_queue': assigned_queue
    }


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_members(request):
    """
    list_members: list functions called in urls.py used to list members under /members url
    """

    members = SocietyMember.objects.filter(is_archived=False).order_by('id')
    # Prospects which have is_applying_for_membership checked, but are not in SocietyMember table
    #   => needs to set up by agent
    # Important: Archived members are excluded by filter as well!
    new_members = ProspectiveCompany.objects.filter(is_applying_for_membership=True, is_archived=False)\
        .exclude(pk__in=SocietyMember.objects.all().values_list('company__pk', flat=True))

    return render(request, 'society_manager/members/list_members.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def view_member(request, member_id=None):
    # Common data
    common_data = common_member_data()
    if member_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_view:
            actual_member = get_object_or_404(SocietyMember, pk=member_id)
            actual_files = Attachment.objects.filter(ticket_rel__assigned_company=actual_member.company,
                                                     is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel__assigned_company=actual_member.company,
                                                      is_archived=False).order_by('-created')
            used_services = UsedServices.objects.filter(member=actual_member, is_archived=False)
            if user_object_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to view this member'))
            return redirect('society_manager:members-list')

    else:
        messages.error(request, _('No member selected'))
        return redirect('society_manager:members-list')

    # POST mode
    form_member = SocietyMemberForm(instance=actual_member, prefix="member")
    return render(request, 'society_manager/members/view.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def manage_member(request, prospect_id=None, member_id=None):
    # Common data
    common_data = common_member_data()
    if prospect_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_edit:
            actual_member = SocietyMember()
            if 'member-nr' in request.POST:
                # get member id from POST
                actual_member.nr = request.POST['member-nr']
            else:
                # create new member id
                new_member_id = str(actual_member.get_next_id())
                actual_member.nr = settings.MEMBERS_NR_PREFIX + new_member_id.rjust(settings.MEMBERS_NR_WIDTH, '0')
                actual_member.notes = common_data['initial_notes']
            # get prospect by id
            actual_prospect = get_object_or_404(ProspectiveCompany, pk=prospect_id)
            actual_member.company = actual_prospect
            if user_object_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to add this member'))
            return redirect('society_manager:members-list')

    elif member_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_edit:
            actual_member = get_object_or_404(SocietyMember, pk=member_id)
            all_services = Service.objects.filter(is_archived=False)
            used_services = UsedServices.objects.filter(member=actual_member, is_archived=False)
            if user_object_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to add this member'))
            return redirect('society_manager:members-list')

    else:
        messages.error(request, _('No member selected'))
        return redirect('society_manager:members-list')

    # POST mode
    if request.method == 'POST':
        form_member = SocietyMemberForm(request.POST, instance=actual_member, prefix="member")
        form_services = UsedServicesForm(request.POST, instance=actual_member, prefix="services")

        if form_member.is_valid():
            form_member.save()

            if 'update-signal' in request.POST:
                return redirect('society_manager:members-edit', actual_member.id)
            elif 'save-signal' in request.POST:
                # we want to increment the members sequence only on saving a new member instance
                if settings.CONTINUOUS_MEMBER_ID:
                    member_nr = request.POST.get('member-nr', '')
                    # increment only, if members sequence is in member-nr of POST
                    if str(actual_member.get_next_id()) in member_nr:
                        get_next_value('members')

                return redirect('society_manager:members-list')

    else:
        # Non-POST mode, show only
        form_member = SocietyMemberForm(instance=actual_member, prefix="member")
        form_services = UsedServicesForm(instance=actual_member, prefix="services")

    return render(request, 'society_manager/members/create_edit_member.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_member(request, member_id=None):
    # Common data
    common_data = common_member_data()

    """Check if we can archive trought get_rights_for_ticket"""
    user_object_rights = rights.get_rights_for_ticket(
        user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
    if user_object_rights.can_archive:
        obj_to_archive = get_object_or_404(SocietyMember, pk=member_id)
        obj_to_archive.is_archived = True
        obj_to_archive.save()
        return redirect('society_manager:members-list')
    else:
        messages.error(request, _('You don\'t have permissions to archive this member'))
        return redirect('society_manager:members-list')


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def export_member(request, member_id=None):
    # Common data
    common_data = common_member_data()
    if member_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_view:
            actual_member = get_object_or_404(SocietyMember, pk=member_id)
            actual_files = Attachment.objects.filter(ticket_rel__assigned_company=actual_member.company,
                                                     is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel__assigned_company=actual_member.company,
                                                      is_private=False, is_archived=False).order_by('-created')
            actual_services = UsedServices.objects.filter(member_id=member_id).order_by('service__name')

            context = {
                'request': request,
                'title': '%s / %s' % (actual_member.nr, actual_member.company),
                'member_json': actual_member.as_json(),
                'company_json': actual_member.company.as_json(),
                'services': actual_services,
                'comments': actual_comments
            }

            # rendering the template
            return render_to_pdf('society_manager/members/export.html', context)
        else:
            messages.error(request, _('You don\'t have permissions to view this member'))
            return redirect('society_manager:members-list')

    else:
        messages.error(request, _('No member selected'))
        return redirect('society_manager:members-list')


# Create or edit used service
@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def manage_services_jx(request):
    # Common data
    common_data = common_member_data()

    if request.is_ajax() and request.POST:
        service_id = None

        """Check if we can edit trought get_rights_for_ticket"""
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_edit:
            if request.POST.get('service_id'):
                service_id = request.POST.get('service_id')
                actual_service = get_object_or_none(Service, pk=service_id)

            if actual_service and request.POST.get('member_id'):
                member_id = request.POST.get('member_id')
                description = request.POST.get('description')
                is_checked = request.POST.get('is_checked').capitalize()
                actual_member = get_object_or_none(SocietyMember, pk=member_id)
                used_service = get_object_or_none(UsedServices, service=service_id, member=member_id)

                if used_service:
                    used_service.description = description
                    used_service.is_checked = is_checked
                    used_service.is_archived = False
                    used_service.save()
                elif actual_member:
                    used_service = UsedServices()
                    used_service.member = actual_member
                    used_service.service = actual_service
                    used_service.description = description
                    used_service.is_checked = is_checked
                    used_service.is_archived = False
                    used_service.save()

                data = {'message': _("Used service created"), 'member_id': member_id, 'service_id': service_id,
                        'service_name': actual_service.name, 'description': description,
                        'is_checked': used_service.is_checked, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _('You don\'t have enough permissions to edit this service'), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    data = {'message': _("Can\'t create service"), 'success': False}
    return JsonResponse(data, encoder=LazyEncoder, safe=False)


# Archive used service
@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_services_jx(request):
    # Common data
    common_data = common_member_data()

    if request.is_ajax() and request.POST:
        service_id = None

        """Check if we can archive trought get_rights_for_ticket"""
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_archive:
            if request.POST.get('service_id') and request.POST.get('member_id'):
                service_id = request.POST.get('service_id')
                member_id = request.POST.get('member_id')
                used_service = get_object_or_404(UsedServices, member_id=member_id, service_id=service_id)
                used_service.is_archived = True
                used_service.save()

                data = {'message': _("Used service archived"),
                        'service_name': used_service.service.name, 'success': True}
                return JsonResponse(data, encoder=LazyEncoder, safe=False)

        else:
            data = {'message': _('You don\'t have enough permissions to archive this service'), 'success': False}
            return JsonResponse(data, encoder=LazyEncoder, safe=False)

    else:
        data = {'message': _("Can\'t archive service"), 'success': False}
        return JsonResponse(data, encoder=LazyEncoder, safe=False)


# Datatables viewsets
class SocietyMemberListJson(BaseDatatableView):
    # The model we're going to show
    model = SocietyMember

    # define the columns that will be returned
    columns = [
        'pk',
        '',
        'nr',
        'company.name',
        'company.additional_name',
        'company.contact_person',
        'company.country.name',
        'company.zipcode',
        'company.location',
        'company.address',
        'company.mobile',
        'company.phone',
        'company.fax',
        'company.email',
        'issue_date',
        'exit_date',
        'payment_due_date',
        'basic_subscription',
        'is_paying',
        'is_active',
    ]

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = [
        '',
        '',
        'nr',
        'company.name',
        'company.additional_name',
        'company.contact_person',
        'company.country.name',
        'company.zipcode',
        'company.location',
        'company.address',
        'company.mobile',
        'company.phone',
        'company.fax',
        'company.email',
        'issue_date',
        'exit_date',
        'payment_due_date',
        'basic_subscription',
        'is_paying',
        'is_active',
    ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return SocietyMember.objects.filter(is_archived=False).order_by('id')

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # escape HTML for security reasons
        json_data = []
        for item in qs:
            json_data.append([
                item.pk,
                '',
                escape(item.nr),
                escape(item.company.name) if item.company.name else '',
                escape(item.company.additional_name) if item.company.additional_name else '',
                escape('{0} {1}, {2}'.format(item.company.contact_title, item.company.contact_last_name,
                                             item.company.contact_first_name)).strip(),
                escape(item.company.country) if item.company.country else '',
                escape(item.company.zipcode) if item.company.zipcode else '',
                escape(item.company.location) if item.company.location else '',
                escape(item.company.address) if item.company.address else '',
                escape(item.company.mobile) if item.company.mobile else '',
                escape(item.company.phone) if item.company.phone else '',
                escape(item.company.fax) if item.company.fax else '',
                escape(item.company.email) if item.company.email else '',
                item.issue_date.strftime(settings.DATE_FORMAT_JSON),
                item.exit_date.strftime(settings.DATE_FORMAT_JSON),
                escape(item.payment_due_date) if item.payment_due_date else '',
                escape(item.basic_subscription) if item.basic_subscription else '',
                _('is paying') if item.is_paying else _('not paying'),
                _('is active') if item.is_active else _('not active'),
            ])
        return json_data

    def filter_queryset(self, qs):
        # use parameters passed in POST request to filter queryset

        # global search:
        search = self.request.POST.get('search[value]', None)
        if search:
            q = Q(nr__icontains=search) |\
                Q(company__name__icontains=search) |\
                Q(company__additional_name__icontains=search) |\
                Q(company__contact_title__icontains=search) |\
                Q(company__contact_last_name__icontains=search) |\
                Q(company__contact_first_name__icontains=search) |\
                Q(company__country__name__icontains=search) |\
                Q(company__zipcode__icontains=search) |\
                Q(company__location__icontains=search) |\
                Q(company__address__icontains=search) |\
                Q(company__mobile__icontains=search) |\
                Q(company__phone__icontains=search) |\
                Q(company__fax__icontains=search) |\
                Q(company__email__icontains=search) |\
                Q(payment_due_date__icontains=search) |\
                Q(basic_subscription__subscription__icontains=search)
            qs = qs.filter(q)

        # search column 'nr'
        idx = getListIndex(self.columns_data, 'nr')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(nr__icontains=filter_value)

        # search column 'company.name'
        idx = getListIndex(self.columns_data, 'company.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__name__icontains=filter_value)

        # search column 'company.additional_name'
        idx = getListIndex(self.columns_data, 'company.additional_name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__additional_name__icontains=filter_value)

        # search column 'company.contact_person'
        idx = getListIndex(self.columns_data, 'company.contact_person')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(
                    Q(company__contact_title__icontains=filter_value) |
                    Q(company__contact_last_name__icontains=filter_value) |
                    Q(company__contact_first_name__icontains=filter_value)
                )

        # search column 'company.country'
        idx = getListIndex(self.columns_data, 'company.country.name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__country__pk__iexact=filter_value)

        # search column 'company.zipcode'
        idx = getListIndex(self.columns_data, 'company.zipcode')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__zipcode__icontains=filter_value)

        # search column 'company.location'
        idx = getListIndex(self.columns_data, 'company.location')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__location__icontains=filter_value)

        # search column 'company.address'
        idx = getListIndex(self.columns_data, 'company.address')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__address__icontains=filter_value)

        # search column 'company.mobile'
        idx = getListIndex(self.columns_data, 'company.mobile')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__mobile__icontains=filter_value)

        # search column 'company.phone'
        idx = getListIndex(self.columns_data, 'company.phone')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__phone__icontains=filter_value)

        # search column 'company.fax'
        idx = getListIndex(self.columns_data, 'company.fax')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__fax__icontains=filter_value)

        # search column 'company.email'
        idx = getListIndex(self.columns_data, 'company.email')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(company__email__icontains=filter_value)

        # search column 'issue_date'
        idx = getListIndex(self.columns_data, 'issue_date')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                min_date, max_date = filter_value.split(' - ')
                qs = qs.filter(issue_date__range=[datetime.strptime(
                    min_date, settings.DATE_FORMAT_JSON), datetime.strptime(max_date, settings.DATE_FORMAT_JSON)])

        # search column 'exit_date'
        idx = getListIndex(self.columns_data, 'exit_date')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                min_date, max_date = filter_value.split(' - ')
                qs = qs.filter(exit_date__range=[datetime.strptime(
                    min_date, settings.DATE_FORMAT_JSON), datetime.strptime(max_date, settings.DATE_FORMAT_JSON)])

        # search column 'payment_due_date'
        idx = getListIndex(self.columns_data, 'payment_due_date')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(payment_due_date__icontains=filter_value)

        # search column 'basic_subscription'
        idx = getListIndex(self.columns_data, 'basic_subscription')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(basic_subscription__subscription__icontains=filter_value)

        # search column 'is_paying'
        idx = getListIndex(self.columns_data, 'is_paying')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(is_paying__icontains=filter_value)

        # search column 'is_active'
        idx = getListIndex(self.columns_data, 'is_active')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(is_active__icontains=filter_value)

        return qs

    def get_context_data(self, *args, **kwargs):
        ret = super().get_context_data(self, *args, **kwargs)

        # add unique values for dropdown filters
        ret['col_6_selections'] = list(SocietyMember.objects.filter(is_archived=False).order_by(
            'company__country__name').values_list('company__country__name', 'company__country__pk').distinct())

        ret['col_16_selections'] = []
        for item in SocietyMember.objects.filter(is_archived=False).order_by(
                'payment_due_date').values_list('payment_due_date', flat=True).distinct():
            if item:
                ret['col_16_selections'].append({
                    0: '{0}. {1}'.format(item.day, item.month_name),
                    1: '{0}{1}'.format(str(item.month).rjust(2, '0'), str(item.day).rjust(2, '0'))
                })

        ret['col_18_selections'] = [{0: _('is paying'), 1: 1}, {0: _('not paying'), 1: 0}]
        ret['col_19_selections'] = [{0: _('is active'), 1: 1}, {0: _('not active'), 1: 0}]

        return ret
