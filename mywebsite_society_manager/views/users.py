# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

import json

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import user_passes_test
from django.db import transaction
from django.db.models import Case, Value, When
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext as _

from mywebsite_society_manager import context_processors, rights
from mywebsite_society_manager.forms import ProfileForm, UserForm
from mywebsite.utils.tools import LazyEncoder

# Get correct user model
User = get_user_model()


# List users
def list_users(request, state_id=None):
    if request.user.is_superuser:
        user_list = User.objects.all().order_by("username")
    else:
        user_list = User.objects.filter(id=request.user.id)
    return render(request, 'society_manager/users/list_users.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
@transaction.atomic
def manage_user(request, user_id=None):
    # Try to locate the object to use it as an instance and if not, create a new one
    # to use it in a new form.
    # common_data = common_ticket_data()
    if request.user.is_superuser:
        if user_id:
            actual_user = get_object_or_404(User, pk=user_id)
            actual_user_profile = actual_user.profile_society_manager
        else:
            actual_user = User()
            actual_user_profile = None
    else:
        if user_id and int(user_id) == request.user.id:
            actual_user = get_object_or_404(User, pk=user_id)
            actual_user_profile = actual_user.profile_society_manager
        else:
            messages.error(request, _('You don\'t have enough permissions to edit this setting'))
            return redirect('society_manager:user-list')

    # override *_columns from context_processor with current selected user
    common_data = context_processors.get_datatable_columns(request, user_id)
    ticket_columns = common_data['ticket_columns']
    prospect_columns = common_data['prospect_columns']
    member_columns = common_data['member_columns']

    # POST mode
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=actual_user)
        if user_form.is_valid():
            user_form.save()
            # profile is available after saving new user instance
            profile_form = ProfileForm(request.POST, instance=actual_user.profile_society_manager)
            if profile_form.is_valid():
                profile_form.save()
            return redirect('society_manager:user-list')

    else:
        # Non-POST mode, show only
        user_form = UserForm(instance=actual_user)
        profile_form = ProfileForm(instance=actual_user_profile)
    return render(request, 'society_manager/users/create_edit_user.html', locals())


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def delete_user(request, user_id=None):
    if request.user.is_superuser:
        if user_id:
            actual_user = get_object_or_404(User, pk=user_id)
            actual_user.delete()
            return redirect('society_manager:user-list')
        else:
            messages.error(request, _('You don\'t have enough permissions to delete this setting'))
            return redirect('society_manager:user-list')


@user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def set_collapsednavbar_jx(request, user_id=None):
    if request.is_ajax() and request.POST and [request.user.id == request.POST.get('submited_user_id')]:
        user_to_toggle = get_object_or_404(User, pk=request.user.id)
        # toggle has_collapsednavbar's boolean value
        user_to_toggle.profile_society_manager.has_collapsednavbar = not user_to_toggle.profile_society_manager.has_collapsednavbar
        user_to_toggle.profile_society_manager.save()
        data = {'message': _('Navbar toggled'), 'success': True}
        return JsonResponse(data, encoder=LazyEncoder, safe=False)
    else:
        data = {'message': _('Can\'t toggle navbar'), 'success': False}
        return JsonResponse(data, encoder=LazyEncoder, safe=False)
