# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import QueueForm
from mywebsite_society_manager.models import Queue


# List queues
def list_queues(request):
    if request.user.is_superuser:
        queues_list = Queue.objects.filter(is_archived=False)
        return render(request, 'society_manager/queues/list_queues.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


def manage_queue(request, queue_id=None):
    # Try to locate the object to use it as an instance and if not, create a new one to use it
    # in a new form.
    # common_data = common_ticket_data()
    if request.user.is_superuser:
        if queue_id:
            actual_queue = get_object_or_404(Queue, pk=queue_id)
        else:
            actual_queue = Queue()
        # POST mode
        if request.method == 'POST':
            form = QueueForm(request.POST, instance=actual_queue)
            if form.is_valid():
                form.save()
                return redirect('society_manager:queue-list')
        else:
            # Non-POST mode, show only
            form = QueueForm(instance=actual_queue)
        return render(request, 'society_manager/queues/create_edit_queue.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:queue-list')


# Archive queue
def archive_queue(request, queue_id=None):
    if request.user.is_superuser:
        if queue_id:
            actual_queue = get_object_or_404(Queue, pk=queue_id)
            actual_queue.is_archived = True
            actual_queue.save()
            return redirect('society_manager:queue-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:queue-list')
