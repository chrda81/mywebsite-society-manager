# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import StateForm
from mywebsite_society_manager.models import State


# List states
def list_state(request):
    if request.user.is_superuser:
        state_list = State.objects.filter(is_archived=False)
        return render(request, 'society_manager/states/list_states.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


# Create or edit state
def manage_state(request, state_id=None):
    if request.user.is_superuser:
        # Try to locate the object to use it as an instance and if not, create a new one
        # to use it in a new form.
        # common_data = common_ticket_data()
        if state_id:
            actual_state = get_object_or_404(State, pk=state_id)
        else:
            actual_state = State()
        # POST mode
        if request.method == 'POST':
            form = StateForm(request.POST, instance=actual_state)
            if form.is_valid():
                form.save()
                return redirect('society_manager:state-list')
        else:
            # Non-POST mode, show only
            form = StateForm(instance=actual_state)
        return render(request, 'society_manager/states/create_edit_state.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:state-list')


# Archive state
def archive_state(request, state_id=None):
    if request.user.is_superuser:
        if state_id:
            actual_state = get_object_or_404(State, pk=state_id)
            actual_state.is_archived = True
            actual_state.save()
            return redirect('society_manager:state-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:state-list')
