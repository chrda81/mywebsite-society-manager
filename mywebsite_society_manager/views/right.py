# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _

from mywebsite_society_manager.forms import RightForm
from mywebsite_society_manager.models import Rights


# List rights
def list_rights(request):
    if request.user.is_superuser:
        rights_list = Rights.objects.filter(is_archived=False).order_by('grp_src_id', '-queue_dst')
        return render(request, 'society_manager/rights/list_rights.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to view this setting'))
        return redirect('society_manager:tickets-settings')


def manage_right(request, right_id=None):
    if request.user.is_superuser:
        if right_id:
            actual_right = get_object_or_404(Rights, pk=right_id)
        else:
            actual_right = Rights()
        # POST mode
        if request.method == 'POST':
            form = RightForm(request.POST, instance=actual_right)
            if form.is_valid():
                form.save()
                return redirect('society_manager:right-list')
        else:
            # Non-POST mode, show only
            form = RightForm(instance=actual_right)
        return render(request, 'society_manager/rights/create_edit_right.html', locals())
    else:
        messages.error(request, _('You don\'t have enough permissions to edit this setting'))
        return redirect('society_manager:right-list')


# Archive right
def archive_right(request, right_id=None):
    if request.user.is_superuser:
        if right_id:
            actual_right = get_object_or_404(Rights, pk=right_id)
            actual_right.is_archived = True
            actual_right.save()
            return redirect('society_manager:right-list')
    else:
        messages.error(request, _('You don\'t have enough permissions to archive this setting'))
        return redirect('society_manager:right-list')
