# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import decorators
from django.db.models import Q
from django.shortcuts import (HttpResponse, get_object_or_404, redirect,
                              render, reverse)
from django.urls import reverse_lazy
from django.utils.html import escape
from django.utils.translation import gettext as _
from django_datatables_view.base_datatable_view import BaseDatatableView

from mywebsite_society_manager import rights
from mywebsite_society_manager.forms import ProspectiveCompanyForm
from mywebsite_society_manager.models import (Attachment, Comments,
                                              ProspectiveCompany, Queue,
                                              SocietyMember)
from mywebsite_society_manager.utils import getListIndex, render_to_pdf


# Commond data & queries used to view/edit prospects
def common_prospect_data():
    # Static data
    main_tab_form_fields = 'name, additional_name, contact_title, contact_last_name, contact_first_name, website, email, mobile, \
        phone, fax, address, zipcode, location, country,'
    payment_tab_form_fields = 'sepa_bank_name, sepa_account_name, sepa_iban, sepa_bic'

    # Queries
    # users_info = User.objects.all()
    assigned_queue = get_object_or_404(Queue, shortcode=settings.PROSPECTS_QUEUE_SHORTCODE)

    return {
        'main_tab_form_fields': main_tab_form_fields,
        'payment_tab_form_fields': payment_tab_form_fields,
        'assigned_queue': assigned_queue
    }


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def list_prospects(request):
    """
    list_prospects: list functions called in urls.py used to list prospects under /prospects url
    """

    # Find only entries which have no 'members' relation
    prospects = ProspectiveCompany.objects.exclude(
        members__id__in=SocietyMember.objects.all().values_list('id', flat=True)
    ).filter(is_archived=False).order_by('name')

    return render(request, 'society_manager/prospects/list_prospects.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def view_prospect(request, prospect_id=None):
    # Common data
    common_data = common_prospect_data()
    if prospect_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_view:
            actual_prospect = get_object_or_404(ProspectiveCompany, pk=prospect_id)
            actual_files = Attachment.objects.filter(ticket_rel__assigned_company=actual_prospect,
                                                     is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel__assigned_company=actual_prospect,
                                                      is_archived=False).order_by('-created')
            if user_object_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to view this prospect'))
            return redirect('society_manager:prospects-list')
    else:
        messages.error(request, _('No prospect selected'))
        return redirect('society_manager:prospects-list')

    # POST mode
    form_prospect = ProspectiveCompanyForm(instance=actual_prospect, prefix="prospect", readonly_form=True)
    return render(request, 'society_manager/prospects/view.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def manage_prospect(request, prospect_id=None):
    # Common data
    common_data = common_prospect_data()
    if prospect_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_view:
            actual_prospect = get_object_or_404(ProspectiveCompany, pk=prospect_id)
            if user_object_rights.can_edit or request.user.is_superuser:
                can_edit = True
            else:
                can_edit = False
        else:
            messages.error(request, _('You don\'t have permissions to add this prospect'))
            return redirect('society_manager:prospects-list')

    else:
        messages.error(request, _('No prospect selected'))
        return redirect('society_manager:prospects-list')

    # POST mode
    if request.method == 'POST':
        form_prospect = ProspectiveCompanyForm(request.POST, instance=actual_prospect, prefix="prospect")

        if form_prospect.is_valid():
            form_prospect.save()

            if 'update-signal' in request.POST:
                return redirect('society_manager:prospects-edit', prospect_id)
            elif 'save-signal' in request.POST:
                return redirect('society_manager:prospects-list')

    else:
        # Non-POST mode, show only
        form_prospect = ProspectiveCompanyForm(instance=actual_prospect, prefix="prospect")

    return render(request, 'society_manager/prospects/create_edit_prospect.html', locals())


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def archive_prospect(request, prospect_id=None):
    # Common data
    common_data = common_prospect_data()

    """Check if we can archive trought get_rights_for_ticket"""
    user_object_rights = rights.get_rights_for_ticket(
        user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
    if user_object_rights.can_archive:
        obj_to_archive = get_object_or_404(ProspectiveCompany, pk=prospect_id)
        obj_to_archive.is_archived = True
        obj_to_archive.save()
        return redirect('society_manager:prospects-list')
    else:
        messages.error(request, _('You don\'t have permissions to archive this prospect'))
        return redirect('society_manager:prospects-list')


@decorators.user_passes_test(rights.is_in_society_managers_group, login_url=reverse_lazy('society_manager:login'))
def export_prospect(request, prospect_id=None):
    # Common data
    common_data = common_prospect_data()
    if prospect_id:
        # Check if exists
        user_object_rights = rights.get_rights_for_ticket(
            user=request.user, queue=common_data['assigned_queue'], ticket_id=None)
        if user_object_rights.can_view:
            actual_prospect = get_object_or_404(ProspectiveCompany, pk=prospect_id)
            actual_files = Attachment.objects.filter(ticket_rel__assigned_company=actual_prospect,
                                                     is_archived=False).order_by('file_name')
            actual_comments = Comments.objects.filter(ticket_rel__assigned_company=actual_prospect,
                                                      is_archived=False).order_by('-created')
            context = {
                'request': request,
                'title': actual_prospect.name,
                'json': actual_prospect.as_json(),
                'comments': actual_comments
            }

            # rendering the template
            return render_to_pdf('society_manager/prospects/export.html', context)
        else:
            messages.error(request, _('You don\'t have permissions to view this prospect'))
            return redirect('society_manager:prospects-list')

    else:
        messages.error(request, _('No prospect selected'))
        return redirect('society_manager:prospects-list')


# Datatables viewsets
class ProspectiveCompanyListJson(BaseDatatableView):
    # The model we're going to show
    model = ProspectiveCompany

    # define the columns that will be returned
    columns = [
        'pk',
        '',
        'name',
        'additional_name',
        'contact_title',
        'contact_last_name',
        'contact_first_name',
        'country.name',
        'zipcode',
        'location',
        'address',
        'email',
        'mobile',
        'phone',
        'fax',
        'labels',
    ]

    # define column names that will be used in sorting
    # order is important and should be same as order of columns
    # displayed by datatables. For non sortable columns use empty
    # value like ''
    order_columns = [
        '',
        '',
        'name',
        'additional_name',
        'contact_title',
        'contact_last_name',
        'contact_first_name',
        'country.name',
        'zipcode',
        'location',
        'address',
        'email',
        'mobile',
        'phone',
        'fax',
        'labels',
    ]

    # set max limit of records returned, this is used to protect our site if someone tries to attack our site
    # and make it return huge amount of data
    max_display_length = 500

    def get_initial_queryset(self):
        # return queryset used as base for futher sorting/filtering
        # these are simply objects displayed in datatable
        # You should not filter data returned here by any filter values entered by user. This is because
        # we need some base queryset to count total number of records.
        return ProspectiveCompany.objects.exclude(
            members__id__in=SocietyMember.objects.all().values_list('id', flat=True)
        ).filter(is_archived=False).order_by('name')

    def prepare_results(self, qs):
        # prepare list with output column data
        # queryset is already paginated here
        # escape HTML for security reasons
        json_data = []
        for item in qs:
            json_data.append([
                item.pk,
                '',
                escape(item.name) if item.name else '',
                escape(item.additional_name) if item.additional_name else '',
                escape(item.contact_title) if item.contact_title else '',
                escape(item.contact_last_name) if item.contact_last_name else '',
                escape(item.contact_first_name) if item.contact_first_name else '',
                escape(item.country) if item.country else '',
                escape(item.zipcode) if item.zipcode else '',
                escape(item.location) if item.location else '',
                escape(item.address) if item.address else '',
                escape(item.email) if item.email else '',
                escape(item.mobile) if item.mobile else '',
                escape(item.phone) if item.phone else '',
                escape(item.fax) if item.fax else '',
                ', '.join(list(item.labels.values_list('name', flat=True))) if item.labels else '',
            ])
        return json_data

    def filter_queryset(self, qs):
        # use parameters passed in POST request to filter queryset

        # global search:
        search = self.request.POST.get('search[value]', None)
        if search:
            q = Q(name__icontains=search) |\
                Q(additional_name__icontains=search) |\
                Q(contact_title__icontains=search) |\
                Q(contact_last_name__icontains=search) |\
                Q(contact_first_name__icontains=search) |\
                Q(country__name__icontains=search) |\
                Q(zipcode__icontains=search) |\
                Q(location__icontains=search) |\
                Q(address__icontains=search) |\
                Q(email__icontains=search) |\
                Q(mobile__icontains=search) |\
                Q(phone__icontains=search) |\
                Q(fax__icontains=search) |\
                Q(labels__name__icontains=search)
            qs = qs.filter(q)

        # search column 'name'
        idx = getListIndex(self.columns_data, 'name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(name__icontains=filter_value)

        # search column 'additional_name'
        idx = getListIndex(self.columns_data, 'additional_name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(additional_name__icontains=filter_value)

        # search column 'contact_title'
        idx = getListIndex(self.columns_data, 'contact_title')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(contact_title__icontains=filter_value)

        # search column 'contact_last_name'
        idx = getListIndex(self.columns_data, 'contact_last_name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(contact_last_name__icontains=filter_value)

        # search column 'contact_first_name'
        idx = getListIndex(self.columns_data, 'contact_first_name')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(contact_first_name__icontains=filter_value)

        # search column 'country'
        idx = getListIndex(self.columns_data, 'country')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(country__pk__iexact=filter_value)

        # search column 'zipcode'
        idx = getListIndex(self.columns_data, 'zipcode')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(zipcode__icontains=filter_value)

        # search column 'location'
        idx = getListIndex(self.columns_data, 'location')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(location__icontains=filter_value)

        # search column 'address'
        idx = getListIndex(self.columns_data, 'address')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(address__icontains=filter_value)

        # search column 'email'
        idx = getListIndex(self.columns_data, 'email')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(email__icontains=filter_value)

        # search column 'mobile'
        idx = getListIndex(self.columns_data, 'mobile')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(mobile__icontains=filter_value)

        # search column 'phone'
        idx = getListIndex(self.columns_data, 'phone')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(phone__icontains=filter_value)

        # search column 'fax'
        idx = getListIndex(self.columns_data, 'fax')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(fax__icontains=filter_value)

        # search column 'labels'
        idx = getListIndex(self.columns_data, 'labels')
        if idx:
            var_name = 'columns[' + str(idx) + '][search][value]'
            filter_value = self.request.POST.get(var_name, None)
            if filter_value:
                qs = qs.filter(labels__name__icontains=filter_value)

        return qs

    def get_context_data(self, *args, **kwargs):
        ret = super().get_context_data(self, *args, **kwargs)

        # add unique values for dropdown filters
        ret['col_7_selections'] = list(ProspectiveCompany.objects.filter(is_archived=False).order_by(
            'country__name').values_list('country__name', 'country__pk').distinct())

        return ret
