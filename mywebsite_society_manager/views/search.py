# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import redirect, render

from mywebsite_society_manager.filters import TicketFilter
from mywebsite_society_manager.models import Ticket

from .tickets import common_ticket_data

# Get correct user model
User = get_user_model()


# List tickets
def main_search(request, state_id=None):
    common_data = common_ticket_data()

    # url name is needed for datatables ajax call, to build the correct initial query
    # have a look at TicketListJson.get_initial_queryset()
    url_name = request.resolver_match.url_name

    if request.is_ajax() and request.POST:
        """vars from post"""
        """
        Some Q objects magic
        http://www.michelepasin.org/blog/2010/07/20/the-power-of-djangos-q-objects/
        """
        assigned_company_data = request.POST.get('assigned_company_text')
        subject_data = request.POST.get('subject_text')
        body_data = request.POST.get('body_text')
        comment_data = request.POST.get('comment_text')
        assigned_user_data = request.POST.get('assigned_id')
        creator_user_data = request.POST.get('creator_id')
        status_data = request.POST.get('status_id')
        active_member_data = request.POST.get('is_active')

        """Q config"""
        q_objects = Q()
        q_objects &= Q(assigned_company__icontains=assigned_company_data)  # 'or' the Q objects together
        q_objects &= Q(subject__icontains=subject_data)
        q_objects &= Q(body__icontains=body_data)
        if comment_data != '':
            q_objects &= Q(ticket_rel_comm__comment__icontains=comment_data)
        if assigned_user_data != '':
            q_objects &= Q(assigned_user__in=assigned_user_data)
        if creator_user_data != '':
            q_objects &= Q(create_user__in=creator_user_data)
        if status_data != '':
            q_objects &= Q(assigned_state__in=status_data)

        # Admin results scope"""
        query_results = list()
        if request.user.is_superuser:
            # qry_results = Ticket.objects.filter(reduce(operator.and_, q_objects))
            query_results = Ticket.objects.filter(q_objects)
        else:
            pass
        data = [obj.as_json() for obj in query_results]
        return JsonResponse(data, safe=False)

    return render(request, 'society_manager/search/search_form.html', locals())


def search(request):
    tickets_list = []
    tickets_filter = TicketFilter(None, queryset=None)

    # url name is needed for datatables ajax call, to build the correct initial query
    # have a look at TicketListJson.get_initial_queryset()
    # all POST vars from above are bypassed to ajax call
    url_name = request.resolver_match.url_name

    if request.POST:
        requested_subject = request.POST.get('subject', None)
        requested_body = request.POST.get('body', None)
        requested_comment = request.POST.get('comment', None)
        requested_assigned_company = request.POST.get('assigned_company', None)
        requested_assigned_state = request.POST.get('assigned_state', None)
        requested_assigned_user = request.POST.get('assigned_user', None)
        requested_create_user = request.POST.get('create_user', None)
        requested_active_member = request.POST.get('active_member', None)

        if not requested_subject and not requested_body and not requested_assigned_company and \
                not requested_assigned_state and not requested_assigned_user and not requested_create_user and \
                not requested_comment and not requested_active_member:
            return redirect('society_manager:search')

        tickets_filter = TicketFilter(request.POST, queryset=None)

    # Need to check if user is superuser?
    return render(request, 'society_manager/search/search_form.html', locals())
