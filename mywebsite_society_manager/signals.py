# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.template.loader import get_template
from django.utils.translation import gettext as _

from notifications.models import Notification

from .models import Comments, Profile, ProspectiveCompany, ProspectiveCompanyLabels, State, Ticket

# User model alias
User = get_user_model()


# subscribe to event post_save of table 'User'
@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance=None, created=False, **kwargs):
    if created or not hasattr(instance, 'profile_society_manager'):
        Profile.objects.create(user=instance)
    instance.profile_society_manager.save()


# subscibe to event pre_save of table 'Ticket'
@receiver(pre_save, sender=Ticket)
def delete_prospective_company_labels(sender, instance=None, **kwargs):
    try:
        old_ticket = Ticket.objects.get(pk=instance.pk)
        old_label_list = old_ticket.get_label_list()
        new_label_list = instance.get_label_list()
        if old_ticket.assigned_company:
            # Split tagify's labels by delimiter set to ','
            for label in old_label_list.split(','):
                if str(label) and label not in new_label_list.split(','):
                    label_to_delete = ProspectiveCompanyLabels.objects.filter(
                        company=instance.assigned_company, name=label)
                    if label_to_delete:
                        label_to_delete.delete()
    except Ticket.DoesNotExist:
        # No label stuff to do here
        pass


# subscribe to event post_save of table 'Ticket'
@receiver(post_save, sender=Ticket, dispatch_uid="update_ticket_labels")
def create_prospective_company_labels(sender, instance=None, created=False, **kwargs):
    if instance.assigned_company:
        # First mode: Ticket -> Company:
        # Split tagify's labels by delimiter set to ','
        label_list = instance.get_label_list()
        for label in label_list.split(','):
            # If label with given name of Ticket related ProspectiveCompany is not existent..
            if str(label) and not ProspectiveCompanyLabels.objects.filter(company=instance.assigned_company,
                                                                          name=label):
                new_label = ProspectiveCompanyLabels()
                new_label.name = label
                new_label.company = instance.assigned_company
                new_label.tagify_data = instance.get_label_data(label)
                new_label.save()

        # Second mode: Company -> Ticket:
        # Restore saved labels from company to new created tickets
        is_dirty = False
        for label_obj in ProspectiveCompanyLabels.objects.filter(company=instance.assigned_company):
            if str(label_obj.name) and label_obj.name not in label_list.split(','):
                # append missing labels in JSON mode
                if str(instance.labels):
                    instance_labels_json = json.loads(instance.labels)
                else:
                    instance_labels_json = json.loads('{}')
                new_labels_json = json.loads(label_obj.tagify_data)
                merged_labels = [*instance_labels_json, *new_labels_json]
                # build json like string without encoding to utf-8, which tagify understands
                json_str = str(merged_labels).replace("'", '"')
                instance.labels = json_str
                is_dirty = True

        if is_dirty:
            # to avoid getting maximum recursion depth exceeded, we disconnect signals, before saving within
            # the signal handler
            post_save.disconnect(create_prospective_company_labels, sender=Ticket, dispatch_uid="update_ticket_labels")
            instance.save()
            post_save.connect(create_prospective_company_labels, sender=Ticket, dispatch_uid="update_ticket_labels")


# subscibe to event pre_save of table 'Ticket'
@receiver(pre_save, sender=Ticket)
def execute_ticket_statemachine(sender, instance=None, **kwargs):
    # Common data
    # if instance.can_send_mail:
    #    # mail data
    #     contact_fullname = '{} {} {}'.format(
    #         instance.assigned_company.contact_title,
    #         instance.assigned_company.contact_first_name,
    #         instance.assigned_company.contact_last_name
    #     )
    #     context = {
    #         'contact_name': contact_fullname,
    #         'ticket_nr': instance.nr
    #     }
    #     mail_recipients = []
    #     if settings.EMAILTEST:
    #         [mail_recipients.append(obj) for obj in settings.EMAILTO.split(',')]
    #     else:
    #         mail_recipients = [instance.assigned_company.email]
    #     email = EmailMessage(
    #         from_email=settings.DEFAULT_FROM_EMAIL,
    #         to=mail_recipients,
    #         # bcc=[settings.DEFAULT_FROM_EMAIL]
    #     )

    # Prospect is applying for membership: Set 'is_applying_for_membership' => True
    if instance.assigned_company and instance.assigned_queue.shortcode == settings.MEMBERS_QUEUE_SHORTCODE:
        instance.assigned_company.is_applying_for_membership = True
        instance.assigned_company.save()

    # Ticket state model
    # New: Ticket instance doesn't exist yet
    if not instance.id:
        instance.new()
        # if instance.needs_to_send_mail:
        #     # Build Email structure with the contact information
        #     mail_template = get_template('society_manager/emails/ticket_new.txt')
        #     content = mail_template.render(context)
        #     # Send Email
        #     email.subject = _('[Ticket#%(ticket_nr)s] Your request "%(subject)s" is created') % {
        #         'ticket_nr': instance.nr, 'subject': instance.subject}
        #     email.body = content
        #     email.content_subtype = 'html'
        #     email.send()

    # Open: Ticket is open
    elif instance.id and instance.assigned_state.shortcode == "open":
        instance.open()

    # In Progress: Ticket is in progress
    elif instance.id and instance.assigned_state.shortcode == "in_progress":
        instance.in_progress()

    # Is Pending: Ticket is pending
    elif instance.id and instance.assigned_state.shortcode == "is_pending":
        instance.is_pending()

    # Closed: Ticket is closed
    elif instance.id and instance.assigned_state.shortcode == "closed":
        instance.close()


# subscribe to event post_save of table 'Ticket'
@receiver(post_save, sender=Ticket, dispatch_uid="update_notifications")
def mark_notifications_as_read(sender, instance=None, created=False, **kwargs):
    # Closed: Ticket is closed
    if instance.id and instance.assigned_state.shortcode == "closed":
        # find all depending notifications for this ticket
        for notification in Notification.objects.filter(actor_object_id=instance.id, unread=True):
            notification.mark_as_read()
