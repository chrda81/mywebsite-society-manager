# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/seddonym/django-yearlessdate!
--------------------------------------------------------------
"""

import calendar
import locale

from django import forms
from django.conf import settings
from django.core.validators import ValidationError
from django.forms.widgets import MultiWidget, Select
from django.utils.deconstruct import deconstructible
from django.utils.translation import get_language

locale.setlocale(locale.LC_ALL, settings.LANG_TO_LOCALE.get(get_language()))
DAY_CHOICES = tuple([('', '---------')] + [(i, i) for i in range(1, 32)])
MONTH_CHOICES = tuple([('', '---------')] + [(i, calendar.month_name[i]) for i in range(1, 13)])


@deconstructible
class YearlessDate(object):
    """ An object representing a date in a year (but not a year itself).
        Suitable especially for birthdays, anniversaries etc.
    """

    def __init__(self, day, month):
        self.day = int(day)
        self.month = int(month)
        self._validate()

    def _validate(self):
        "Checks that the values of day and month are valid"
        # Get valid max month days for the month (we use 2008 since it's a leap year)
        try:
            week, month_days = calendar.monthrange(2008, self.month)
        except calendar.IllegalMonthError:
            raise Exception('Cannot create DateInYear object with a month value of %d.' % self.month)

        if self.day < 1 or self.day > month_days:
            raise Exception('Cannot create DateInYear object - invalid day value %d for month %s.' %
                            (self.day, self.month_name))

    @property
    def month_name(self):
        "Returns the full name of the month"
        return calendar.month_name[self.month]

    def __str__(self):
        return "%d. %s" % (self.day, self.month_name)

    def __eq__(self, other):
        if not isinstance(other, YearlessDate):
            return False
        return (self.day == other.day) and (self.month == other.month)

    def __gt__(self, other):
        if self.month != other.month:
            return self.month > other.month
        return self.day > other.day

    def __ne__(self, other):
        return not self == other

    def __le__(self, other):
        return not self > other

    def __ge__(self, other):
        return (self > other) or self == other

    def __lt__(self, other):
        return not self >= other


class YearlessDateSelect(MultiWidget):
    def __init__(self, *args, **kwargs):
        widgets = (
            Select(attrs={'class': 'select form-control'}, choices=DAY_CHOICES),
            Select(attrs={'class': 'select form-control'}, choices=MONTH_CHOICES),
        )
        super(YearlessDateSelect, self).__init__(widgets=widgets, *args, **kwargs)

    def decompress(self, value):
        if value is None:
            return [None, None]
        return [value.day, value.month]


class YearlessDateFormField(forms.Field):
    widget = YearlessDateSelect

    def clean(self, value):
        if value == ['', '']:
            # If the values are both None, trigger the default validation for null
            super(YearlessDateFormField, self).clean(None)
        else:
            try:
                return YearlessDate(*value)
            except:
                raise ValidationError('Invalid date.')
