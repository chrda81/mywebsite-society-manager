/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 *
 * Credit for contents of this file goes to the project:
 *
 *  "name": "datatables-filterwidgets",
 *  "version": "1.1.1",
 *  "main": "datatables-filterwidgets.js",
 *  "homepage": "https://github.com/cristoper/datatables-filterwidgets",
 *  "authors": [
 *  "cristoper <chris@mretc.net>"
 *  "description": "A DataTables extension to easily add per-column filter widgets (search box, range slider, date picker, selection menu) to a table.",
 *  "license": "WTFPL",
 *  "dependencies":
 *   - DataTables >=1.10.x (https://datatables.net/)
 *   - MomentJS
 *   - jQuery-ui-Slider-Pips (https://github.com/simeydotme/jQuery-ui-Slider-Pips/)
 */

import moment from 'node_modules/moment';
import _ from 'node_modules/lodash';

(function($, document, window) {
    var defaults = {
        implicit: 'auto',
    };

    /* When a Datatable initializes, check to see if it is configured for
     * columnFilters */
    $(document).on('init.dt', function(e, settings, json) {
        if (e.namespace !== 'dt') {
            return;
        }

        var opts = settings.oInit.columnFilters;
        if (opts === true) {
            opts = {};
        }
        $.extend(opts, defaults);

        if (opts) {
            addColumnFilters(settings, opts);
        }
    });

    // Catch the 'responsive-resize' events which fires before the 'init.dt' event
    $(document).on('responsive-resize.dt', function(e, datatable, columns) {
        var table = datatable.table().node();
        if (!table.colfil_state) {
            initialize_table_state(table);
        }
        table.colfil_state.resp_columns = columns;
    });

    /**
     * @param {HTMLnode} tableNode - the table being initialized
     */
    function initialize_table_state(tableNode) {
        // Store state in DOM object
        tableNode.colfil_state = {};
        tableNode.colfil_state.widgetArray = [];
        tableNode.resp_columns = [];
        tableNode.cached_api = {};
    }

    /** The main function of our extension; it sets everything up and adds a
     * row to the table header containing all of the filter widgets.
     *
     * @param {Object} settings - the DataTables settings object (passed, for
     * example, from an event) which can be used to create an instance of a
     * DataTables API object for the table of interest. See:
     * https://datatables.net/reference/type/DataTables.Settings
     *
     * @param {Object} opts - the options passed to our extension in the
     * DataTable() function.
     *
     * Example opts object:
     *
     * {
     *   0: { type: 'Range' },
     *   2: 'date',
     *   3: { type: 'Text',
     *        prefix: '$'
     *      },
     *   implicit: 'None'
     * }
     *
     */
    function addColumnFilters(settings, opts) {
        var dTable = $.fn.dataTable.Api(settings);

        // Store state in DOM object
        var table = dTable.table().node();
        if (!table.colfil_state) {
            initialize_table_state(table);
        }
        var header = $(dTable.table().header()); // jQuery
        var controlRow = $('<tr id="columnFiltersRow"></tr>');

        dTable.columns().every(function() {
            // create a control column for every table column
            var i = this.index();
            var isVisible = this.visible();
            var colType = settings.aoColumns[i].sType;
            var controlCell = $('<td></td>'); // jQuery
            if (!isVisible) {
                controlCell.hide();
            }

            // Get widget type based on config options
            var type =
                (opts[i] && opts[i].type) ||
                (typeof opts[i] === 'string' && opts[i]) ||
                opts.implicit ||
                'none';
            type = type.toLowerCase();
            if (type == 'auto') {
                // See: https://datatables.net/reference/option/columns.type
                switch (colType) {
                    case 'date':
                        type = 'date';
                        break;
                    case 'num':
                        type = 'range';
                        break;
                    default:
                        type = 'text';
                }
            }

            // Add the widgets
            var widget = new widgetConstructors[type](dTable, i, opts[i]);
            controlCell.html(widget.html);
            controlRow.append(controlCell);
            table.colfil_state.widgetArray.push(widget);
        });

        controlRow.find('td').css('overflow', 'visible');

        // Hide any columns already hidden by the Responsive extension
        if (table.colfil_state.resp_columns) {
            show_hide_columns(table.colfil_state.resp_columns);
        }

        // Add the control row to the table
        header.append(controlRow);

        // Keep in sync with visibility of columns
        // (for example, as set by Buttons extension)
        dTable.on('column-visibility.dt', function(e, settings, column, state) {
            var col = $(controlRow.children()[column]);
            state == true ? col.show() : col.hide();

            // Workaround for Responsive extension issue with multiple header rows
            // https://github.com/DataTables/Responsive/issues/71
            if (dTable.responsive) {
                dTable.responsive.rebuild();
                dTable.responsive.recalc();
            }
        });

        // Keep in sync with visibility controlled by Responsive extension
        $(document).off('responsive-resize.dt');
        dTable.on('responsive-resize.dt', function(e, datatable, columns) {
            show_hide_columns(columns);
        });

        // custom search for filtering via our widgets
        $.fn.dataTable.ext.search.push(function(
            settings,
            searchData,
            index,
            rowData,
            counter
        ) {
            if (!settings.nTable) {
                // settings is a private API, so nTable might not exist
                // in future versions
                settings.nTable = new $.fn.dataTable.Api(settings)
                    .table()
                    .node();
            }
            var table = settings.nTable;

            if (counter == 0) {
                table.dTable = new $.fn.dataTable.Api(settings);
            }
            var api = table.dTable;
            var header = $(api.table().header());
            var widgetArray = table.colfil_state.widgetArray;

            if (!widgetArray) {
                return true;
            }

            for (var i = 0; i < widgetArray.length; i++) {
                var widget = widgetArray[i];
                if (widget.filter && !widget.filter(searchData[i])) {
                    // If ANY filter returns false, then don't show the row
                    return false;
                }
            }
            return true;
        });

        /** Show/hide control columns based on Responsive extension
         * We must loop only over visible columns, because the Responsive
         * extension has no knowledge of hidden columns
         *
         * @param {Array} columns - true=show; false=hide
         */
        function show_hide_columns(columns) {
            var visibleCols = dTable
                .columns()
                .visible()
                .filter(function(val) {
                    return val;
                });
            columns.forEach(function(is_visible, index) {
                var col = $(controlRow.children()[index]);
                is_visible == true ? col.show() : col.hide();
            });
        }
    }

    /* Every widget constructor is passed a reference to the DataTable API
     * object, the column index, and any options passed during configuration,
     * and it must return an object with two properties: 'html' the html
     * element to insert in the control row, and 'filter' a function which is
     * passed a cell value and must return true (show row) or false (hide row)
     *
     * TODO: need to add a DataTables API call to extend this with more widget types.
     * */
    var widgetConstructors = {
        range: TextWidget, //RangeWidget,
        none: NoneWidget,
        date: DateWidget,
        text: TextWidget,
        select: SelectWidget,
    };

    // Construct a Range widget (two-handled slider)
    // function RangeWidget(dTable, colIndex, opts) {
    //     opts = opts || {};
    //     var data = dTable.column(colIndex).data();
    //     var slider = $("<div class='range-slider'></div>");
    //     this.numSteps = opts.numSteps || 10;
    //     this.min = opts.min || data.min();
    //     // The maximum calculation depends on numSteps and min:
    //     this.max = opts.max || calcEvenMax(data.max(), this.min, this.numSteps);
    //     // The step size depends on the (even) max, min and numSteps
    //     this.step = opts.step || calcStepSize(this.max, this.min, this.numSteps);
    //     this.defaults = opts.defaults || [this.min, this.max];
    //     this.prefix = opts.prefix || '';
    //     this.suffix = opts.suffix || '';
    //     this.pips = opts.pips || false;
    //     var widget = this;

    //     // Turn it into a jQuery-ui slider
    //     slider.slider({
    //         min: this.min,
    //         max: this.max,
    //         step: this.step,
    //         values: [this.min, this.max],
    //         range: true,
    //         slide: function(e, ui) {
    //             var value = ui.values;
    //             widget.min = value[0];
    //             widget.max = value[1];
    //             dTable.draw();
    //         }
    //     }).slider("float", {
    //         prefix: widget.prefix,
    //         suffix: widget.suffix,
    //         formatLabel: formatLabel
    //     });

    //     // get HTML from jQuery
    //     this.html = slider.get();

    //     /** Called by custom filter whenever DataTable is drawn
    //      *
    //      * @param {Number} value - the value in the table cell. We test this
    //      * value against the slider's set range.
    //      * @returns {Bool} false if the value is outside of range, true if it
    //      * is within the range.
    //      */
    //     this.filter = function(value) {
    //         value = parseFloat(value);
    //         var max = this.max;
    //         var min = this.min;

    //         // Range filters are not concerned with NaNs, so let them pass
    //         if (isNaN(value)) {
    //             return true;
    //         }

    //         return (min <= value && value <= max);
    //     }

    //     /*** Helper Functions (used by above code) ***/

    //     /** calculate a reasonable step size
    //      *
    //      * @param {Number} max - the maximum of the dataset
    //      * @param {Number} min - the minimum of the dataset
    //      * @returns {Number} The stepsize to use
    //      */
    //     function calcStepSize(max, min, numSteps) {
    //         return Math.floor((max-min)/numSteps);
    //     }

    //     /** Calculate a new maximum so that (max-min) is evenly divided by
    //      * this.steps
    //      *
    //      * @param {Number} max - the original maximum value
    //      * @returns {Number} the new (larger) maximum value
    //      */
    //     function calcEvenMax(max, min, numSteps) {
    //         var modulus = (max-min) % numSteps;
    //         var diff = numSteps - modulus;
    //         return max + diff;
    //     }

    //     /** Make numbers short and readable for printing on slider float
    //     */
    //     function formatLabel(val) {
    //         return this.prefix + shortenLargeNumber(val,0) + this.suffix;
    //     }

    //     /** Taken from:
    //      * @see http://stackoverflow.com/a/28608086/408930
    //      *
    //      * Shorten number to thousands, millions, billions, etc.
    //      * @see http://en.wikipedia.org/wiki/Metric_prefix
    //      *
    //      * @param {number} num Number to shorten.
    //      * @param {number} [digits=0] The number of digits to appear after the decimal point.
    //      * @returns {string|number}
    //      *
    //      * @example
    //      * // returns '12.5k'
    //      * shortenLargeNumber(12543, 1)
    //      *
    //      * @example
    //      * // returns '-13k'
    //      * shortenLargeNumber(-12567)
    //      *
    //      * @example
    //      * // returns '51M'
    //      * shortenLargeNumber(51000000)
    //      *
    //      * @example
    //      * // returns 651
    //      * shortenLargeNumber(651)
    //      *
    //      * @example
    //      * // returns 0.12345
    //      * shortenLargeNumber(0.12345)
    //      */
    //     function shortenLargeNumber(num, digits) {
    //         var units = ['k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'],
    //             decimal;

    //             for (var i=units.length-1; i>=0; i--) {
    //                 decimal = Math.pow(1000, i+1);

    //                 if (num <= -decimal || num >= decimal) {
    //                     return +(num / decimal).toFixed(digits) + units[i];
    //                 }
    //             }
    //             return num;
    //     }

    // } // RangeWidget

    // Construct a None widget (no widget)
    function NoneWidget() {
        this.html = '';
        this.filter = function() {
            return true;
        };
    }

    // Construct a Text widget
    function TextWidget(dTable, colIndex, opts) {
        var input = $("<input type='search'></input>");

        // Note that the oninput event is supported by IE9+
        // (but is buggy in IE9 (http://help.dottoro.com/ljhxklln.php)
        input.on('input', function() {
            dTable
                .column(colIndex)
                .search(this.value)
                .draw();
            $(this).focus();
        });
        input.css('width', '90%');
        this.html = input;
    }

    // Construct a Date widget
    function DateWidget(dTable, colIndex, opts) {
        // // sort moments array with lodash
        // var data = _.uniq(_.sortBy(dTable.column(colIndex).data(), function (value) {
        //     return moment(value).format($dateformat);
        // }).reverse());
        // var min_date = moment(data[0], $dateformat);
        // var max_date = moment(data[data.length - 1], $dateformat);
        // var from_to_input = $(
        //     "<input type='text' name='from_to' id='from_to_date" +
        //         colIndex +
        //         "' data-toggle='tooltip'></input>"
        // );

        var from_to_input = $(
            '<span id="from_to_date' +
                colIndex +
                '" class="fa fa-calendar text-center" data-toggle="tooltip"></span>'
        );

        this.set_min = moment('19700101');
        this.set_max = moment()
            .add(10, 'years')
            .format($dateformat);

        var widget = this;

        from_to_input.daterangepicker(
            {
                showISOWeekNumbers: true,
                opens: 'left',
                timePicker: true,
                timePicker24Hour: true,
                autoUpdateInput: false,
                showCustomRangeLabel: false,
                showDropdowns: true,
                alwaysShowCalendars: true,
                todayHighlight: true,
                autoApply: true,
                startDate: this.set_min,
                endDate: this.set_max,
                locale: $dp_locale,
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [
                        moment().subtract(1, 'days'),
                        moment().subtract(1, 'days'),
                    ],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [
                        moment().startOf('month'),
                        moment().endOf('month'),
                    ],
                    'Last Month': [
                        moment()
                            .subtract(1, 'month')
                            .startOf('month'),
                        moment()
                            .subtract(1, 'month')
                            .endOf('month'),
                    ],
                    'Show All': [this.set_min, this.set_max],
                },
            },
            function(start, end, label) {
                widget.set_min = start;
                widget.set_max = end;
                from_to_input.attr(
                    'value',
                    moment(start).format($dateformat) +
                        ' - ' +
                        moment(end).format($dateformat)
                );
                dTable
                    .column(colIndex)
                    .search(
                        moment(start).format($dateformat) +
                            ' - ' +
                            moment(end).format($dateformat)
                    )
                    .draw();
                $(this).focus();
            }
        );
        from_to_input.css('width', '90%');
        from_to_input.css('cursor', 'pointer');
        from_to_input.css('font-size', '1.4em');
        from_to_input.attr(
            'title',
            moment('19700101').format($dateformat) +
                ' - ' +
                moment().format($dateformat)
        );

        // Locale for datarangepicker range keys
        $('.ranges li[data-range-key="Today"]').text(sNow);
        $('.ranges li[data-range-key="Yesterday"]').text(sYesterday);
        $('.ranges li[data-range-key="Last 7 Days"]').text(sLast7);
        $('.ranges li[data-range-key="Last 30 Days"]').text(sLast30);
        $('.ranges li[data-range-key="This Month"]').text(sThisMonth);
        $('.ranges li[data-range-key="Last Month"]').text(sLastMonth);
        $('.ranges li[data-range-key="Show All"]').text(sShowAll);

        var html_div = $("<div class='datepicker'></div>");
        html_div.append(from_to_input);

        this.html = html_div;

        /** Called by custom filter whenever DataTable is drawn
         *
         * @param {Number} value - the value in the table cell. We test this
         * value against the daterangepicker's set range.
         * @returns {Bool} false if the value is outside of range, true if it
         * is within the range.
         */
        this.filter = function(value) {
            value = moment(value, $dateformat);
            var max = moment(this.set_max, $dateformat).add(1, 'days');
            var min = this.set_min;

            // If it's not a date value, let it through
            // are not concerned with NaNs, so let them pass
            if (isNaN(value)) {
                return true;
            }

            return min <= value && value <= max;
        };
    }

    /**
     *
     * opts.options - an array of options. If opts is not given, then a
     * list will be built automatically from the unique strings in the column
     * data.
     *
     * Hint: sLengthMenuAll is set in base.html and retrieves translation from Django locals
     */
    function SelectWidget(dTable, colIndex, opts) {
        opts = opts || {};
        var column = dTable.column(colIndex);
        var data = column.data();
        var optsArray = opts.options || data.unique();
        var select = $('<select></select>');
        select.append('<option value="">' + sLengthMenuAll + '</option>');

        // build the client list from column.data(), when serverSide = false
        if (!dTable.settings().init().serverSide) {
            $.each(optsArray.sort(), function(index, option) {
                select.append('<option>' + option + '</option>');
            });
        } else {
            // build the client list from ajax response
            var json = dTable.ajax.json();

            $.each(json['col_' + colIndex + '_selections'], function(
                index,
                option
            ) {
                if (option[0]) {
                    select.append(
                        '<option value="' +
                            option[1] +
                            '">' +
                            option[0] +
                            '</option>'
                    );
                } else {
                    select.append('<option>' + option + '</option>');
                }
            });
        }

        select.css('width', '90%');
        this.html = select;

        // Update the table when a selection is made
        select.change(function() {
            dTable
                .column(colIndex)
                .search(this.value)
                .draw();
            $(this).focus();
        });

        /* Filter out any rows which don't match the selection exactly, apart
         * from case.
         */
        this.filter = function(value) {
            var selection = select.val().toLowerCase();
            value = value.toLowerCase();
            if (selection === sLengthMenuAll.toLowerCase()) {
                return true;
            }
            return selection == value;
        };
    }

    /***
     * DataTable API Plugins
     */

    $.fn.dataTable.Api.register('max()', function() {
        /* NOTE: some JavaScript implementations limit the number of arguments
         * to something like 65,536 -- but if a table is larger than that, it
         * should probably be using server-side processing anyway */
        var numArray = this.filter(function(element) {
            return !isNaN(element);
        });
        return Math.max.apply(null, numArray);
    });

    $.fn.dataTable.Api.register('min()', function() {
        /* NOTE: some JavaScript implementations limit the number of arguments
         * to something like 65,536 -- but if a table is larger than that, it
         * should probably be using server-side processing anyway */
        var numArray = this.filter(function(element) {
            return !isNaN(element);
        });
        return Math.min.apply(null, numArray);
    });
})(jQuery, document, window);
