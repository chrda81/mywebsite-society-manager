/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 *
 * Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
 */

import 'node_modules/jquery';
import 'node_modules/select2';
import 'node_modules/bootstrap-daterangepicker';
import 'node_modules/ion-rangeslider';
import marked from 'node_modules/marked';
import Tagify from 'node_modules/@yaireo/tagify';
import moment from 'node_modules/moment';

import 'static/mywebsite/js/default/ajax-setup';
import 'society_manager_assets/js/datatables';

// Global vars
var idTicket_needs_to_send_mail_data;

// Global functions
function Init($parent) {
    //some docs: http://stackoverflow.com/questions/28576002/ajax-jquery-django (about: jdjangp +jquery + models +json)
    //we catch the values rendered by Django template
    var idTicket_data = $('#idTicket').val();
    var idTicket_fsm_state_data = $('#id_ticket-fsm_state').val();
    var idPercentage_data = $('#id_ticket-percentage').val();
    var count_microtask_data = $('#count_microtask').val();
    var path = window.location.pathname.split('/')[2];

    function update_percentage_input(percentage_value) {
        var raw_value = percentage_value;
        if ($('#id_ticket-percentage').val()) {
            $('#id_ticket-percentage').val(raw_value);
        } else {
            $('#ticketform').append(
                '<input type="hidden" id="id_ticket-percentage" name="ticket-percentage" value="' +
                    raw_value +
                    '">'
            );
        }
    }

    //Update the global percentage via ajax call
    function set_percentage(final_value) {
        $.ajax({
            type: 'POST',
            url: set_percentage_url,
            dataType: 'json',
            data: {
                range_value: final_value,
            },
            success: function(data) {
                //console.log("Post set_percentage: " + final_value);
                notif('info', sSuccess, data.message);
            },
        });
    }

    //Get the global percentage to use it as value for the slider
    function get_percentage(ticket_id) {
        $.ajax({
            type: 'GET',
            url: get_percentage_url,
            dataType: 'json',
            data: {
                ticket_id: ticket_id,
            },
            success: function(data) {
                //console.log(data);
                var dataparsed = data.percentage_data;
                //console.log(dataparsed);
                return dataparsed;
            },
        });
    }

    if ($('.range_base_ticket').length) {
        $('.range_base_ticket').ionRangeSlider({
            skin: 'flat',
            type: 'single',
            keyboard: true,
            //disable: true,
            min: 0,
            max: 100,
            step: 10,
            from: idPercentage_data,
            max_interval: 0,
            onFinish: function(data) {
                //Log final value for test purpouses
                var raw_value = data.from;
                //console.log("Value: " + raw_value);
                set_percentage(raw_value);
                update_percentage_input(data.from);
            },
        });

        var slider_for_existing = $('.range_base_ticket').data(
            'ionRangeSlider'
        );

        //Define if existing ticket load needs to disable the range slider because the existence
        //of microtasks
        if (typeof count_microtask_data !== 'undefined') {
            if (count_microtask_data != 0 || path == 'view') {
                slider_for_existing.update({
                    disable: true,
                });
                //console.log('IonSlider range_base_ticket disabled!');
            }

            //console.log(count_microtask_data);
        }
    }

    if ($('.range_base_ticket_for_submit').length) {
        $('.range_base_ticket_for_submit').ionRangeSlider({
            type: 'single',
            keyboard: 'true',
            min: 0,
            max: 100,
            step: 10,
            from: idPercentage_data,
            max_interval: 0,
            onFinish: function(data) {
                //Log final value for test purpouses
                // var raw_value = data.from;
                update_percentage_input(data.from);
                // //console.log("Value: " + raw_value);
                // if($('#id_ticket-percentage').val())
                // {
                //     $('#id_ticket-percentage').val(raw_value)
                // }
                // else
                // {
                //     $('#ticketform').append('<input type="hidden" id="id_ticket-percentage" name="ticket-percentage" value="'+raw_value+'">');
                // }
            },
        });

        var slider_for_new = $('.range_base_ticket_for_submit').data(
            'ionRangeSlider'
        );
    }

    //Create all divs with updated data
    function update_comments_new() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            // contentType: "application/x-www-form-urlencoded;charset=utf-8",
            url: get_comments_url,
            success: function(data) {
                var dataparsed = data;
                //console.log(data);
                var comments = $('.comment_box').empty();
                $.each(dataparsed, function(i, item) {
                    $('.comment_box').append(item);
                });
                // convert html tags
                $(comments)
                    .find('.content-markdown-old')
                    .each(function() {
                        $(this).html(
                            marked(
                                $(this)
                                    .text()
                                    .trim()
                            )
                        );
                    });
            },
        });
    }

    //Render mail template
    // use variable 'idTicket_can_send_mail_data' to prevent opening this modal dialog
    $('.bs-send-mail-modal-lg')
        .off('show.bs.modal')
        .on('show.bs.modal', function(e) {
            // override fsm_state with possible data-ticket-state from view.html
            if (e.hasOwnProperty('relatedTarget')) {
                if (
                    typeof $(e.relatedTarget).data('ticket-state') !==
                    'undefined'
                ) {
                    idTicket_fsm_state_data = $(e.relatedTarget).data(
                        'ticket-state'
                    );
                }
            }
            //console.log('Ticket-State: ' + idTicket_fsm_state_data);

            $.ajax({
                type: 'POST',
                url: render_mail_url,
                dataType: 'json',
                data: {
                    ticket_state: idTicket_fsm_state_data,
                    message_text: $('#message_data').val(),
                    message_template: $('#message_template').val(),
                },
                success: function(data) {
                    //console.log(data);
                    $('#to-input').val(data.to);
                    $('#cc-input').val('');
                    $('#bcc-input').val(data.bcc);
                    $('#subject-input').val(data.subject);
                    $('#body-plain-input').val(data.plain_content);
                    $('#body-html-input').val(data.html_content);
                    if (data.attachments) {
                        data.attachments.forEach(element => {
                            let file_fullname = element;
                            let file_shortname = element.split('/').pop();
                            let attachment_length =
                                $('div[class="form-row flex-item w-50 pl-1"]')
                                    .length + 1;

                            // check, if attachment was already attached to modal dialog section and set control var
                            let element_found = false;
                            $(
                                'div[class="form-row flex-item w-50 pl-1"] label'
                            ).each(function(index, item) {
                                //console.log(item.innerText.trim() + ' - ' + file_shortname);
                                if (item.innerText.trim() == file_shortname) {
                                    element_found = true;
                                }
                            });

                            // if not control var, add attachment
                            if (!element_found) {
                                $(
                                    '#attachment-collapse div[class="d-flex flex-row align-items-start flex-wrap"]'
                                ).append(
                                    '\
                            <div class="form-row flex-item w-50 pl-1" data-tag="template-attachment">\
                                <label><input\
                                        id="attachment_' +
                                        attachment_length +
                                        '"\
                                        type="checkbox"\
                                        style="margin-right: 5px;"\
                                        value="' +
                                        file_fullname +
                                        '"\
                                    >' +
                                        file_shortname +
                                        '\
                                </label>\
                            </div>\
                        '
                                );
                            }
                        });
                    } else {
                        // when no attachments are in data var, remove template-attachments from modal dialog
                        $('div[data-tag="template-attachment"]').remove();
                    }
                    $('.ignore-mail').prop(
                        'checked',
                        data.ignore_state == 'true' ? true : false
                    );
                    // add class to submit button that mail can be sent
                    if (
                        idTicket_fsm_state_data != 'view' &&
                        data.needs_to_send_mail
                    ) {
                        //console.log("send-mail");
                        $('.ignore-mail')
                            .parent('label')
                            .show();
                        $('.bs-send-mail-modal-lg')
                            .find('.message-to-mail')
                            .addClass('send-mail');
                    } else if (idTicket_fsm_state_data == 'view') {
                        //console.log("add-message");
                        $('.ignore-mail')
                            .parent('label')
                            .hide();
                        $('.bs-send-mail-modal-lg')
                            .find('.message-to-mail')
                            .addClass('add-message');
                    }
                },
                error: function(xhr, status, error) {
                    //$('#body-input').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    // server informs client to show modal dialog via variable, but only on Init via 'DOM'
    if (typeof idTicket_needs_to_send_mail_data !== 'undefined') {
        if (idTicket_needs_to_send_mail_data == 'True' && $parent == 'DOM') {
            //console.log('bs-send-mail-modal-lg triggered');
            $('.bs-send-mail-modal-lg').modal({ show: true });
        }
    }

    //Ignore modal mail dialog
    $('.ignore-mail')
        .off('click')
        .on('click', function(event) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    ticket_id: idTicket_data,
                    ignore_state: $(this).is(':checked'),
                },
                success: function(data) {
                    //console.log(data);
                    // if (!data.success) {
                    //     notif('error', sError, data.message);
                    // } else {
                    //     notif('info', sSuccess, data.message);
                    // }

                    //Set global vars
                    idTicket_needs_to_send_mail_data = data.ignore_state;
                    //console.log('Set send mail: ' + idTicket_needs_to_send_mail_data);
                },
                error: function(xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    //Post new message
    $('.add-message')
        .off('click')
        .on('click', function(event) {
            //console.log('Via .add-message called');
            event.preventDefault();

            var sendAsMail = false;
            var send_mail_url = '';
            if ($(this).hasClass('message-to-mail')) {
                sendAsMail = true;
                send_mail_url = $(this).attr('href');
                //console.log(send_mail_url);
            }
            //console.log(sendAsMail);

            // save comment
            $.ajax({
                type: 'POST',
                url: add_comment_url,
                dataType: 'json',
                data: {
                    message_text: $('#message_data').val(),
                    send_as_mail: sendAsMail,
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        notif('error', sError, data.message);
                    } else {
                        $('#message_data').val('');
                        notif('info', sSuccess, data.message);
                        update_comments_new();
                        $('#message_template')
                            .val('')
                            .trigger('change');
                    }
                },
                error: function(xhr, status, error) {
                    //$('#message_data').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });

            // send ticket state as mail
            if (sendAsMail) {
                // collect attachments
                let attachment_files = Array();
                $('input[id^="attachment_"]').each(function() {
                    if ($(this).prop('checked')) {
                        attachment_files.push($(this).val());
                    }
                });
                //console.log(attachment_files);

                $.ajax({
                    type: 'POST',
                    url: send_mail_url,
                    dataType: 'json',
                    data: {
                        message_text: $('.bs-send-mail-modal-lg')
                            .find('#body-html-input')
                            .val(),
                        mail_subject: $('.bs-send-mail-modal-lg')
                            .find('#subject-input')
                            .val(),
                        mail_to: $('.bs-send-mail-modal-lg')
                            .find('#to-input')
                            .val(),
                        mail_cc: $('.bs-send-mail-modal-lg')
                            .find('#cc-input')
                            .val(),
                        mail_bcc: $('.bs-send-mail-modal-lg')
                            .find('#bcc-input')
                            .val(),
                        attachments: JSON.stringify(attachment_files),
                    },
                    success: function(data) {
                        $('.bs-send-mail-modal-lg')
                            .find('#body-html-input')
                            .val('');
                        $('.bs-send-mail-modal-lg')
                            .find('#body-plain-input')
                            .val('');
                        //console.log(data);
                        // Close the modal
                        $('.bs-send-mail-modal-lg').modal('toggle');
                        notif('info', sSuccess, data.message);
                    },
                    error: function(xhr, status, error) {
                        //$('#message_data').val('');
                        var json = JSON.parse(xhr.responseText);
                        var error_message = json.message;
                        notif('error', sError, error_message);
                    },
                });
            }

            // Disable E-Mail and Comment buttons
            $('button.add-message').attr('disabled', '');
            $('button[data-target=".bs-send-mail-modal-lg"]').attr(
                'disabled',
                ''
            );
        });

    //Send ticket state as mail
    $('.send-mail')
        .off('click')
        .on('click', function(event) {
            //console.log('Via .send-mail called');
            event.preventDefault();

            // collect attachments
            let attachment_files = Array();
            $('input[id^="attachment_"]').each(function() {
                if ($(this).prop('checked')) {
                    attachment_files.push($(this).val());
                }
            });
            //console.log(attachment_files);

            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    message_text: $('.bs-send-mail-modal-lg')
                        .find('#body-html-input')
                        .val(),
                    mail_subject: $('.bs-send-mail-modal-lg')
                        .find('#subject-input')
                        .val(),
                    mail_to: $('.bs-send-mail-modal-lg')
                        .find('#to-input')
                        .val(),
                    mail_cc: $('.bs-send-mail-modal-lg')
                        .find('#cc-input')
                        .val(),
                    mail_bcc: $('.bs-send-mail-modal-lg')
                        .find('#bcc-input')
                        .val(),
                    attachments: JSON.stringify(attachment_files),
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        // Close the modal
                        $('.bs-send-mail-modal-lg').modal('toggle');
                        notif('error', sError, data.message);
                    } else {
                        $('.bs-send-mail-modal-lg')
                            .find('#body-html-input')
                            .val('');
                        $('.bs-send-mail-modal-lg')
                            .find('#body-plain-input')
                            .val('');
                        // Close the modal
                        $('.bs-send-mail-modal-lg').modal('toggle');
                        notif('info', sSuccess, data.message);
                    }
                },
                error: function(xhr, status, error) {
                    //$('#message_data').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    // Archive ticket
    $('.bs-archive-ticket-modal-lg button.request-ok')
        .off('click')
        .on('click', function(event) {
            //console.log('.request-ok called');
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    ticket_id: idTicket_data,
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        // Close the modal
                        $('.bs-archive-ticket-modal-lg').modal('toggle');
                        notif('error', sError, data.message);
                    } else {
                        // Close the modal
                        $('.bs-archive-ticket-modal-lg').modal('toggle');
                        notif('info', sSuccess, data.message);
                        if (data.redirect_url) {
                            // redirect to url
                            //console.log('Redirecting to ' + data.redirect_url);
                            setTimeout(function() {
                                window.location.href = data.redirect_url;
                            }, 1000);
                        }
                    }
                },
                error: function(xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    //Archive message
    //With ON class we can keep changes in new dynamically created objects!
    $('.comment_box .archive-message')
        .off('click')
        .on('click', function() {
            var idActualMessage = $(this)
                .closest('div#comment')
                .find("input[name='idPMessage']")
                .val();
            $.ajax({
                type: 'POST',
                url: archive_comment_url,
                dataType: 'json',
                data: {
                    message_id: idActualMessage,
                    ticket_id: idTicket_data,
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        notif('error', sError, data.message);
                    } else {
                        $('#message_data').val('');
                        notif('info', sSuccess, data.message);
                        update_comments_new();
                    }
                },
                error: function(xhr, status, error) {
                    //$('#message_data').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    //Request archiving of comment
    //With ON class we can keep changes in new dynamically created objects!
    var idRequestArchivingMessage;
    $('.bs-archive-comment-modal-lg')
        .off('show.bs.modal')
        .on('show.bs.modal', function(e) {
            idRequestArchivingMessage = $(e.relatedTarget).data('comment-id');
        });
    $('.bs-archive-comment-modal-lg button.request-ok')
        .off('click')
        .on('click', function() {
            var requestMessage = $('#request-input').val();
            //console.log(requestMessage);
            //console.log(idRequestArchivingMessage);
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    comment_id: idRequestArchivingMessage,
                    ticket_subject: 'Request archiving of comment',
                    ticket_body: requestMessage,
                },
                success: function(data) {
                    //console.log(data);
                    // Close the modal
                    $('.bs-archive-comment-modal-lg').modal('toggle');
                    $('#request-input').val('');
                    notif('info', sSuccess, data.message);
                },
                error: function(xhr, status, error) {
                    //$('#request-input').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    //Reset microtask modal
    $('#microtask_modal')
        .off('hidden.bs.modal')
        .on('hidden.bs.modal', function() {
            $('#mkModalLabel').html(gettext('Add new microtask'));
            //Clean some inserted data
            $('#subject_mk').val('');
            $('#body_mk').val('');
            //Select the firs option in select list
            $('#state_mk').val($('#state_mk option:first').val());
            //Reset the microtask slider state
            slider_new_microtask.update({
                from: 0,
            });
            //Remove the microtask id reference from modal
            $('.modal-footer')
                .find('[name="idmk"]')
                .remove();
        });

    //Update all microtask table
    function update_microtasks() {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: get_microtasks_url,
            success: function(data) {
                var dataparsed = data;
                //console.log(data);
                if (!data.success) {
                    notif('error', sError, data.message);
                } else {
                    $('#tblmicrotasks').empty();
                    $.each(dataparsed, function(i, item) {
                        $('#tblmicrotasks').append(item);
                    });
                }
            },
        });
    }

    //Microtask percentage
    var PercentageNewMK = 0;
    $('.range_new_mk').ionRangeSlider({
        skin: 'flat',
        type: 'single',
        keyboard: 'true',
        min: 0,
        max: 100,
        step: 10,
        from: 0,
        max_interval: 0,
        onFinish: function(data) {
            PercentageNewMK = data.from;
        },
        onUpdate: function(data) {
            PercentageNewMK = data.from;
        },
    });

    var slider_new_microtask = $('.range_new_mk').data('ionRangeSlider');

    //Post new microtask
    $('.add-microtask')
        .off('click')
        .on('click', function() {
            //console.log('am i called');
            //Try if ID exists and instance the variable
            //var ActualMK = $(this).closest("#microtask_modal").find("input[name='idmk']").val();
            var ActualMK = $(this)
                .closest('.modal-footer')
                .find("input[name='idmk']")
                .val();
            var add_mk_url = $(this).attr('data-url');
            $.ajax({
                type: 'POST',
                url: add_mk_url,
                dataType: 'json',
                data: {
                    id_mk: ActualMK,
                    subject_text: $('#subject_mk').val(),
                    body_text: $('#body_mk').val(),
                    state_id: $('#state_mk').val(),
                    percentage_num: PercentageNewMK,
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        notif('error', sError, data.message);
                    } else {
                        notif('info', sSuccess, data.message);
                        //Close the modal
                        $('#microtask_modal').modal('toggle');
                        //Get the new global average
                        //console.log(get_percentage(idTicket_data))
                        var new_percentage = get_percentage(idTicket_data);
                        //console.log(new_percentage);
                        //Disable the slider if its the first microtask and update the percentage
                        slider_for_existing.update({
                            disable: true,
                            from: new_percentage,
                        });
                        //Update the main input value for "percentage" to avoid 0% percentage
                        update_percentage_input(new_percentage);
                        //Update all microtask from table
                        update_microtasks();
                    }
                },
                error: function(xhr, status, error) {
                    //$("#message_data").val("");
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    //Edit microtasks modal
    function edit_microtask(mk_id, edit_mk_url) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: edit_mk_url,
            success: function(data) {
                //console.log(data);
                $('#microtask_modal')
                    .find('[name="subject_mk"]')
                    .val(data.subject_data);
                $('#microtask_modal')
                    .find('[name="body_mk"]')
                    .val(data.body_data);
                $('#microtask_modal')
                    .find('[name="state_mk"]')
                    .val(data.state_data_id);
                slider_new_microtask.update({
                    from: data.percentage_data,
                });
                $('.modal-footer').append(
                    '<input type="hidden" id="idmk" name="idmk" value="' +
                        mk_id +
                        '">'
                );
                $('#mkModalLabel').html(gettext('Edit microtask'));
                $('#microtask_modal').modal('show');
            },
        });
    }

    $('#tblmicrotasks .edit-mk')
        .off('click')
        .on('click', function() {
            //console.log('am i called');
            var idActualMK = $(this)
                .closest('td#buttons')
                .find("input[name='idmk']")
                .val();
            var edit_mk_url = $(this).attr('data-url');
            edit_microtask(idActualMK, edit_mk_url);
        });

    //Archive microtask by ID
    $('#tblmicrotasks .archive-mk')
        .off('click')
        .on('click', function() {
            //var idActualMessage = $(".archive-message").closest("#idPMessage").attr("value");
            var ActualMK = $(this)
                .closest('td#buttons')
                .find("input[name='idmk']")
                .val();
            var archive_mk_url = $(this).attr('data-url');
            $.ajax({
                type: 'POST',
                url: archive_mk_url,
                dataType: 'json',
                data: {
                    mk_id: ActualMK,
                    ticket_id: idTicket_data,
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        notif('error', sError, data.message);
                    } else {
                        var new_percentage = get_percentage(idTicket_data);
                        //console.log(percentage);
                        //Disable the slider if its the firs microtask and update the percentage
                        slider_for_existing.update({
                            disable: true,
                            from: new_percentage,
                        });
                        update_microtasks();
                        var rowCount = $('#tblmicrotasks tr').length - 1;
                        //console.log(rowCount);
                        if (rowCount == 0) {
                            slider_for_existing.update({
                                disable: false,
                            });
                        } else {
                            slider_for_existing.update({
                                disable: true,
                            });
                        }
                        notif('info', sSuccess, data.message);
                    }
                },
                error: function(xhr, status, error) {
                    //$("#message_data").val("");
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    // Post new company
    // Prevent multiple event binding: https://www.gajotres.net/prevent-jquery-multiple-event-triggering/
    $('#add-company')
        .off('click')
        .on('click', function(event) {
            // event.preventDefault()

            var $form = $('#ajax-form-company');
            var $formData = $form.serialize();
            //console.log('FormData: ' + $formData);
            var $thisURL = $form.attr('action');
            $.ajax({
                method: 'POST',
                url: $thisURL,
                data: $formData,
                success: function(data) {
                    if (!data.success) {
                        $('.messages').replaceWith(
                            '<div class="messages"><span> </span></div>'
                        );
                        $('.messages span').append(
                            '<div class="form_errors">' +
                                data.message +
                                '</div>'
                        );
                        //console.log(data)
                        $form.replaceWith(data.html_form);
                    } else {
                        //console.log(data);
                        // reset form data and remove field errors
                        $('.messages').replaceWith(
                            '<div class="messages"><span> </span></div>'
                        );
                        $(':input', '#ajax-form-company')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .prop('checked', false)
                            .prop('selected', false);
                        // Bootstrap 4
                        $form
                            .find('input')
                            .filter('.form-control')
                            .val('');
                        $form.find('input').removeClass('is-invalid');
                        $form
                            .find('textarea')
                            .filter('.form-control')
                            .val('');
                        $form.find('textarea').removeClass('is-invalid');
                        $form.find('.invalid-feedback').remove();
                        notif('info', sSuccess, data.message);
                        // Close the modal
                        $('#company_modal').modal('toggle');
                        // Add new company to listbox #id_ticket-assigned_company and select it
                        $('#id_ticket-assigned_company').append(
                            "<option value='" +
                                data.company_id +
                                "' selected>" +
                                data.company_name +
                                '</option>'
                        );
                        $('#id_ticket-assigned_company').trigger('change');
                    }
                },
                error: function(xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    //console.log(error_message);
                    notif('error', sError, error_message);
                },
            });
        });
}

function markdownfunction() {
    $('.content-markdown-old').each(function() {
        $(this).html(
            marked(
                $(this)
                    .text()
                    .trim()
            )
        );
    });
    $('.content-markdown-old.homy h1').css('font-size', '18px');
    $('.content-markdown-new').each(function() {
        $(this).html(marked($(this).text()));
    });
}

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Date picker creation date
    if ($('#id_ticket-date').length) {
        $('#id_ticket-date').daterangepicker({
            showISOWeekNumbers: true,
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            showCustomRangeLabel: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            todayHighlight: true,
            autoApply: true,
            locale: $dp_locale,
            ranges: {
                sNow: [
                    moment().format($dateformat),
                    moment().format($dateformat),
                ],
                sClear: [
                    moment('19700101').format($dateformat),
                    moment('19700101').format($dateformat),
                ],
            },
        });

        // Locale for range keys
        $('.ranges li[data-range-key="sNow"]').text(sNow);
        $('.ranges li[data-range-key="sClear"]').text(sClear);

        $('#id_ticket-date')
            .data('daterangepicker')
            .setStartDate(moment().format($dateformat));
    }

    // Date picker pending date
    if ($('#id_ticket-pending').length) {
        $('#id_ticket-pending').daterangepicker({
            showISOWeekNumbers: true,
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            showCustomRangeLabel: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            todayHighlight: true,
            autoApply: true,
            locale: $dp_locale,
            ranges: {
                sNow: [
                    moment().format($dateformat),
                    moment().format($dateformat),
                ],
                sClear: [
                    moment('19700101').format($dateformat),
                    moment('19700101').format($dateformat),
                ],
            },
        });

        // Locale for range keys
        $('.ranges li[data-range-key="sNow"]').text(sNow);
        $('.ranges li[data-range-key="sClear"]').text(sClear);

        // initialized pending date for input
        swapDisableOfPending();

        // event listener on combobox for pending state
        $('#id_ticket-assigned_state').on('change', function() {
            swapDisableOfPending();
        });
    }

    function swapDisableOfPending() {
        if ($('#id_ticket-assigned_state :selected').text() == sPending) {
            $('#id_ticket-pending').prop('disabled', false);
        } else {
            $('#id_ticket-pending').prop('disabled', true);
        }
    }

    // Init datatables
    var $dt_columns_ticket = [
        { id: 2, text: 'ID' },
        { id: 3, text: 'Subject' },
        { id: 4, text: 'Company Name' },
        { id: 5, text: 'State' },
        { id: 6, text: 'Queue' },
        { id: 7, text: 'User' },
        { id: 8, text: 'Created' },
        { id: 9, text: 'Updated' },
        { id: 10, text: 'Pending' },
        { id: 11, text: 'Assigned User' },
        { id: 12, text: 'Percentage' },
        { id: 13, text: 'Priority' },
    ];
    window.$dt_columns_ticket = $dt_columns_ticket;
    var table = $('#dataTables-tickets').DataTable({
        processing: true,
        ajax: {
            url: app_url_prefix + '/tickets/data/',
            type: 'POST',
            data: {
                url_name: typeof $url_name !== 'undefined' ? $url_name : '',
                requested_label:
                    typeof $requested_label !== 'undefined'
                        ? $requested_label
                        : '',
                requested_state_id:
                    typeof $requested_state_id !== 'undefined'
                        ? $requested_state_id
                        : '',
                requested_queue_id:
                    typeof $requested_queue_id !== 'undefined'
                        ? $requested_queue_id
                        : '',
                requested_subject:
                    typeof $requested_subject !== 'undefined'
                        ? $requested_subject
                        : '',
                requested_body:
                    typeof $requested_body !== 'undefined'
                        ? $requested_body
                        : '',
                requested_comment:
                    typeof $requested_comment !== 'undefined'
                        ? $requested_comment
                        : '',
                requested_assigned_company:
                    typeof $requested_assigned_company !== 'undefined'
                        ? $requested_assigned_company
                        : '',
                requested_assigned_state:
                    typeof $requested_assigned_state !== 'undefined'
                        ? $requested_assigned_state
                        : '',
                requested_assigned_user:
                    typeof $requested_assigned_user !== 'undefined'
                        ? $requested_assigned_user
                        : '',
                requested_create_user:
                    typeof $requested_create_user !== 'undefined'
                        ? $requested_create_user
                        : '',
                requested_active_member:
                    typeof $requested_active_member !== 'undefined'
                        ? $requested_active_member
                        : '',
            },
        },
        deferRender: true,
        serverSide: true,
        lengthMenu: [
            [25, 50, 100, -1],
            [25, 50, 100, sLengthMenuAll],
        ],
        responsive: {
            details: {
                type: 'column',
                target: 1,
            },
        },
        dom: '<lfip<rt>ip><"clearfix">B',
        sortCellsTop: true,
        select: {
            style: 'multi',
            selector: 'td:first-child',
            blurable: true,
        },
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(:first-child, :nth-child(2))',
                postfixButtons: [
                    {
                        text: sResetSavedState,
                        action: function(e, dt, node, config) {
                            if (localStorage) {
                                table.state.clear();
                                window.location.reload();
                            }
                        },
                    },
                ],
            },
            {
                extend: 'colexp',
                columns: ':not(:first-child, :nth-child(2))',
            },
            {
                extend: 'collection',
                text: 'Export',
                autoclose: true,
                enabled: false,
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                        customize: function(win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .css('background-color', '#fff')
                                .prepend(
                                    '<img src="/static/mywebsite/img/logo.png" style="position:absolute; top:10px; left:700px; width: 110px; height: 45px;" />'
                                );

                            $(win.document.body)
                                .find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                    },
                ],
            },
        ],
        columnDefs: [
            {
                // 0: checkbox
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                width: 12,
                name: 'select-checkbox',
                data: null,
                defaultContent: '',
            },
            {
                // 1: responsive control
                targets: 1,
                orderable: false,
                className: 'control',
                name: 'control',
                data: null,
                defaultContent: '',
            },
            {
                // 2: ID
                targets: 2,
                className: 'marked-for-export',
                data: 2,
                name: 'nr',
                render: function(data, type, full, meta) {
                    return (
                        '<a href="' +
                        app_url_prefix +
                        '/tickets/view/' +
                        full[0] +
                        '">' +
                        full[2] +
                        '</a>'
                    );
                },
            },
            {
                // 3: Subject
                targets: 3,
                className: 'marked-for-export',
                data: 3,
                name: 'subject',
            },
            {
                // 4: Company name
                targets: 4,
                className: 'marked-for-export',
                data: 4,
                name: 'assigned_company.name',
            },
            {
                // 5: State
                targets: 5,
                className: 'marked-for-export',
                data: 5,
                name: 'assigned_state.name',
            },
            {
                // 6: Queue
                targets: 6,
                visible: false,
                className: 'marked-for-export',
                data: 6,
                name: 'assigned_queue.name',
            },
            {
                // 7: User
                targets: 7,
                visible: false,
                className: 'marked-for-export',
                data: 7,
                name: 'str_create_user',
            },
            {
                // 8: Created
                targets: 8,
                visible: false,
                className: 'marked-for-export',
                data: 8,
                name: 'created',
                render: function(data, type, full, meta) {
                    return moment(data, 'DD.MM.YYYY HH:mm').format($dateformat);
                },
            },
            {
                // 9: Updated
                targets: 9,
                className: 'marked-for-export',
                data: 9,
                name: 'updated',
                render: function(data, type, full, meta) {
                    return moment(data, 'DD.MM.YYYY HH:mm').format($dateformat);
                },
            },
            {
                // 10: Pending
                targets: 10,
                className: 'marked-for-export',
                data: 10,
                name: 'pending',
                render: function(data, type, full, meta) {
                    return moment(data, 'DD.MM.YYYY HH:mm').format($dateformat);
                },
            },
            {
                // 11: Assigned User
                targets: 11,
                className: 'marked-for-export',
                data: 11,
                name: 'str_assigned_user',
            },
            {
                // 12: Percentage
                targets: 12,
                visible: false,
                className: 'marked-for-export',
                data: 12,
                name: 'percentage',
            },
            {
                // 13: Priority
                targets: 13,
                visible: false,
                className: 'marked-for-export',
                data: 13,
                name: 'assigned_prio.name',
            },
            {
                // 14: Member Active
                targets: 14,
                className: 'marked-for-export',
                data: 14,
                name: 'assigned_company.members.is_active',
                render: function(data, type, row) {
                    return data === true
                        ? '<i class="fa fa-check"></i>'
                        : '<i class="fa fa-minus"></i>';
                },
            },
        ],
        columnFilters: [
            {
                // 0: checkbox
                type: 'None',
            },
            {
                // 1: responsive control
                type: 'None',
            },
            {
                // 2: ID
                type: 'Text',
            },
            {
                // 3: Subject
                type: 'Text',
            },
            {
                // 4: Company name
                type: 'Text',
            },
            {
                // 5: State
                type: 'Select',
            },
            {
                // 6: Queue
                type: 'Select',
            },
            {
                // 7: User
                type: 'Select',
            },
            {
                // 8: Created
                type: 'Date',
            },
            {
                // 9: Updated
                type: 'Date',
            },
            {
                // 10: Pending
                type: 'Date',
            },
            {
                // 11: Assigned User
                type: 'Select',
            },
            {
                // 12: Percentage
                type: 'Range',
            },
            {
                // 13: Priority
                type: 'Select',
            },
            {
                // 14: Member Active
                type: 'Select',
            },
        ],
        order: [[8, 'desc']], // order by column 'Created'
        ordering: true,
        language: $dt_language,
        createdRow: function(row, data, dataIndex) {
            // change color on row, if pending date is lt now
            if (
                // uses columns 5: State and 10: Pending!
                moment(data[10], $dateformat).isSameOrBefore(moment()) &&
                data[5].includes(sPending)
            ) {
                $(row).css('color', '#e55c3c');
                $(row).addClass('warning');
            }
        },
    });

    table.on('select deselect', function() {
        var selectedRows = table
            .rows({
                selected: true,
            })
            .count();

        table.button(2).enable(selectedRows > 0); // export button
    });

    // select checkbox: de/select all 'filtered' rows
    table.on('click', 'th.select-checkbox', function() {
        if ($('th.select-checkbox').hasClass('selected')) {
            table.rows({ filter: 'applied' }).every(function() {
                this.deselect();
            });
            $('th.select-checkbox').removeClass('selected');
        } else {
            table.rows({ filter: 'applied' }).every(function() {
                this.select();
            });
            $('th.select-checkbox').addClass('selected');
        }
    });

    // never save filter values for state save
    table.on('stateSaveParams.dt', function(e, settings, data) {
        data.columns.forEach(column => {
            column.search.search = '';
        });
    });

    // Tagify
    let labels_input = document.querySelector('input[name=ticket-labels]'),
        tagify,
        controller; // for aborting the call

    // event binding
    if (labels_input != null) {
        tagify = new Tagify(labels_input, {
            autocomplete: true,
            whitelist: [],
            dropdown: {
                enabled: 2,
                maxItems: 5,
            },
        });

        tagify.on('input', onTagInput);
        tagify.on('add', onTagAdded);
    }

    function onTagInput(e) {
        let value = e.detail;

        // https://developer.mozilla.org/en-US/docs/Web/API/AbortController/abort
        controller && controller.abort();
        controller = new AbortController();

        fetch(app_url_prefix + '/labels/get-list-flat', {
            signal: controller.signal,
        })
            .then(RES => RES.json())
            .then(function(data) {
                tagify.settings.whitelist = data;
                tagify.dropdown.show.call(tagify, value); // render the suggestions dropdown
            });

        //console.log(tagify.settings.whitelist);
    }

    function onTagAdded(e) {
        //console.log('added', e.detail.data)
        tagify.settings.whitelist.length = 0; // reset the whitelist
    }

    // create select2 combobox for companies
    $('#id_ticket-assigned_company')
        .select2({
            width: '100%',
            maximumSelectionLength: 25,
            placeholder: '---------',
            allowClear: true,
            selectOnClose: true,
            templateResult: formatCompanyResult,
        })
        .on('select2:unselecting', function(ev) {
            // Bugfix for open dropbox again, after allowClear!
            if (ev.params.args.originalEvent) {
                ev.params.args.originalEvent.stopPropagation();
            } else {
                $('select').each(function(index) {
                    if (
                        $(this).hasClass('select2-hidden-accessible') &&
                        $(this)
                            .data('select2')
                            .isOpen()
                    ) {
                        $(this).select2('close');
                    }
                });
                $(this).one('select2:opening', function(ev) {
                    ev.preventDefault();
                });
            }
        });

    $('#ticketform').on('submit', function(e) {
        // add value of disabled field pending to data, otherwise Django form validate will fail!
        let $pending = $('#id_ticket-pending').clone(true); // use deep copy of field, to remove disabled attribute
        $(this).append($pending.removeAttr('disabled').css('display', 'none'));

        // add value of disabled field assigned_queue to data
        let $queue = $('#id_ticket-assigned_queue').clone(true);
        if ($queue.attr('disabled')) {
            $(this).append(
                $queue.removeAttr('disabled').css('display', 'none')
            );
        }

        return true;
    });

    // disable OK-Button on modal dialog
    //Init
    if ($('.bs-archive-comment-modal-lg #request-input').val() == '') {
        $('.bs-archive-comment-modal-lg button.request-ok').attr(
            'disabled',
            ''
        );
    }
    //OnInput
    $('.bs-archive-comment-modal-lg #request-input').on('input', function() {
        if ($('.bs-archive-comment-modal-lg #request-input').val() == '') {
            $('.bs-archive-comment-modal-lg button.request-ok').attr(
                'disabled',
                ''
            );
        } else {
            $('.bs-archive-comment-modal-lg button.request-ok').removeAttr(
                'disabled'
            );
        }
    });

    // set default value for combobox 'message_template'
    //OnChange
    $('#message_template').on('change', function() {
        if (
            $('#message_template option:selected').text() != sStandardTemplate
        ) {
            let message =
                sTemplateText +
                ' (' +
                $('#message_template option:selected').text() +
                ').';

            if ($('#message_data').length) {
                if (
                    $('#message_data').val() == '' ||
                    $('#message_data')
                        .val()
                        .includes(sTemplateText)
                ) {
                    $('#message_data')
                        .val(message)
                        .trigger('change');
                }
            }
        } else {
            if ($('#message_data').length) {
                if (
                    $('#message_data')
                        .val()
                        .includes(sTemplateText)
                ) {
                    $('#message_data')
                        .val('')
                        .trigger('change');
                }
            }
        }
    });
    //Init after binding to combobox event, so that onchange is fired to clear message_data and disable buttons
    $('#message_template')
        .val('')
        .trigger('change');

    // disable E-Mail and Comment-Buttons on Ticket-View
    //Init
    if ($('#message_data').length) {
        if ($('#message_data').val() == '') {
            $('button.add-message').attr('disabled', '');
            $('button[data-target=".bs-send-mail-modal-lg"]').attr(
                'disabled',
                ''
            );
        } else if (
            $('#message_data')
                .val()
                .includes(sTemplateText)
        ) {
            $('#message_data').val('');
        }
    }
    //OnInput
    if ($('#message_data').length) {
        $('#message_data').on('input propertychange change', function() {
            if ($('#message_data').val() == '') {
                $('button.add-message').attr('disabled', '');
                $('button[data-target=".bs-send-mail-modal-lg"]').attr(
                    'disabled',
                    ''
                );
            } else {
                $('button.add-message').removeAttr('disabled');

                let idTicket_can_send_mail_data = $(
                    '#id_ticket-can_send_mail'
                ).val();
                if (idTicket_can_send_mail_data == 'True') {
                    $(
                        'button[data-target=".bs-send-mail-modal-lg"]'
                    ).removeAttr('disabled');
                }
            }
        });
    }

    // file attachments
    var docs = 2;
    $('.add_document').click(function(e) {
        e.preventDefault();

        //console.log(docs);

        if (docs <= 10) {
            $('.add_form').append(
                '<div class="row"><div class="col-md-10"><input type="file" id="id_attach-file_name_' +
                    docs +
                    '" name="attach-file_name_' +
                    docs +
                    '" class="btn btn-sm text-left"></div><a href="#" class="remove_field"><i class="fa fa-times"></i></a><br /><br /></div>'
            );
            docs++;
        } else if (docs == 11) {
            $('.add_form').append(
                '<p>You can only add 10 documents at a time</p>'
            );
            docs++;
        }
    });

    $('.add_form').on('click', '.remove_field', function(e) {
        e.preventDefault();
        $(this)
            .parent('div')
            .remove();
        docs--;
    });

    //Init global var only once after document is loaded, not on AJAX complete
    idTicket_needs_to_send_mail_data = $('#id_ticket-needs_to_send_mail').val();
    Init('DOM');
    markdownfunction();
    //console.log('DOM loaded');
});

// jQuery DOM manipulation after each AJAX call has completed
$(document).ajaxComplete(function() {
    Init('AJAX');
    //console.log('AJAX complete');
});

// export functions for Django templates
window.previewMarkdown = function() {
    $('#id_ticket-body_preview').html(marked($('#id_ticket-body').val()));
};

// template for select2 combobox
function formatCompanyResult(item) {
    if (!item.id) {
        return item.text;
    }

    var $company;
    if ($members.includes(item.text)) {
        $company = $(
            '<span>' +
                item.text +
                '</span> <span style="color: #008ac6;">' +
                $members_text +
                '</span>'
        );
    } else {
        $company = $('<span>' + item.text + '</span>');
    }

    return $company;
}
