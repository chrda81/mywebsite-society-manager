/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import Tagify from 'node_modules/@yaireo/tagify';

import '../../static/mywebsite/js/appAdmin.js';

// jQuery DOM manipulation
$(document).ready(function() {
    let labels_input = document.querySelector('input[id=id_labels]'),
        tagify,
        controller; // for aborting the call

    // event binding
    if (labels_input != null) {
        tagify = new Tagify(labels_input, {
            autocomplete: true,
            whitelist: [],
            dropdown: {
                enabled: 2,
                maxItems: 5,
            },
        });

        tagify.on('input', onTagInput);
        tagify.on('add', onTagAdded);
    }

    function onTagInput(e) {
        let value = e.detail;

        // https://developer.mozilla.org/en-US/docs/Web/API/AbortController/abort
        controller && controller.abort();
        controller = new AbortController();

        // variable get_labels_list_url is injected from base_site.html
        fetch(get_labels_list_url, {
            signal: controller.signal,
        })
            .then(RES => RES.json())
            .then(function(data) {
                tagify.settings.whitelist = data;
                tagify.dropdown.show.call(tagify, value); // render the suggestions dropdown
            });

        // console.log(tagify.settings.whitelist);
    }

    function onTagAdded(e) {
        // console.log('added', e.detail.data)
        tagify.settings.whitelist.length = 0; // reset the whitelist
    }
});

// Hot module replacement for webpack
if (module.hot) {
    module.hot.dispose(() => {
        // some code for disposal goes here
    });
    module.hot.accept();
}
