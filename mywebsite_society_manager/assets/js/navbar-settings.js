/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 *
 * Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
 */

//Store settings for mantain collapsed the side-navbar
$(document).ready(function() {
    var tckstorage = localStorage;

    try {
        var userid_data = document.getElementById('idUser').value;
    } catch (e) {}

    //console.log(userid_data)

    function set_navbar() {
        $.ajax({
            type: 'POST',
            url: navbar_url,
            dataType: 'json',
            data: {
                submited_user_id: userid_data,
            },
            success: function(data) {
                //console.log(data);
                //notif('info', sSuccess, data.message);
            },
            error: function(xhr, status, error) {
                var json = JSON.parse(xhr.responseText);
                var error_message = json.message;
                notif('error', sError, error_message);
            },
        });
    }

    $('.nav').on('click', '#menu_toggle', function() {
        set_navbar();
        //console.log(tckstorage);
    });
});
