/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import $ from 'node_modules/jquery';
window.jQuery = $;
window.$ = $;

import 'node_modules/jquery.nicescroll';
import 'node_modules/nprogress';
import 'node_modules/bootstrap';
import 'node_modules/bootstrap-progressbar/bootstrap-progressbar';
import 'node_modules/fastclick';

import PNotify from 'node_modules/pnotify';
window.PNotify = PNotify;
import 'node_modules/pnotify/dist/pnotify.buttons';
import 'node_modules/pnotify/dist/pnotify.nonblock';
import 'node_modules/smartresize.js';
import moment from 'node_modules/moment';

// import i18n functions from Django. Read url, e.g. 'const django_i18n_url' from base.html
// import get_django_i18n_script from './localization';
// eval(get_django_i18n_script(django_i18n_url));

import 'society_manager_assets/js/navbar-settings';
import 'society_manager_assets/js/custom';
import 'society_manager_assets/js/tickets';
import 'society_manager_assets/js/members';
import 'society_manager_assets/js/prospects';
import 'society_manager_assets/js/settings';

// Global functions

//PNotify for events (add messages, range change, etc)
function notif(type, title, text) {
    new PNotify({
        title: '' + title + '',
        text: '' + text + '',
        type: '' + type + '',
        styling: 'fontawesome',
        nonblock: true,
        buttons: {
            sticker: false,
        },
        hide: true,
        delay: 2000,
    });
}
window.notif = notif;

// // Extension for notifications/notify.js
// function markAsRead(slug) {
//     $.ajax({
//         url: site_url_prefix + '/notifications/mark-as-read/' + slug,
//         type: 'POST',
//         data: {},
//         success: function(msg) {
//             setTimeout(fetch_api_data, 500);
//         },
//         error: function(jqXHR, textStatus) {
//             alert('Error Occured'); //MESSAGE
//         },
//     });
// }
// window.markAsRead = markAsRead;

function my_notification_list(data) {
    var menus = document.getElementsByClassName(notify_menu_class);
    if (menus) {
        var messages = data.unread_list
            .map(function(item, idx) {
                var message = '';
                if (typeof item.actor !== 'undefined') {
                    message = item.actor;
                }
                if (typeof item.verb !== 'undefined') {
                    message = message + ': ' + item.verb;
                }
                if (typeof item.target !== 'undefined') {
                    message = message + ' ' + item.target;
                }
                if (typeof item.timestamp !== 'undefined') {
                    message =
                        message +
                        ' - ' +
                        moment(item.timestamp).format($dateformat);
                }
                var bgcolor = '#ededed';
                if (idx % 2 === 0) {
                    bgcolor = '#fff';
                }
                return (
                    // '<li><a href="#" style="background: ' + bgcolor + ';" onclick="markAsRead(' +
                    '<li><a href="/inbox/notifications/mark-as-read/' +
                    item.slug +
                    '/?next=' +
                    app_url_prefix +
                    '/tickets/view/' +
                    item.actor_object_id +
                    '" style="background: ' +
                    bgcolor +
                    '">' +
                    message +
                    '<span style="margin-left: 5px" class="fa fa-times"></span></a></li>'
                );
            })
            .join('');

        for (var i = 0; i < menus.length; i++) {
            menus[i].innerHTML = messages;
        }
    }
}
window.my_notification_list = my_notification_list;

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    //convert Django messages to PNotify events
    if (typeof notify_messages == 'object') {
        //console.log(typeof(notify_messages));
        notify_messages.forEach(function(element) {
            if (element.tag == 'error') {
                notif('error', sError, element.message);
            } else {
                notif('info', sSuccess, element.message);
            }
        });
    }
});

// Hot module replacement for webpack
if (module.hot) {
    module.hot.dispose(() => {
        // some code for disposal goes here
    });
    module.hot.accept();
}
