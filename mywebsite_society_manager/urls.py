# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2019 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.contrib.auth import views as auth_views
from django.urls import path, re_path
from mywebsite_members import views as members_views

from .views import get_labels_list_flat_jx
from .views import group as vgroup
from .views import index
from .views import masterdata as vmasterdata
from .views import members as vmembers
from .views import priority as vpriorities
from .views import prospects as vprospects
from .views import queues as vqueues
from .views import right as vright
from .views import save_appsettings
from .views import search as vsearch
from .views import settings
from .views import states as vstates
from .views import tickets as vtickets
from .views import users as vusers
from .views import view_appsettings

app_name = 'society_manager'


urlpatterns = [
    # Dashboard, main screen spotted
    path('', index, name='index'),
    path('labels/get-list-flat', get_labels_list_flat_jx, name='get-labels-list-flat'),

    # Auth
    path('login/', members_views.LoginView.as_view(template_name='society_manager/auth/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='society_manager/auth/logout.html'), name='logout'),

    # Users
    path('settings/user/', vusers.list_users, name='user-list'),
    path('settings/user/create/', vusers.manage_user, name='user-create'),
    path('settings/user/<user_id>', vusers.manage_user, name='user-edit'),
    path('settings/user/delete/<user_id>', vusers.delete_user, name='user-delete'),
    path('settings/user/set_togglenavbar/', vusers.set_collapsednavbar_jx, name='user-set-collapsednavbar'),

    # Groups
    path('settings/groups/', vgroup.list_groups, name='group-list'),
    path('settings/groups/create/', vgroup.manage_group, name='group-create'),
    path('settings/groups/<group_id>', vgroup.manage_group, name='group-edit'),
    path('settings/groups/delete/<group_id>', vgroup.delete_group, name='group-delete'),

    # Rights
    path('settings/rights/', vright.list_rights, name='right-list'),
    path('settings/rights/create/', vright.manage_right, name='right-create'),
    path('settings/rights/<right_id>', vright.manage_right, name='right-edit'),
    path('settings/rights/archive/<right_id>', vright.archive_right, name='right-archive'),

    # States
    path('settings/state/', vstates.list_state, name='state-list'),
    path('settings/state/create/', vstates.manage_state, name='state-create'),
    path('settings/state/<state_id>', vstates.manage_state, name='state-edit'),
    path('settings/state/archive/<state_id>', vstates.archive_state, name='state-archive'),

    # Priorities
    path('settings/priorities/', vpriorities.list_priorities, name='priority-list'),
    path('settings/priorities/create/', vpriorities.manage_priority, name='priority-create'),
    path('settings/priorities/<priority_id>', vpriorities.manage_priority, name='priority-edit'),
    path('settings/priorities/archive/<priority_id>', vpriorities.archive_priority, name='priority-archive'),

    # Queues
    path('settings/queue/', vqueues.list_queues, name='queue-list'),
    path('settings/queue/create/', vqueues.manage_queue, name='queue-create'),
    path('settings/queue/<queue_id>', vqueues.manage_queue, name='queue-edit'),
    path('settings/queue/archive/<queue_id>', vqueues.archive_queue, name='queue-archive'),

    # Tickets
    path('tickets/', vtickets.list_tickets, name='tickets-list'),
    path('tickets/data/', vtickets.TicketListJson.as_view(), name='tickets-list-json'),
    path('tickets/<ticket_id>', vtickets.list_tickets, name='tickets-list'),
    path('tickets/state/<state_id>', vtickets.list_tickets_state,  name='tickets-list-state'),
    path('tickets/queue/<queue_id>', vtickets.list_tickets_queue, name='tickets-list-queue'),
    path('tickets/labels/<label>', vtickets.list_tickets_label, name='tickets-list-label'),
    path('tickets/create/', vtickets.manage_ticket, name='tickets-create'),
    path('tickets/create/auto/', vtickets.create_ticket_jx, name='tickets-auto-create'),
    path('tickets/edit/<ticket_id>', vtickets.manage_ticket, name='tickets-edit'),
    path('tickets/view/<ticket_id>', vtickets.view_ticket, name='tickets-view'),
    path('tickets/archive/<ticket_id>', vtickets.archive_ticket_jx, name='tickets-archive'),
    path('tickets/render_mail/<ticket_id>', vtickets.render_mail_jx, name='tickets-render-mail'),
    path('tickets/send_mail/<ticket_id>', vtickets.send_mail_jx, name='tickets-send-mail'),
    path('tickets/ignore_mail/<ticket_id>', vtickets.ignore_mail_jx, name='tickets-ignore-mail'),

    # Prospects
    path('prospects/', vprospects.list_prospects, name='prospects-list'),
    path('prospects/data/', vprospects.ProspectiveCompanyListJson.as_view(), name='prospects-list-json'),
    path('prospects/edit/<prospect_id>', vprospects.manage_prospect, name='prospects-edit'),
    path('prospects/view/<prospect_id>', vprospects.view_prospect, name='prospects-view'),
    path('prospects/archive/<prospect_id>', vprospects.archive_prospect, name='prospects-archive'),
    path('prospects/export/<prospect_id>', vprospects.export_prospect, name='prospects-export'),

    # Members
    path('members/', vmembers.list_members, name='members-list'),
    path('members/data/', vmembers.SocietyMemberListJson.as_view(), name='members-list-json'),
    path('members/create/<prospect_id>', vmembers.manage_member, name='members-create'),
    path('members/edit/<member_id>', vmembers.manage_member, name='members-edit'),
    path('members/view/<member_id>', vmembers.view_member, name='members-view'),
    path('members/archive/<member_id>', vmembers.archive_member, name='members-archive'),
    path('members/export/<member_id>', vmembers.export_member, name='members-export'),
    path('members/services/manage/', vmembers.manage_services_jx, name='used-services-create-edit'),
    path('members/services/archive/', vmembers.archive_services_jx, name='used-services-archive'),

    # Comments post
    path('tickets/add_comment/<ticket_id>', vtickets.add_comment_jx, name='tickets-add-comment'),
    path('tickets/get_comments/<ticket_id>', vtickets.get_comments_jx, name='tickets-get-comments'),
    path('tickets/archive_comment/', vtickets.archive_comment_jx, name='tickets-archive-comment'),

    # Percentage post
    path('tickets/set_percentage/<ticket_id>/range/', vtickets.set_percentage_jx, name='tickets-set-percentage'),
    path('tickets/get_percentage/<ticket_id>', vtickets.get_percentage_jx, name='tickets-get-percentage'),

    # Prospective company post
    path('tickets/add_company/', vtickets.add_company_jx, name='tickets-add-company'),

    # Microtask post
    path('tickets/add_microtask/<ticket_id>', vtickets.add_microtask_jx, name='tickets-add-microtask'),
    path('tickets/get_microtask/<mk_id>', vtickets.get_microtask_jx, name='tickets-get-microtask'),
    path('tickets/get_microtasks/<ticket_id>', vtickets.get_microtasks_jx, name='tickets-get-microtasks'),
    path('tickets/archive_microtask/', vtickets.archive_microtask_jx, name='tickets-archive-microtasks'),

    # Master data for countries
    path('settings/countries/', vmasterdata.list_countries, name='countries-list'),
    path('settings/countries/create/', vmasterdata.manage_countries, name='countries-create'),
    path('settings/countries/<country_id>', vmasterdata.manage_countries, name='countries-edit'),
    path('settings/countries/archive/<country_id>', vmasterdata.archive_countries, name='countries-archive'),

    # Master data for services
    path('settings/services/', vmasterdata.list_services, name='services-list'),
    path('settings/services/create/', vmasterdata.manage_services, name='services-create'),
    path('settings/services/<service_id>', vmasterdata.manage_services, name='services-edit'),
    path('settings/services/archive/<service_id>', vmasterdata.archive_services, name='services-archive'),

    # Master data for basic subscriptions
    path('settings/basic_subscriptions/', vmasterdata.list_basic_subscriptions, name='basic-subscriptions-list'),
    path('settings/basic_subscriptions/create/', vmasterdata.manage_basic_subscriptions, name='basic-subscriptions-create'),
    path('settings/basic_subscriptions/<basic_subscription_id>', vmasterdata.manage_basic_subscriptions, name='basic-subscriptions-edit'),
    path('settings/basic_subscriptions/archive/<basic_subscription_id>', vmasterdata.archive_basic_subscriptions, name='basic-subscriptions-archive'),

    # Master data for enrollment fees
    path('settings/enrollment_fees/', vmasterdata.list_enrollment_fees, name='enrollment-fees-list'),
    path('settings/enrollment_fees/create/', vmasterdata.manage_enrollment_fees, name='enrollment-fees-create'),
    path('settings/enrollment_fees/<enrollment_fee_id>', vmasterdata.manage_enrollment_fees, name='enrollment-fees-edit'),
    path('settings/enrollment_fees/archive/<enrollment_fee_id>', vmasterdata.archive_enrollment_fees, name='enrollment-fees-archive'),

    # Master data for payment types
    path('settings/payment_types/', vmasterdata.list_payment_types, name='payment-types-list'),
    path('settings/payment_types/create/', vmasterdata.manage_payment_types, name='payment-types-create'),
    path('settings/payment_types/<payment_type_id>', vmasterdata.manage_payment_types, name='payment-types-edit'),
    path('settings/payment_types/archive/<payment_type_id>', vmasterdata.archive_payment_types, name='payment-types-archive'),

    # Master data for payment intervals
    path('settings/payment_intervals/', vmasterdata.list_payment_intervals, name='payment-intervals-list'),
    path('settings/payment_intervals/create/', vmasterdata.manage_payment_intervals, name='payment-intervals-create'),
    path('settings/payment_intervals/<payment_interval_id>', vmasterdata.manage_payment_intervals, name='payment-intervals-edit'),
    path('settings/payment_intervals/archive/<payment_interval_id>', vmasterdata.archive_payment_intervals, name='payment-intervals-archive'),

    # Master data for membership types
    path('settings/membership_types/', vmasterdata.list_membership_types, name='membership-types-list'),
    path('settings/membership_types/create/', vmasterdata.manage_membership_types, name='membership-types-create'),
    path('settings/membership_types/<membership_type_id>', vmasterdata.manage_membership_types, name='membership-types-edit'),
    path('settings/membership_types/archive/<membership_type_id>', vmasterdata.archive_membership_types, name='membership-types-archive'),

    # Search
    path('search/', vsearch.search, name='search'),

    # Settings & utilities
    path('settings/', settings, name='tickets-settings'),
    path('settings/appsettings/', view_appsettings, name='appsettings-view'),
    path('settings/appsettings/save/', save_appsettings, name='appsettings-save'),
]
