/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';

require('node_modules/jszip');
const pdfMake = require('node_modules/pdfmake/build/pdfmake.min.js');
const pdfFonts = require('node_modules/pdfmake/build/vfs_fonts.js');
pdfMake.vfs = pdfFonts.pdfMake.vfs;
require('node_modules/datatables.net-bs4')(window, $);
require('node_modules/datatables.net-autofill-bs4')(window, $);
require('node_modules/datatables.net-buttons-bs4')(window, $);
require('node_modules/datatables.net-buttons/js/buttons.colVis.js')(window, $);
require('node_modules/datatables.net-buttons/js/buttons.html5.js')(window, $);
require('node_modules/datatables.net-buttons/js/buttons.print.js')(window, $);
require('node_modules/datatables.net-responsive-bs4')(window, $);
require('node_modules/datatables.net-select-bs4')(window, $);
require('node_modules/datatable-sorting-datetime-moment');
require('society_manager_assets/js/datatables-filterwidgets');

// sort date columns correctly
$.fn.dataTable.moment($dateformat);

// export column button
$.fn.dataTable.ext.buttons.colexp = function(dt, conf) {
    return {
        extend: 'collection',
        text: function(dt) {
            return dt.i18n('buttons.colexp', 'Column export');
        },
        className: 'buttons-colexp',
        buttons: [
            {
                extend: 'columnsExport',
                columns: conf.columns,
                columnText: conf.columnText,
            },
        ],
    };
};

// Selected columns with individual buttons - toggle column export
$.fn.dataTable.ext.buttons.columnsExport = function(dt, conf) {
    var columns = dt
        .columns(conf.columns)
        .indexes()
        .map(function(idx) {
            return {
                extend: 'columnExport',
                columns: idx,
                visibility: conf.visibility,
                columnText: conf.columnText,
            };
        })
        .toArray();

    return columns;
};

// Single button to toggle column export
$.fn.dataTable.ext.buttons.columnExport = {
    columns: undefined, // column selector
    text: function(dt, button, conf) {
        return conf._columnText(dt, conf);
    },
    className: 'buttons-columnExport',
    action: function(e, dt, button, conf) {
        var col = parseInt(conf.columns);
        var dataTable = dt.context[0];

        // call datables.net API
        var api = new $.fn.dataTable.Api(dataTable);
        var column = api.table().column(col);

        // toggle class 'marked-for-export'
        if (column.header().className.includes('marked-for-export')) {
            $(column.header()).removeClass('marked-for-export');
            // disable button
            this.active(false);
        } else {
            $(column.header()).addClass('marked-for-export');
            // enable button
            this.active(true);
        }
    },
    init: function(dt, button, conf) {
        var col = parseInt(conf.columns);
        var dataTable = dt.context[0];

        // call datables.net API
        var api = new $.fn.dataTable.Api(dataTable);
        var column = api.table().column(col);

        // enables button, if class 'marked-for-export' exists
        if (column.header().className.includes('marked-for-export')) {
            this.active(true);
        }
    },
    _columnText: function(dt, conf) {
        // Use DataTables' internal data structure until this is presented
        // is a public API. The other option is to use
        // `$( column(col).node() ).text()` but the node might not have been
        // populated when Buttons is constructed.
        var idx = dt.column(conf.columns).index();
        var title = dt
            .settings()[0]
            .aoColumns[idx].sTitle.replace(/\n/g, ' ') // remove new lines
            .replace(/<br\s*\/?>/gi, ' ') // replace line breaks with spaces
            .replace(/<select(.*?)<\/select>/g, '') // remove select tags, including options text
            .replace(/<!\-\-.*?\-\->/g, '') // strip HTML comments
            .replace(/<.*?>/g, '') // strip HTML
            .replace(/^\s+|\s+$/g, ''); // trim

        return conf.columnText ? conf.columnText(dt, idx, title) : title;
    },
};

// Load user customized columns from app settings
$(document).on('init.dt', function(e, settings) {
    let api = new $.fn.dataTable.Api(settings);

    if ($ticket_columns.length > 0) {
        if (settings.sTableId == 'dataTables-tickets') {
            settings.aoColumns.forEach(column => {
                //console.log($ticket_columns);
                if ($ticket_columns.includes(column.idx.toString())) {
                    api.table()
                        .column(column.idx)
                        .visible(true);
                } else {
                    // don't hide control columns, which have no sTitle
                    if (column.sTitle != '') {
                        api.table()
                            .column(column.idx)
                            .visible(false);
                    }
                }
            });
        }
    }

    if ($prospect_columns.length > 0) {
        if (settings.sTableId == 'dataTables-prospects') {
            settings.aoColumns.forEach(column => {
                //console.log($prospect_columns);
                if ($prospect_columns.includes(column.idx.toString())) {
                    api.table()
                        .column(column.idx)
                        .visible(true);
                } else {
                    // don't hide control columns, which have no sTitle
                    if (column.sTitle != '') {
                        api.table()
                            .column(column.idx)
                            .visible(false);
                    }
                }
            });
        }
    }

    if ($member_columns.length > 0) {
        if (settings.sTableId == 'dataTables-members') {
            settings.aoColumns.forEach(column => {
                //console.log($member_columns);
                if ($member_columns.includes(column.idx.toString())) {
                    api.table()
                        .column(column.idx)
                        .visible(true);
                } else {
                    // don't hide control columns, which have no sTitle
                    if (column.sTitle != '') {
                        api.table()
                            .column(column.idx)
                            .visible(false);
                    }
                }
            });
        }
    }
});
