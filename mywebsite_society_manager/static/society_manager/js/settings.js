/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 *
 * Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
 */

import 'node_modules/jquery';
import 'node_modules/select2';
import 'node_modules/jquery-minicolors';
import _ from 'node_modules/lodash';

import 'society_manager_assets/js/datatables';

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Init datatables
    $('#dataTables-basic-subscriptions').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Subscription'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-countries').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-enrollment-fees').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Subscription'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-membership-types').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-payment-intervals').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-payment-types').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-services').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-users').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Username'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-groups').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-queues').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-rights').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[2, 'asc']], // order by column 'Name'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-states').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[1, 'asc']], // order by column 'ID'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    $('#dataTables-priorities').DataTable({
        responsive: true,
        dom: '<lf<t>ip>',
        stateSave: false,
        order: [[1, 'asc']], // order by column 'ID'
        ordering: true,
        language: $dt_language,
        columnDefs: [
            {
                orderable: false,
                className: 'control',
                targets: 0,
            },
        ],
    });

    // Initialize colorpicker
    if ($('#id_color').length) {
        $('#id_color').minicolors({
            theme: 'bootstrap',
        });
    }

    // create select2 combobox for groups, if field is not hidden
    if (!$('#id_groups').is('[hidden]')) {
        $('#id_groups')
            .select2({
                width: '100%',
                placeholder: '---------',
                allowClear: true,
                selectOnClose: true,
            })
            .on('select2:unselecting', function(ev) {
                // Bugfix for open dropbox again, after allowClear!
                if (ev.params.args.originalEvent) {
                    ev.params.args.originalEvent.stopPropagation();
                } else {
                    $('select').each(function(index) {
                        if (
                            $(this).hasClass('select2-hidden-accessible') &&
                            $(this)
                                .data('select2')
                                .isOpen()
                        ) {
                            $(this).select2('close');
                        }
                    });
                    $(this).one('select2:opening', function(ev) {
                        ev.preventDefault();
                    });
                }
            });
    }

    // create select2 combobox for group permissions
    $('#id_permissions')
        .select2({
            width: '100%',
            placeholder: '---------',
            allowClear: true,
            selectOnClose: true,
        })
        .on('select2:unselecting', function(ev) {
            // Bugfix for open dropbox again, after allowClear!
            if (ev.params.args.originalEvent) {
                ev.params.args.originalEvent.stopPropagation();
            } else {
                $('select').each(function(index) {
                    if (
                        $(this).hasClass('select2-hidden-accessible') &&
                        $(this)
                            .data('select2')
                            .isOpen()
                    ) {
                        $(this).select2('close');
                    }
                });
                $(this).one('select2:opening', function(ev) {
                    ev.preventDefault();
                });
            }
        });

    // create select2 combobox for ticket columns and format data
    if ($('#id_ticket_columns').length) {
        var ticket_data = _.map(window.$dt_columns_ticket, function(obj) {
            //console.log($ticket_columns);
            if ($ticket_columns.includes(obj.id.toString())) {
                obj.selected = true;
            }
            //console.log(obj);
            return obj;
        });
        $('#id_ticket_columns')
            .select2({
                width: '100%',
                placeholder: '---------',
                allowClear: true,
                selectOnClose: true,
                data: ticket_data,
            })
            .on('select2:unselecting', function(ev) {
                // Bugfix for open dropbox again, after allowClear!
                if (ev.params.args.originalEvent) {
                    ev.params.args.originalEvent.stopPropagation();
                } else {
                    $('select').each(function(index) {
                        if (
                            $(this).hasClass('select2-hidden-accessible') &&
                            $(this)
                                .data('select2')
                                .isOpen()
                        ) {
                            $(this).select2('close');
                        }
                    });
                    $(this).one('select2:opening', function(ev) {
                        ev.preventDefault();
                    });
                }
            });
    }

    // create select2 combobox for prospect columns and format data
    if ($('#id_prospect_columns').length) {
        var prospect_data = _.map(window.$dt_columns_prospect, function(obj) {
            //console.log($prospect_columns);
            if ($prospect_columns.includes(obj.id.toString())) {
                obj.selected = true;
            }
            //console.log(obj);
            return obj;
        });
        $('#id_prospect_columns')
            .select2({
                width: '100%',
                placeholder: '---------',
                allowClear: true,
                selectOnClose: true,
                data: prospect_data,
            })
            .on('select2:unselecting', function(ev) {
                // Bugfix for open dropbox again, after allowClear!
                if (ev.params.args.originalEvent) {
                    ev.params.args.originalEvent.stopPropagation();
                } else {
                    $('select').each(function(index) {
                        if (
                            $(this).hasClass('select2-hidden-accessible') &&
                            $(this)
                                .data('select2')
                                .isOpen()
                        ) {
                            $(this).select2('close');
                        }
                    });
                    $(this).one('select2:opening', function(ev) {
                        ev.preventDefault();
                    });
                }
            });
    }

    // create select2 combobox for member columns and format data
    if ($('#id_member_columns').length) {
        var member_data = _.map(window.$dt_columns_member, function(obj) {
            //console.log($member_columns);
            if ($member_columns.includes(obj.id.toString())) {
                obj.selected = true;
            }
            //console.log(obj);
            return obj;
        });
        $('#id_member_columns')
            .select2({
                width: '100%',
                placeholder: '---------',
                allowClear: true,
                selectOnClose: true,
                data: member_data,
            })
            .on('select2:unselecting', function(ev) {
                // Bugfix for open dropbox again, after allowClear!
                if (ev.params.args.originalEvent) {
                    ev.params.args.originalEvent.stopPropagation();
                } else {
                    $('select').each(function(index) {
                        if (
                            $(this).hasClass('select2-hidden-accessible') &&
                            $(this)
                                .data('select2')
                                .isOpen()
                        ) {
                            $(this).select2('close');
                        }
                    });
                    $(this).one('select2:opening', function(ev) {
                        ev.preventDefault();
                    });
                }
            });
    }
});
