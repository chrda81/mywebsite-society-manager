/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 *
 * Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
 */

'use strict';

import 'node_modules/jquery';
import 'static/mywebsite/js/default/ajax-setup';

// import i18n functions from Django. Read url, e.g. 'const django_i18n_url' from base.html
export default function get_django_i18n_script($url) {
    let django_i18n_script = '';
    $.ajax({
        type: 'GET',
        url: $url,
        async: false,
    }).done(function(data) {
        django_i18n_script = data;
    });

    return django_i18n_script;
}
