/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';
import 'society_manager_assets/js/datatables';

// Global functions

function Init() {
    //we catch the values rendered by Django template
    var idProspect_data = $('#idProspect').val();

    //Request membership
    $('.bs-create-member-modal-lg button.request-ok')
        .off('click')
        .on('click', function() {
            //console.log(idProspect_data);
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    prospect_id: idProspect_data,
                    ticket_subject: 'Application of membership',
                },
                success: function(data) {
                    //console.log(data);
                    // Close the modal
                    $('.bs-create-member-modal-lg').modal('toggle');
                    notif('info', sSuccess, data.message);
                },
                error: function(xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });
}

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Init datatables
    var $dt_columns_prospect = [
        { id: 2, text: 'Company Name' },
        { id: 3, text: 'Additional Name' },
        { id: 4, text: 'Contact Title' },
        { id: 5, text: 'Contact Last Name' },
        { id: 6, text: 'Contact First Name' },
        { id: 7, text: 'Country' },
        { id: 8, text: 'Zipcode' },
        { id: 9, text: 'Location' },
        { id: 10, text: 'Address' },
        { id: 11, text: 'E-Mail' },
        { id: 12, text: 'Mobile' },
        { id: 13, text: 'Phone' },
        { id: 14, text: 'Fax' },
        { id: 15, text: 'Labels' },
    ];
    window.$dt_columns_prospect = $dt_columns_prospect;
    var table = $('#dataTables-prospects').DataTable({
        processing: true,
        ajax: {
            url: app_url_prefix + '/prospects/data/',
            type: 'POST',
        },
        deferRender: true,
        serverSide: true,
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, sLengthMenuAll]],
        responsive: {
            details: {
                type: 'column',
                target: 1,
            },
        },
        dom: '<lfip<rt>ip><"clearfix">B',
        sortCellsTop: true,
        select: {
            style: 'multi',
            selector: 'td:first-child',
            blurable: true,
        },
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(:first-child, :nth-child(2))',
                postfixButtons: [
                    {
                        text: sResetSavedState,
                        action: function(e, dt, node, config) {
                            if (localStorage) {
                                table.state.clear();
                                window.location.reload();
                            }
                        },
                    },
                ],
            },
            {
                extend: 'colexp',
                columns: ':not(:first-child, :nth-child(2))',
            },
            {
                extend: 'collection',
                text: 'Export',
                autoclose: true,
                enabled: false,
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                        customize: function(win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .css('background-color', '#fff')
                                .prepend(
                                    '<img src="/static/mywebsite/img/logo.png" style="position:absolute; top:10px; left:700px; width: 110px; height: 45px;" />'
                                );

                            $(win.document.body)
                                .find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                    },
                ],
            },
        ],
        columnDefs: [
            {
                // 0: checkbox
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                width: 12,
                name: 'select-checkbox',
                data: null,
                defaultContent: '',
            },
            {
                // 1: responsive control
                targets: 1,
                orderable: false,
                className: 'control',
                name: 'control',
                data: null,
                defaultContent: '',
            },
            {
                // 2: Company Name
                targets: 2,
                className: 'marked-for-export',
                data: 2,
                name: 'name',
                render: function(data, type, full, meta) {
                    return (
                        '<a href="' + app_url_prefix + '/prospects/view/' +
                        full[0] +
                        '">' +
                        full[2] +
                        '</a>'
                    );
                },
            },
            {
                // 3: Additional Name
                targets: 3,
                visible: false,
                className: 'marked-for-export',
                data: 3,
                name: 'additional_name',
            },
            {
                // 4: Contact Title
                targets: 4,
                data: 4,
                name: 'contact_title',
            },
            {
                // 5: Contact Last Name
                targets: 5,
                data: 5,
                name: 'contact_last_name',
            },
            {
                // 6: Contact First Name
                targets: 6,
                data: 6,
                name: 'contact_first_name',
            },
            {
                // 7: Country
                targets: 7,
                data: 7,
                name: 'country',
            },
            {
                // 8: Zip Code
                targets: 8,
                data: 8,
                name: 'zipcode',
            },
            {
                // 9: Location
                targets: 9,
                data: 9,
                name: 'location',
            },
            {
                // 10: Address
                targets: 10,
                data: 10,
                name: 'address',
            },
            {
                // 11: E-Mail
                targets: 11,
                data: 11,
                name: 'email',
            },
            {
                // 12: Mobile
                targets: 12,
                data: 12,
                name: 'mobile',
            },
            {
                // 13: Phone
                targets: 13,
                data: 13,
                name: 'phone',
            },
            {
                // 14: Fax
                targets: 14,
                data: 14,
                name: 'fax',
            },
            {
                // 15: Labels
                targets: 15,
                data: 15,
                name: 'labels',
            },
        ],
        columnFilters: [
            {
                // 0: checkbox
                type: 'None',
            },
            {
                // 1: responsive control
                type: 'None',
            },
            {
                // 2: Company Name
                type: 'Text',
            },
            {
                // 3: Additional Name
                type: 'Text',
            },
            {
                // 4: Contact Title
                type: 'Text',
            },
            {
                // 5: Contact Last Name
                type: 'Text',
            },
            {
                // 6: Contact First Name
                type: 'Text',
            },
            {
                // 7: Country
                type: 'Select',
            },
            {
                // 8: Zip Code
                type: 'Text',
            },
            {
                // 9: Location
                type: 'Text',
            },
            {
                // 10: Address
                type: 'Text',
            },
            {
                // 11: E-Mail
                type: 'Text',
            },
            {
                // 12: Mobile
                type: 'Text',
            },
            {
                // 13: Phone
                type: 'Text',
            },
            {
                // 14: Fax
                type: 'Text',
            },
            {
                // 15: Labels
                type: 'Text',
            },
        ],
        order: [[2, 'asc']], // order by column 'Company Name'
        ordering: true,
        language: $dt_language,
    });

    table.on('select deselect', function() {
        var selectedRows = table.rows({ selected: true }).count();

        table.button(2).enable(selectedRows > 0); // export button
    });

    // select checkbox: de/select all 'filtered' rows
    table.on('click', 'th.select-checkbox', function() {
        if ($('th.select-checkbox').hasClass('selected')) {
            table.rows({ filter: 'applied' }).every(function() {
                this.deselect();
            });
            $('th.select-checkbox').removeClass('selected');
        } else {
            table.rows({ filter: 'applied' }).every(function() {
                this.select();
            });
            $('th.select-checkbox').addClass('selected');
        }
    });

    // never save filter values for state save
    table.on('stateSaveParams.dt', function(e, settings, data) {
        data.columns.forEach(column => {
            column.search.search = '';
        });
    });

    Init();
});

// jQuery DOM manipulation after each AJAX call has completed
$(document).ajaxComplete(function() {
    Init();
});
