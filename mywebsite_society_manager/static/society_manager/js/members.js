/**
 * Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

import 'node_modules/jquery';
import 'node_modules/select2';
import 'node_modules/bootstrap-daterangepicker';
import moment from 'node_modules/moment';

import 'society_manager_assets/js/datatables';

// Global functions

function Init() {
    //we catch the values rendered by Django template
    var idMember_data = $('#idMember').val();

    //Request company edit
    $('.tab-content button.company-edit')
        .off('click')
        .on('click', function() {
            //console.log(idMember_data);
            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    member_id: idMember_data,
                    ticket_subject: 'Edit company',
                },
                success: function(data) {
                    //console.log(data);
                    notif('info', sSuccess, data.message);
                },
                error: function(xhr, status, error) {
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });
}

// jQuery DOM manipulation after page has load (triggered only once)
$(document).ready(function() {
    // Date picker issue date
    if ($('#id_member-issue_date').length) {
        $('#id_member-issue_date').daterangepicker({
            showISOWeekNumbers: true,
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            showCustomRangeLabel: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            todayHighlight: true,
            autoApply: true,
            locale: $dp_locale,
            ranges: {
                sNow: [
                    moment().format($dateformat),
                    moment().format($dateformat),
                ],
                sClear: [
                    moment('19700101').format($dateformat),
                    moment('19700101').format($dateformat),
                ],
            },
        });

        // Locale for range keys
        $('.ranges li[data-range-key="sNow"]').text(sNow);
        $('.ranges li[data-range-key="sClear"]').text(sClear);
    }

    // Date picker exit date
    if ($('#id_member-exit_date').length) {
        $('#id_member-exit_date').daterangepicker({
            showISOWeekNumbers: true,
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            showCustomRangeLabel: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            todayHighlight: true,
            autoApply: true,
            locale: $dp_locale,
            ranges: {
                sNow: [
                    moment().format($dateformat),
                    moment().format($dateformat),
                ],
                sClear: [
                    moment('19700101').format($dateformat),
                    moment('19700101').format($dateformat),
                ],
            },
        });

        // Locale for range keys
        $('.ranges li[data-range-key="sNow"]').text(sNow);
        $('.ranges li[data-range-key="sClear"]').text(sClear);
    }

    // Init datatables
    var $dt_columns_member = [
        { id: 2, text: 'ID' },
        { id: 3, text: 'Company Name' },
        { id: 4, text: 'Additional Name' },
        { id: 5, text: 'Contact' },
        { id: 6, text: 'Country' },
        { id: 7, text: 'Zipcode' },
        { id: 8, text: 'Location' },
        { id: 9, text: 'Address' },
        { id: 10, text: 'Mobile' },
        { id: 11, text: 'Phone' },
        { id: 12, text: 'Fax' },
        { id: 13, text: 'E-Mail' },
        { id: 14, text: 'Issue Date' },
        { id: 15, text: 'Exit Date' },
        { id: 16, text: 'Payment Due Date' },
        { id: 17, text: 'Basic Subscription' },
        { id: 18, text: 'Paying' },
        { id: 19, text: 'Active' },
    ];
    window.$dt_columns_member = $dt_columns_member;
    var table = $('#dataTables-members').DataTable({
        processing: true,
        ajax: {
            url: app_url_prefix + '/members/data/',
            type: 'POST',
        },
        deferRender: true,
        serverSide: true,
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, sLengthMenuAll]],
        responsive: {
            details: {
                type: 'column',
                target: 1,
            },
        },
        dom: '<lfip<rt>ip><"clearfix">B',
        sortCellsTop: true,
        select: {
            style: 'multi',
            selector: 'td:first-child',
            blurable: true,
        },
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                columns: ':not(:first-child, :nth-child(2))',
                postfixButtons: [
                    {
                        text: sResetSavedState,
                        action: function(e, dt, node, config) {
                            if (localStorage) {
                                table.state.clear();
                                window.location.reload();
                            }
                        },
                    },
                ],
            },
            {
                extend: 'colexp',
                columns: ':not(:first-child, :nth-child(2))',
            },
            {
                extend: 'collection',
                text: 'Export',
                autoclose: true,
                enabled: false,
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: '.marked-for-export',
                            orthogonal: 'export',
                            modifier: {
                                selected: true,
                            },
                        },
                        customize: function(win) {
                            $(win.document.body)
                                .css('font-size', '10pt')
                                .css('background-color', '#fff')
                                .prepend(
                                    '<img src="/static/mywebsite/img/logo.png" style="position:absolute; top:10px; left:700px; width: 110px; height: 45px;" />'
                                );

                            $(win.document.body)
                                .find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        },
                    },
                ],
            },
        ],
        columnDefs: [
            {
                // 0: checkbox
                targets: 0,
                orderable: false,
                className: 'select-checkbox',
                width: 12,
                name: 'select-checkbox',
                data: null,
                defaultContent: '',
            },
            {
                // 1: responsive control
                targets: 1,
                orderable: false,
                className: 'control',
                name: 'control',
                data: null,
                defaultContent: '',
            },
            {
                // 2: Nr
                targets: 2,
                className: 'marked-for-export',
                data: 2,
                name: 'nr',
                render: function(data, type, full, meta) {
                    return (
                        '<a href="' + app_url_prefix + '/members/view/' +
                        full[0] +
                        '">' +
                        full[2] +
                        '</a>'
                    );
                },
            },
            {
                // 3: Company Name
                targets: 3,
                className: 'marked-for-export',
                data: 3,
                name: 'company.name',
            },
            {
                // 4: Additional Name
                targets: 4,
                visible: false,
                data: 4,
                name: 'company.additional_name',
            },
            {
                // 5: Contact
                targets: 5,
                data: 5,
                name: 'company.contact_person',
            },
            {
                // 6: Country
                targets: 6,
                data: 6,
                name: 'company.country.name',
            },
            {
                // 7: Zip Code
                targets: 7,
                data: 7,
                name: 'company.zipcode',
            },
            {
                // 8: Location
                targets: 8,
                data: 8,
                name: 'company.location',
            },
            {
                // 9: Address
                targets: 9,
                data: 9,
                name: 'company.address',
            },
            {
                // 10: Mobile
                targets: 10,
                data: 10,
                name: 'company.mobile',
            },
            {
                // 11: Phone
                targets: 11,
                visible: false,
                data: 11,
                name: 'company.phone',
            },
            {
                // 12: Fax
                targets: 12,
                visible: false,
                data: 12,
                name: 'company.fax',
            },
            {
                // 13: E-Mail
                targets: 13,
                visible: false,
                data: 13,
                name: 'company.email',
            },
            {
                // 14: Issue Date
                targets: 14,
                visible: false,
                data: 14,
                name: 'issue_date',
                render: function(data, type, full, meta) {
                    return moment(data, 'DD.MM.YYYY HH:mm').format($dateformat);
                },
            },
            {
                // 15: Exit Date
                targets: 15,
                visible: false,
                data: 15,
                name: 'exit_date',
                render: function(data, type, full, meta) {
                    return moment(data, 'DD.MM.YYYY HH:mm').format($dateformat);
                },
            },
            {
                // 16: Payment Due Dat
                targets: 16,
                data: 16,
                name: 'payment_due_date',
            },
            {
                // 17: Basic Subscription
                targets: 17,
                visible: false,
                data: 17,
                name: 'basic_subscription',
            },
            {
                // 18: Paying
                targets: 18,
                visible: false,
                data: 18,
                name: 'is_paying',
            },
            {
                // 19: Active
                targets: 19,
                data: 19,
                name: 'is_active',
            },
        ],
        columnFilters: [
            {
                // 0: checkbox
                type: 'None',
            },
            {
                // 1: responsive control
                type: 'None',
            },
            {
                // 2: Nr
                type: 'Text',
            },
            {
                // 3: Company Name
                type: 'Text',
            },
            {
                // 4: Additional Name
                type: 'Text',
            },
            {
                // 5: Contact
                type: 'Text',
            },
            {
                // 6: Country
                type: 'Select',
            },
            {
                // 7: Zip Code
                type: 'Text',
            },
            {
                // 8: Location
                type: 'Text',
            },
            {
                // 9: Address
                type: 'Text',
            },
            {
                // 10: Mobile
                type: 'Text',
            },
            {
                // 11: Phone
                type: 'Text',
            },
            {
                // 12: Fax
                type: 'Text',
            },
            {
                // 13: E-Mail
                type: 'Text',
            },
            {
                // 14: Issue Date
                type: 'Date',
            },
            {
                // 15: Exit Date
                type: 'Date',
            },
            {
                // 16: Payment Due Date
                type: 'Select',
            },
            {
                // 17: Basic Subscription
                type: 'Text',
            },
            {
                // 18: Paying
                type: 'Select',
            },
            {
                // 19: Active
                type: 'Select',
            },
        ],
        order: [[19, 'desc'], [2, 'asc']], // order by column 'Active', 'Nr'
        ordering: true,
        language: $dt_language,
    });

    table.on('select deselect', function() {
        var selectedRows = table.rows({ selected: true }).count();

        table.button(2).enable(selectedRows > 0); // export button
    });

    // select checkbox: de/select all 'filtered' rows
    table.on('click', 'th.select-checkbox', function() {
        if ($('th.select-checkbox').hasClass('selected')) {
            table.rows({ filter: 'applied' }).every(function() {
                this.deselect();
            });
            $('th.select-checkbox').removeClass('selected');
        } else {
            table.rows({ filter: 'applied' }).every(function() {
                this.select();
            });
            $('th.select-checkbox').addClass('selected');
        }
    });

    // never save filter values for state save
    table.on('stateSaveParams.dt', function(e, settings, data) {
        data.columns.forEach(column => {
            column.search.search = '';
        });
    });

    $('#memberform').on('submit', function(e) {
        // add value of disabled field company to data, otherwise Django form validate will fail!
        let $company = $('#id_member-company').clone(true); // use deep copy of field, to remove disabled attribute
        $(this).append($company.removeAttr('disabled').css('display', 'none'));

        return true;
    });

    // triggered when modal is about to be shown
    $('.bs-create-edit-used-services-modal-lg').on('show.bs.modal', function(
        e
    ) {
        if (e.hasOwnProperty('relatedTarget')) {
            if (
                typeof $(e.relatedTarget).data('service-id') !== 'undefined' &&
                typeof $(e.relatedTarget).data('service-description') !==
                    'undefined'
            ) {
                // show all services in select box
                $('#id_services-service option').each(function(i, x) {
                    $(this).show();
                });

                $('#id_services-service').val(
                    $(e.relatedTarget).data('service-id')
                );
                $('#id_services-description').val(
                    $(e.relatedTarget).data('service-description')
                );
                $('#id_services-is_checked').val(
                    $(e.relatedTarget).data('service-is-checked')
                );
            } else {
                // hide used services in select box
                $('#id_services-service option').each(function(index, item) {
                    if ($used_services_columns.includes($(this).text())) {
                        $(this).hide();
                    }
                });

                // clear selection
                $('#id_services-service').val('');
                $('#id_services-description').val('');
                $('#id_services-is_checked').prop('checked', false);
            }
        }
    });

    $('.bs-create-edit-used-services-modal-lg .request-ok')
        .off('click')
        .on('click', function(event) {
            //console.log('.request-ok called');
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    service_id: $('#id_services-service').val(),
                    member_id: $('#id_services-member').val(),
                    description: $('#id_services-description').val(),
                    is_checked: $('#id_services-is_checked').is(':checked'),
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        // Close the modal
                        $('.bs-create-edit-used-services-modal-lg').modal(
                            'toggle'
                        );
                        notif('error', sError, data.message);
                    } else {
                        let is_checked = '<i class="fa fa-times"></i>';
                        if (data.is_checked == 'True') {
                            is_checked = '<i class="fa fa-check"></i>';
                        }

                        let table_length =
                            $('#dataTables-used-services tbody tr').length + 1;

                        // check, if table row is already there
                        let element_found = false;
                        $('#dataTables-used-services tbody tr').each(function(
                            index,
                            item
                        ) {
                            if (item.innerText.includes(data.service_name)) {
                                element_found = true;

                                // set 3rd column
                                $(item)
                                    .find('td:eq(2)')
                                    .html(data.description);

                                // set 4th column
                                $(item)
                                    .find('td:eq(3)')
                                    .html(is_checked);
                            }
                        });

                        // if not control var, add new row
                        if (!element_found) {
                            $('#dataTables-used-services tbody').append(
                                '<tr class="odd gradeX">\
                                <td></td>\
                                <td class="text-left"><a href="#" data-target=".bs-create-edit-used-services-modal-lg" data-toggle="modal" data-service-id="' +
                                    data.service_id +
                                    '" data-service-description="' +
                                    data.description +
                                    '">' +
                                    data.service_name +
                                    '</a></td>\
                                <td class="text-left">\
                                    ' +
                                    data.description +
                                    '\
                                </td>\
                                <td class="text-center">' +
                                    is_checked +
                                    '</i></td>\
                                <td\
                                    class="text-right"\
                                    style="white-space: nowrap"\
                                ><a\
                                        href="#"\
                                        class="btn btn-outline btn-xs"\
                                        data-target=".bs-create-edit-used-services-modal-lg"\
                                        data-toggle="modal"\
                                        data-service-id="' +
                                    data.service_id +
                                    '"\
                                        data-service-description="' +
                                    data.description +
                                    '"\
                                        data-service-is_checked="' +
                                    data.is_checked +
                                    '"\
                                    ><i\
                                            class="fa fa-pencil-square-o"\
                                            aria-hidden="true"\
                                        ></i></a>\
                                    <a\
                                        href="#"\
                                        class="btn btn-outline btn-xs"\
                                        data-target=".bs-archive-used-services-modal-lg"\
                                        data-toggle="modal"\
                                        data-service-id="' +
                                    data.service_id +
                                    '"\
                                    ><i\
                                            class="fa fa-archive"\
                                            aria-hidden="true"\
                                        ></i></a>\
                                </td>\
                                </tr>'
                            );
                        }

                        // Close the modal
                        $('.bs-create-edit-used-services-modal-lg').modal(
                            'toggle'
                        );
                        notif('info', sSuccess, data.message);
                    }
                },
                error: function(xhr, status, error) {
                    //$('#message_data').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    // triggered when modal is about to be shown
    $('.bs-archive-used-services-modal-lg').on('show.bs.modal', function(e) {
        if (e.hasOwnProperty('relatedTarget')) {
            if (typeof $(e.relatedTarget).data('service-id') !== 'undefined') {
                $('#idService').val($(e.relatedTarget).data('service-id'));
            }
        }
    });

    $('.bs-archive-used-services-modal-lg .request-ok')
        .off('click')
        .on('click', function(event) {
            //console.log('.request-ok called');
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: $(this).attr('href'),
                dataType: 'json',
                data: {
                    service_id: $('#idService').val(),
                    member_id: $('#idMember').val(),
                },
                success: function(data) {
                    //console.log(data);
                    if (!data.success) {
                        // Close the modal
                        $('.bs-archive-used-services-modal-lg').modal('toggle');
                        notif('error', sError, data.message);
                    } else {
                        let table_length =
                            $('#dataTables-used-services tbody tr').length + 1;

                        // check, if table row is already there
                        let element_found = false;
                        $('#dataTables-used-services tbody tr').each(function(
                            index,
                            item
                        ) {
                            if (item.innerText.includes(data.service_name)) {
                                // delete item
                                $(item).remove();
                            }
                        });

                        // Close the modal
                        $('.bs-archive-used-services-modal-lg').modal('toggle');
                        notif('info', sSuccess, data.message);
                    }
                },
                error: function(xhr, status, error) {
                    //$('#message_data').val('');
                    var json = JSON.parse(xhr.responseText);
                    var error_message = json.message;
                    notif('error', sError, error_message);
                },
            });
        });

    Init();
});

// jQuery DOM manipulation after each AJAX call has completed
$(document).ajaxComplete(function() {
    Init();
});
