# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django import forms
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

import django_filters

from .models import MembershipType, SocietyMember, Ticket


class SocietyMembersFilter(django_filters.FilterSet):
    nr = django_filters.CharFilter(field_name='nr', lookup_expr='icontains')
    company = django_filters.CharFilter(field_name='company__name', lookup_expr='icontains')
    contact = django_filters.CharFilter(field_name='company__contact_last_name', lookup_expr='icontains')
    membership = django_filters.ModelMultipleChoiceFilter(queryset=MembershipType.objects.filter(is_archived=False),
                                                          widget=forms.CheckboxSelectMultiple)
    is_active = django_filters.ChoiceFilter(choices=((0, _('not active')), (1, _('is active'))))
    is_paying = django_filters.ChoiceFilter(choices=((0, _('not paying')), (1, _('is paying'))))

    class Meta:
        model = SocietyMember
        fields = ['nr', 'company', 'contact', 'membership', 'is_active', 'is_paying']


class TicketFilter(django_filters.FilterSet):
    assigned_company = django_filters.CharFilter(field_name='assigned_company__name', lookup_expr='icontains')
    subject = django_filters.CharFilter(lookup_expr='icontains')
    body = django_filters.CharFilter(lookup_expr='icontains')
    comment = django_filters.CharFilter(field_name='ticket_rel_comm__comment',
                                        lookup_expr='icontains', label=_('Comment contains'))
    active_member = django_filters.BooleanFilter(
        field_name='assigned_company__members__is_active', label=_('Active Member'), method='filter_boolean_value')

    class Meta:
        model = Ticket
        fields = ['assigned_company', 'subject', 'body', 'comment',
                  'assigned_state', 'create_user', 'assigned_user', 'active_member']

    def __init__(self, *args, **kwargs):
        super(TicketFilter, self).__init__(*args, **kwargs)
        if self.data == {}:
            self.queryset = self.queryset.none()

    def filter_boolean_value(self, queryset, name, value):
        if value:
            return queryset.filter(Q(**{name: True}) & Q(assigned_company__members__is_archived=False))
        else:
            return queryset.exclude(**{name: True})
