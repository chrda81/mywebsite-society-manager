# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator
  and https://github.com/seddonym/django-yearlessdate!
--------------------------------------------------------------
"""

import mimetypes
import tempfile
from io import BytesIO  # A stream implementation using an in-memory bytes buffer. It inherits BufferIOBase
from pathlib import Path
from urllib.parse import urlparse

from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.core.exceptions import FieldError
from django.core.files.storage import default_storage
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.http import HttpResponse, HttpResponseNotFound
from django.template.loader import render_to_string
from django.utils.encoding import force_text
from django.utils.functional import Promise

import weasyprint

from .widgets import YearlessDate, YearlessDateFormField


def getListIndex(_list, name):
    position = 0

    for item in _list:
        if item['name'] == name:
            return position

        position = position + 1

    return None


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_text(obj)
        return super(LazyEncoder, self).default(obj)


class YearlessDateField(models.Field):
    """ A model field for storing dates without years.
    """
    description = "A date without a year, for use in things like birthdays"

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 4
        super(YearlessDateField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if isinstance(value, YearlessDate):
            return value
        if not value:
            return None
        # The string case.
        return YearlessDate(value[2:], value[:2])

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def get_prep_value(self, value):
        "The reverse of to_python, for inserting into the database"
        if value is not None:
            return ''.join(["%02d" % i for i in (value.month, value.day)])

    def get_internal_type(self):
        return 'CharField'

    def value_to_string(self, obj):
        "For serialization"
        value = self.value_from_object(obj)
        return self.get_prep_value(value)

    def formfield(self, **kwargs):
        # This is a fairly standard way to set up some defaults
        # while letting the caller override them.
        defaults = {'form_class': YearlessDateFormField}
        defaults.update(kwargs)
        return super(YearlessDateField, self).formfield(**defaults)


def get_object_or_none(classmodel, **kwargs):
    try:
        return classmodel.objects.get(**kwargs)
    except classmodel.DoesNotExist:
        return None


def paginate(queryset, page, limit=settings.PAGINATE_BY):
    start = (page - 1) * limit
    end = (page * limit)
    page_count = queryset.count() / limit
    queryset = queryset[start:end]
    return queryset, {
        'page': page,
        'page_count': page_count,
        'last_page': page_count + 1,
        'pages': range(1, int(page_count + 2)),
    }


def query_view(
        model, params, order_by='-id', granted_queues=None, excludes=None,
        limit=settings.PAGINATE_BY, **kwargs):
    params = {key: value for key, value in params.items() if value}
    params.update({key: value for key, value in kwargs.items() if value})
    try:
        page = int(params.pop('page', 1))
    except ValueError:
        page = 1

    try:
        if excludes:
            queryset = model.objects.exclude(excludes).filter(is_archived=False)
        else:
            queryset = model.objects.filter(is_archived=False)
        queryset = queryset.filter(**params).filter(granted_queues).order_by(order_by)
    except FieldError:
        return {}

    data, pagination = paginate(queryset, page, limit)

    return {
        'data': data,
        'pagination': pagination,
        'query': '&'.join(['%s=%s' % (key, value)
                           for key, value in params.items()])
    }


def weasyprint_url_fetcher(url, *args, **kwargs):
    # load file:// paths directly from disk
    if url.startswith('file:'):
        mime_type, encoding = mimetypes.guess_type(url)
        url_path = urlparse(url).path
        data = {
            'mime_type': mime_type,
            'encoding': encoding,
            'filename': Path(url_path).name,
        }

        if settings.MEDIA_URL and url_path.startswith(settings.MEDIA_URL):
            path = url_path.replace(settings.MEDIA_URL, settings.MEDIA_ROOT)
            data['file_obj'] = default_storage.open(path)
            return data

        elif settings.STATIC_URL and url_path.startswith(settings.STATIC_URL):
            path = url_path.replace(settings.STATIC_URL, '')
            data['file_obj'] = open(find(path), 'rb')
            return data

    # fall back to weasyprint default fetcher
    return weasyprint.default_url_fetcher(url, *args, **kwargs)


def render_to_pdf(template_src, context_dict={}):
    """ define render_to_pdf() function """
    html_string = render_to_string(template_src, context_dict)
    html = weasyprint.HTML(string=html_string, url_fetcher=weasyprint_url_fetcher)
    result = html.write_pdf()

    if not result:
        return HttpResponseNotFound('The requested pdf was not found in our server.')

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=export.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response


def to_bool(value):
    """
       Converts 'something' to boolean. Raises exception for invalid formats
           Possible True  values: 1, True, "1", "TRue", "yes", "y", "t"
           Possible False values: 0, False, None, [], {}, "", "0", "faLse", "no", "n", "f", 0.0, ...

       Taken from https://stackoverflow.com/questions/715417/converting-from-a-string-to-boolean-in-python
    """
    if str(value).lower() in ("yes", "y", "true",  "t", "1"):
        return True
    if str(value).lower() in ("no",  "n", "false", "f", "0", "0.0", "", "none", "[]", "{}"):
        return False
    raise Exception('Invalid value for boolean conversion: ' + str(value))


def get_parsed_value(text, key_word):
    """
        Extract a value from a text input by its key word (until end of line), e.g.

        text:
            ...
            - First name: Johnny
            ...

        get_parsed_value(text, 'First name'), will extract 'Johnny'.
    """
    if text:
        for line in text.split('\n'):
            if key_word in line:
                value = line.partition(f"{key_word}:")[2]
                # initializing bad_chars_list
                bad_chars = ['\r', '\n']

                # using replace() to
                # remove bad_chars
                for i in bad_chars:
                    value = value.replace(i, '')
                
                # remove leading and endign spaces
                return value.strip()

    return ''
