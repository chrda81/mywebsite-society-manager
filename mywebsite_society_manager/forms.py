# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""
from datetime import datetime

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation
from django.core.validators import FileExtensionValidator
from django.forms.widgets import HiddenInput
from django.shortcuts import reverse
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Field, Layout
from mywebsite_home.models import Settings
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from . import models, rights, utils

# User model alias
User = get_user_model()


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'groups', 'is_staff', 'is_superuser')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = models.Profile
        fields = ('avatar', 'status_rel', 'rssfeed', 'ticket_columns',
                  'prospect_columns', 'member_columns', 'has_collapsednavbar')

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['ticket_columns'] = forms.CharField(
            label=_('ticket_columns_field'), widget=forms.widgets.SelectMultiple(), required=False)
        self.fields['prospect_columns'] = forms.CharField(
            label=_('prospect_columns_field'), widget=forms.widgets.SelectMultiple(), required=False)
        self.fields['member_columns'] = forms.CharField(
            label=_('member_columns_field'), widget=forms.widgets.SelectMultiple(), required=False)


class ProspectiveCompanyForm(forms.ModelForm):
    mobile = PhoneNumberField(label=_('mobile_field'), widget=PhoneNumberPrefixWidget(), required=False)
    phone = PhoneNumberField(label=_('phone_field'), widget=PhoneNumberPrefixWidget(), required=True)
    fax = PhoneNumberField(label=_('fax_field'), widget=PhoneNumberPrefixWidget(), required=False)
    country = forms.ModelChoiceField(label=_('country_field'),
                                     queryset=models.Country.objects.filter(is_archived=False))

    class Meta:
        model = models.ProspectiveCompany
        fields = '__all__'
        exclude = ('is_archived',)

    def __init__(self, *args, **kwargs):
        # pop additional attributes before instantiating
        self.required_fields_only = kwargs.pop('required_fields_only', False)
        self.readonly_form = kwargs.pop('readonly_form', False)
        super(ProspectiveCompanyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'ajax-form-company'
        self.helper.form_class = ''
        self.helper.form_method = 'POST'
        self.helper.form_action = reverse('society_manager:tickets-add-company')
        self.helper.attrs.update({'novalidate': ''})

        # make form readonly
        if self.readonly_form:
            for name, field in self.fields.items():
                field.disabled = True

        # build forms layout
        if self.required_fields_only:
            self.helper.layout = Layout(
                'name',
                'contact_last_name',
                'contact_first_name',
                'country',
                'zipcode',
                'location',
                'address',
                'email',
                Field('phone', template='fields/phonenumber_prefix_input.html', css_class='col-md-6')
            )
        else:
            self.helper.layout = Layout(
                Div(
                    Div('name', css_class='form-group col-md-6 mb-0'),
                    Div('additional_name', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
                Div(
                    Div('contact_title', css_class='form-group col-md-2 mb-0'),
                    Div('contact_first_name', css_class='form-group col-md-5 mb-0'),
                    Div('contact_last_name', css_class='form-group col-md-5 mb-0'),
                    css_class='form-row'
                ),
                Div(
                    Div('address', css_class='form-group col-md-12 mb-0'),
                    css_class='form-row'
                ),
                Div(
                    Div('country', css_class='form-group col-md-3 mb-0'),
                    Div('zipcode', css_class='form-group col-md-3 mb-0'),
                    Div('location', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
                Div(
                    Div('email', css_class='form-group col-md-6 mb-0'),
                    Div('website', css_class='form-group col-md-6 mb-0'),
                    css_class='form-row'
                ),
                Div(
                    Div(
                        Field('mobile', template='fields/phonenumber_prefix_input.html', css_class='col-md-3 mr-2'),
                        css_class='form-group col-md-12 mb-0'
                    ),
                    css_class='form-row'
                ),
                Div(
                    Div(
                        Field('phone', template='fields/phonenumber_prefix_input.html', css_class='col-md-3 mr-2'),
                        css_class='form-group col-md-12 mb-0'
                    ),
                    css_class='form-row'
                ),
                Div(
                    Div(
                        Field('fax', template='fields/phonenumber_prefix_input.html', css_class='col-md-3 mr-2'),
                        css_class='form-group col-md-12 mb-0'
                    ),
                    css_class='form-row'
                )
            )

    """
    Error codes:
    """

    def clean_error_unique_constraint(self):
        violation = _('A prospective company with this name already exists')
        return violation

    def clean(self):
        # Some essential vars
        cleaned_data = self.cleaned_data

        # Check unique constraint
        if not self.instance.pk and hasattr(cleaned_data, 'name'):
            unique_key = cleaned_data['name']
            query = models.Ticket.objects.filter(name=unique_key)
            if unique_key and len(query) > 0:
                raise forms.ValidationError(self.clean_error_unique_constraint())


class SocietyMemberForm(forms.ModelForm):
    sepa_bank_name = forms.CharField(label=_('sepa_bank_name_field'), max_length=250, required=False)
    sepa_account_name = forms.CharField(label=_('sepa_account_name_field'), max_length=250, required=False)
    sepa_iban = forms.CharField(label=_('sepa_iban_field'), max_length=34, required=False)
    sepa_bic = forms.CharField(label=_('sepa_bic_field'), max_length=11, required=False)

    def __init__(self, *args, **kwargs):
        super(SocietyMemberForm, self).__init__(*args, **kwargs)

        # ignore errors for payment data, if tab is hidden
        if not settings.SHOW_MEMBERS_PAYMENT_DATA:
            self.fields['enrollment_fee'].required = False
            self.fields['basic_subscription'].required = False
            self.fields['payment_type'].required = False
            self.fields['payment_interval'].required = False
            self.initial['is_paying'] = False
            self.fields['is_paying'].widget = HiddenInput()

        if 'instance' in kwargs:
            member = kwargs['instance']
            self.initial['sepa_bank_name'] = member.company.sepa_bank_name
            self.initial['sepa_account_name'] = member.company.sepa_account_name
            self.initial['sepa_iban'] = member.company.sepa_iban
            self.initial['sepa_bic'] = member.company.sepa_bic

    class Meta:
        model = models.SocietyMember
        fields = '__all__'
        exclude = ('is_archived',)
        widgets = {
            'recommendation': forms.TextInput(attrs={'placeholder': _('How did the prospect hear about us?')}),
        }

    def save(self):
        if self.cleaned_data:
            # handle company model
            company = self.instance.company
            company.sepa_bank_name = self.cleaned_data['sepa_bank_name']
            company.sepa_account_name = self.cleaned_data['sepa_account_name']
            company.sepa_iban = self.cleaned_data['sepa_iban']
            company.sepa_bic = self.cleaned_data['sepa_bic']
            company.save()

            # save the member before saving dependencies
            super().save()


# Ticketator forms
class GroupForm(forms.ModelForm):
    class Meta:
        model = models.Group
        fields = '__all__'


class QueueForm(forms.ModelForm):
    class Meta:
        model = models.Queue
        fields = '__all__'
        exclude = ('is_archived',)
        help_texts = {
            'shortcode': _('<b>Important:</b> Use short code as distinctive short notation for this queue.\
                <br />You can use something as [YourQueue]. <b>This value is used in App-Settings!</b>')
        }


class RightForm(forms.ModelForm):
    class Meta:
        model = models.Rights
        fields = '__all__'
        exclude = ('is_archived',)

    # Check at form stage if registry is created or if we can create it
    def clean_queue_dst(self):
        detect_function = models.Rights.detect_rights_exists(
            models.Rights(), self.cleaned_data.get('grp_src'),
            self.cleaned_data.get('queue_dst'))
        # Check if no pk assigned and if detect_function['status'] is True
        if not self.instance.pk and detect_function['status']:
            raise forms.ValidationError(
                "Rule already created (" + str(detect_function['numbers'][0]) + ") src=>" +
                str(self.cleaned_data.get('grp_src')) +
                " dst=>" + str(self.cleaned_data.get('queue_dst')) + "")
        return self.cleaned_data.get('queue_dst')


class StateForm(forms.ModelForm):
    class Meta:
        model = models.State
        fields = '__all__'
        exclude = ('is_archived',)
        help_texts = {
            'shortcode': _('<b>Important:</b> Use short code as distinctive short notation for this state.\
                <br /><b>This value is used in App-Settings!</b>')
        }


class PriorityForm(forms.ModelForm):
    class Meta:
        model = models.Priority
        fields = '__all__'
        exclude = ('is_archived',)


class TicketForm(forms.ModelForm):
    #  Pass request to a form => http://stackoverflow.com/questions/6325681/
    #  passing-a-user-request-to-forms
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(TicketForm, self).__init__(*args, **kwargs)
        # hide label of following fields
        self.fields['body'].label = False
        self.initial['labels'] = self.instance.labels

    class Meta:
        model = models.Ticket
        fields = '__all__'
        exclude = ('fsm_state', 'is_mailed', 'is_archived')
        widgets = {
            'subject': forms.TextInput(attrs={'placeholder': _('Brief description about the ticket')}),
            'body': forms.Textarea(attrs={'placeholder': _('Please enter your comments here')}),
            'labels': forms.TextInput(attrs={'placeholder': _('Add labels separated by comma')})
        }

    """
    Check for violations:
    """

    def clean_assigned_queue(self):
        if self.instance.pk is not None:
            user_object_rights = rights.get_rights_for_ticket(
                user=self.request.user,
                queue=self.cleaned_data.get('assigned_queue'), ticket_id=None)
            if not user_object_rights.can_create:
                raise forms.ValidationError(self.clean_error_cantcreate())

        return self.cleaned_data.get('assigned_queue')

    def clean_assigned_company(self):
        # get assigned_company from request
        value = self.cleaned_data.get('assigned_company')
        queue = self.cleaned_data.get('assigned_queue')

        if self.instance.pk is not None:
            # get assigned_company from instance
            if not value and self.instance.assigned_company:
                value = self.instance.assigned_company

        """Check queue for assigned company"""
        # search for all non-changeable queues of assigned tickets
        if value and queue:
            company_queues = value.rel_ticket.filter(
                assigned_queue__is_changeable=False).order_by().values_list('assigned_queue__name', flat=True).distinct()
            if company_queues and queue.name not in company_queues:
                raise forms.ValidationError(self.clean_error_wrong_queue(),
                                            code='invalid',
                                            params={'value': ','.join(company_queues)})

        return value

    """
    Error codes:
    """

    def clean_error_cantedit(self):
        cantedit = _('You don\'t have permissions to edit this ticket')
        return cantedit

    def clean_error_cantview(self):
        cantview = _('You don\'t have permissions to view this ticket')
        return cantview

    def clean_error_cantcreate(self):
        cantcreate = _('You don\'t have permissions to create this ticket')
        return cantcreate

    def clean_error_cantsave(self):
        cantsave = _('You don\'t have permissions to save this ticket')
        return cantsave

    def clean_error_unique_constraint(self):
        violation = _('A ticket with this subject and company already exists')
        return violation

    def clean_error_wrong_queue(self):
        violation = _('The company is assigned to a non changeable queue. Please change the actual tickets queue to\
            one of these: %(value)s')
        return violation

    def clean(self):
        # Some essential vars
        user_obj = self.request.user
        cleaned_data = self.cleaned_data
        queue_obj = cleaned_data.get('assigned_queue')

        # """Check unique constraint"""
        # if not self.instance.pk or (self.instance.pk and self.instance.assigned_company is None):
        #     # having 'subject' is mandatory for this check
        #     if hasattr(cleaned_data, 'subject'):
        #         unique_key1 = cleaned_data['subject']
        #         unique_key2 = cleaned_data['assigned_company']
        #         query = models.Ticket.objects.filter(subject=unique_key1, assigned_company=unique_key2)
        #         if unique_key1 and unique_key2 and len(query) > 0:
        #             raise forms.ValidationError(self.clean_error_unique_constraint())

        """Check creation"""
        if not self.instance.pk:
            user_object_rights = rights.get_rights_for_ticket(
                user=user_obj, queue=queue_obj, ticket_id=None)
            if not user_object_rights.can_create:
                raise forms.ValidationError(self.clean_error_cantcreate())

        """Check edition"""
        if self.instance.pk:
            user_object_rights = rights.get_rights_for_ticket(
                user=user_obj, queue=queue_obj, ticket_id=self.instance.id)
            if not user_object_rights.can_edit:
                raise forms.ValidationError(self.clean_error_cantedit())


class TicketAdminForm(forms.ModelForm):
    labels = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': _('Add labels separated by comma')}
    ),
        required=False
    )

    class Meta:
        model = models.Ticket
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(TicketAdminForm, self).__init__(*args, **kwargs)
        self.fields['fsm_state'].widget.attrs['readonly'] = True
        self.fields['labels'].label = _('labels_field')
        self.initial['labels'] = self.instance.labels


class AttachmentForm(forms.Form):
    allowed_file_extensions = []
    # collect all possible document file extensions
    for ext in settings.FILEBROWSER_EXTENSIONS['Document']:
        allowed_file_extensions.append(ext.strip(' .'))

    # collect all possible image file extensions
    for ext in settings.FILEBROWSER_EXTENSIONS['Image']:
        allowed_file_extensions.append(ext.strip(' .'))

    file_name_1 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_2 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_3 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_4 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_5 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_6 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_7 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_8 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_9 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)
    file_name_10 = forms.FileField(validators=[FileExtensionValidator(
        allowed_extensions=allowed_file_extensions
    )], required=False)


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = '__all__'
        exclude = ('is_archived',)


# Master data forms
class BasicSubscriptionForm(forms.ModelForm):
    class Meta:
        model = models.BasicSubscription
        fields = '__all__'
        exclude = ('is_archived',)


class CountryForm(forms.ModelForm):
    class Meta:
        model = models.Country
        fields = '__all__'
        exclude = ('is_archived',)


class EnrollmentFeeForm(forms.ModelForm):
    class Meta:
        model = models.EnrollmentFee
        fields = '__all__'
        exclude = ('is_archived',)


class PaymentTypeForm(forms.ModelForm):
    class Meta:
        model = models.PaymentType
        fields = '__all__'
        exclude = ('is_archived',)


class PaymentIntervalForm(forms.ModelForm):
    class Meta:
        model = models.PaymentInterval
        fields = '__all__'
        exclude = ('is_archived',)


class MembershipTypeForm(forms.ModelForm):
    class Meta:
        model = models.MembershipType
        fields = '__all__'
        exclude = ('is_archived',)


class ServiceForm(forms.ModelForm):
    class Meta:
        model = models.Service
        fields = '__all__'
        exclude = ('is_archived',)


class UsedServicesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UsedServicesForm, self).__init__(*args, **kwargs)

        if 'instance' in kwargs:
            member = kwargs['instance']
            self.initial['member'] = member.pk
            self.initial['is_checked'] = False

    class Meta:
        model = models.UsedServices
        fields = '__all__'
        exclude = ('is_archived',)
