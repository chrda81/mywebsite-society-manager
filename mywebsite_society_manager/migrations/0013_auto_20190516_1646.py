# Generated by Django 2.0.10 on 2019-05-16 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        # ('auditlog', '0007_object_pk_type'),
        ('mywebsite_society_manager', '0012_auto_20190508_0223'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='logs',
            name='log_ticket',
        ),
        migrations.RemoveField(
            model_name='logs',
            name='log_user',
        ),
        # migrations.CreateModel(
        #     name='AuditLogEntry',
        #     fields=[
        #     ],
        #     options={
        #         'proxy': True,
        #         'indexes': [],
        #     },
        #     bases=('auditlog.logentry',),
        # ),
        migrations.AlterField(
            model_name='microtasks',
            name='body',
            field=models.TextField(blank=True, null=True, verbose_name='microtask_body_field'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='body',
            field=models.TextField(blank=True, null=True, verbose_name='ticket_body_field'),
        ),
        migrations.DeleteModel(
            name='Logs',
        ),
    ]
