# Generated by Django 2.0.10 on 2019-05-07 15:00

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('mywebsite_society_manager', '0010_delete_settings'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='societymember',
            name='interests',
        ),
        migrations.RemoveField(
            model_name='societymember',
            name='service_focus',
        ),
        migrations.AddField(
            model_name='prospectivecompany',
            name='is_applying_for_membership',
            field=models.BooleanField(default=False, verbose_name='is_applying_for_membership_field'),
        ),
        migrations.AddField(
            model_name='state',
            name='shortcode',
            field=models.CharField(default='', max_length=16, verbose_name='shortcode_field'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='prospectivecompany',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None, verbose_name='phone_field'),
        ),
        migrations.AlterField(
            model_name='queue',
            name='shortcode',
            field=models.CharField(max_length=16, verbose_name='shortcode_field'),
        ),
        migrations.AlterField(
            model_name='societymember',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='is_active_field'),
        ),
        migrations.AlterField(
            model_name='societymember',
            name='is_paying',
            field=models.BooleanField(default=True, verbose_name='is_paying_field'),
        ),
    ]
