# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.

  Credit for contents of this file goes to the project https://github.com/sabueso/ticketator!
--------------------------------------------------------------
"""

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.shortcuts import get_object_or_404

# Get correct user model
User = get_user_model()


def logo_company(request):
    return {'logo': settings.LOGO_COMPANY}


def collect_django_messages(request):
    messages = get_messages(request)
    notify_messages = []
    for m in messages:
        notify_messages.append((m.tags, m.message))
    return {'notify_messages': notify_messages}


def get_datatable_columns(request, user_id=None):
    ticket_columns = []
    prospect_columns = []
    member_columns = []

    if not request.user.is_anonymous:
        if user_id:
            actual_user = get_object_or_404(User, pk=user_id)
        else:
            actual_user = get_object_or_404(User, pk=request.user.id)
        actual_user_profile = actual_user.profile_society_manager

        if actual_user_profile and actual_user_profile.ticket_columns:
            ticket_columns = [item.strip("'") for item in actual_user_profile.ticket_columns[1:-1].split(', ')]

        if actual_user_profile and actual_user_profile.prospect_columns:
            prospect_columns = [item.strip("'") for item in actual_user_profile.prospect_columns[1:-1].split(', ')]

        if actual_user_profile and actual_user_profile.member_columns:
            member_columns = [item.strip("'") for item in actual_user_profile.member_columns[1:-1].split(', ')]

    return {'ticket_columns': ticket_columns, 'prospect_columns': prospect_columns, 'member_columns': member_columns}
